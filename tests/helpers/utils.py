def count_logs_by_check_name(logs):
    count_logs = {}
    for l in logs:
        if l.check_name in count_logs:
            count_logs[l.check_name] += 1
        else:
            count_logs[l.check_name] = 1
    return count_logs


def compare_log_counts(logs, ref_logs):
    for check_name, ref_count in ref_logs.items():
        assert check_name in logs, 'Expected check %s was not found in execution logs. Execution logs: %s' % (
            check_name, logs)
        assert logs[check_name] == ref_count, 'Expected check %s has a different number of logs in execution logs (test:%d, ref:%d). Execution logs: %s' % (
            check_name, logs[check_name], ref_count, logs)
    for check_name in logs:
        assert check_name in ref_logs, 'Check %s is absent in reference logs. Execution logs: %s' % (
            check_name, logs)
        

def compare_points_from_geometry( expected_geometry: list, result_geometry: list):
    for i in range(len(result_geometry)):
        assert expected_geometry[i].ExportToWkt() == result_geometry[i].ExportToWkt(), "%s isn't equal to %s" % (
        expected_geometry[i].ExportToWkt(), result_geometry[i].ExportToWkt()
    )
    assert len(expected_geometry)== len(result_geometry), "%s isn't equal to %s" % (
        len(expected_geometry), len(result_geometry))
    

def assert_get_ogr_geometry(geometry , expected_geom, epsilon = 0.01):
    from deliverycheck.util.formats.dxf import get_ogr_geometry
    result = get_ogr_geometry(geometry, epsilon)
    assert expected_geom.ExportToWkt() == result.ExportToWkt(), "%s isn't equal to %s" % (expected_geom, result) 


def unzip(zip_file_path: str, extension: str, tmp_dir: str)-> str :
    """Unzips the file with a specific extension from the zip named zip_file_name and returns the unzipped file path

    Args:
        zip_file_path : path to zip that contains the desired file
        file_type : extension of the file desired from zip (e.g : dxf)
        tmp_dir : path to temporary directory where the file is unzipped

    Returns:
        str : file path
    """
    import zipfile

    with zipfile.ZipFile(zip_file_path , 'r') as zip:
        for file_name in zip.namelist():
            if file_name.endswith(".%s" % extension):
                return zip.extract(file_name, tmp_dir)
    