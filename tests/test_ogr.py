import os
import shutil
from osgeo import osr
from deliverycheck.util.formats.ogr import *
import pytest
from helpers.utils import compare_points_from_geometry
import tempfile


def test_feature_has_field():
    """ Tests the function feature_has_field by running it with its two possible outcomes False and True
    """
    # create feature and field
    field = ogr.FieldDefn("Test", ogr.OFTString)
    feature_defn = ogr.FeatureDefn()
    feature_defn.AddFieldDefn(field)
    feature = ogr.Feature(feature_defn)

    field_name = "Test"
    field_name_False = "Test_False"

    # test the function to verify that the feature has a field named field_name
    assert feature_has_field(
        feature, field_name), "The feature %s should have a field named %s" % (field_name)

    # test the function to verify that the feature doesn't have a field named field_name
    assert not feature_has_field(
        feature, field_name_False), "The feature %s shouldn't have a field named %s" % (feature, field_name_False)


def test_layer_has_field():
    """ Tests the function layer_has_field by running it with its two possible outcomes False and True
    """
    # create temporary directory
    tmp_dir = tempfile.gettempdir()
    os.mkdir(tmp_dir + "/shp")

    # create shapefile
    driver = ogr.GetDriverByName("ESRI Shapefile")
    ds = driver.CreateDataSource(tmp_dir + "/shp/test.shp")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    # create layer
    layer = ds.CreateLayer("test_layer_has_field", srs, ogr.wkbPoint)

    # create field
    field = ogr.FieldDefn("Test_True", ogr.OFTInteger)
    layer.CreateField(field)

    field_name_True = "Test_True"
    field_name_False = "Test_False"

    # test the function to verify that the layer have a field named field_name
    assert layer_has_field(layer, field_name_True), "The layer %s should have a field named %s" % (
        layer, field_name_True)

    # test the function to verify that the layer have a field named field_name
    assert not layer_has_field(
        layer, field_name_False), "The layer %s shouldn't have a field named %s" % (layer, field_name_False)

    # remove shp
    shutil.rmtree(tmp_dir + "/shp")


def test_as_unmeasured_geometry():
    """ Tests the function as_unmeasured_geometry for all possible coordinates option of a point geometry
    """
    types = ["Point", "Point25D",
             "PointZM", "PointM"]

    coord = [(123, 414, 478, 523)]
    for geom_type in types:
        for x, y, z, m in coord:
            if geom_type == "Point":
                point = ogr.Geometry(ogr.wkbPoint)
                expected_point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(x, y)
                expected_point.AddPoint(x, y)
            elif geom_type == "Point25D":
                point = ogr.Geometry(ogr.wkbPoint25D)
                expected_point = ogr.Geometry(ogr.wkbPoint25D)
                point.AddPoint(x, y, z)
                expected_point.AddPoint(x, y, z)
            elif geom_type == "PointZM":
                point = ogr.Geometry(ogr.wkbPointZM)
                expected_point = ogr.Geometry(ogr.wkbPointZM)
                point.AddPointZM(x, y, z, m)
                expected_point.AddPoint(x, y, z)
            else:
                point = ogr.Geometry(ogr.wkbPointM)
                expected_point = ogr.Geometry(ogr.wkbPointM)
                point.AddPointM(x, y, m)
                expected_point.AddPoint_2D(x, y)

        result = as_unmeasured_geometry(point)

        # compare the expected point and the point returned by the function as_unmeasured_geometry
        assert result.ExportToWkt() == expected_point.ExportToWkt(
        ), "%s should be equal to %s" % (result, expected_point)


def test_get_points_from_geometry_simple_point():
    """ Tests the function get_points_from_geometry for the geometry point
    """
    # create point
    coordinates = [(119, 64, 59)]
    point = ogr.Geometry(ogr.wkbPoint)
    for x, y, z in coordinates:
        point.AddPoint(x, y)

    result_point = get_points_from_geometry(point)

    # compare the expected point and the point returned by the function get_points_from_geometry
    compare_points_from_geometry([point], result_point)

    # create point25D
    point25D = ogr.Geometry(ogr.wkbPoint25D)
    for x, y, z in coordinates:
        point25D.AddPoint(x, y, z)

    result_point25D = get_points_from_geometry(point25D)

    # compare the expected point and the point returned by the function get_points_from_geometry
    compare_points_from_geometry([point], result_point25D)


def test_get_points_from_geometry_simple_linestring():
    """ Tests the function get_points_from_geometry for the geometry linestring
    """
    coordinates = [(111, 637, 51),
                   (128, 636, 50)]

    # create linestring
    line = ogr.Geometry(ogr.wkbLineString)
    points = []
    for x, y, z in coordinates:
        line.AddPoint(x, y)
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(x, y)
        points.append(point)

    result_points = get_points_from_geometry(line)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points)

    # create linestring25D
    line25D = ogr.Geometry(ogr.wkbLineString25D)
    points25D = []
    for x, y, z in coordinates:
        line25D.AddPoint(x, y, z)
        point25D = ogr.Geometry(ogr.wkbPoint25D)
        point25D.AddPoint(x, y)
        points25D.append(point25D)

    result_points25D = get_points_from_geometry(line25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points25D)


def test_get_points_from_geometry_simple_polygon():
    """ Tests the function get_points_from_geometry for the geometry polygon
    """
    coordinates = [(117, 712, 124),
                   (121, 641, 122),
                   (122, 721, 121)]

    # create polygon
    ring = ogr.Geometry(ogr.wkbLinearRing)
    polygon = ogr.Geometry(ogr.wkbPolygon)
    points = []
    out_points = []
    for x, y, z in coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        ring.AddPoint(x, y)
        point.AddPoint(x, y)
        points.append(point)
    ring.CloseRings()
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(*coordinates[0][:-1])
    points.append(point)
    polygon.AddGeometry(ring)
    result_points = get_points_from_geometry(polygon)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points)

    # create polygon25D
    ring25D = ogr.Geometry(ogr.wkbLinearRing)
    polygon25D = ogr.Geometry(ogr.wkbPolygon25D)
    for x, y, z in coordinates:
        ring25D.AddPoint(x, y, z)
    ring25D.CloseRings()
    polygon25D.AddGeometry(ring25D)

    result_points25D = get_points_from_geometry(polygon25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points25D)

    # create polygon with hole
    inner_ring_coordinates = [(114, 691),
                              (119, 648),
                              (119, 691)]

    inner_ring = ogr.Geometry(ogr.wkbLinearRing)
    polygon_with_hole = ogr.Geometry(ogr.wkbPolygon)
    polygon_with_hole.AddGeometry(ring)

    for x, y in inner_ring_coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        inner_ring.AddPoint(x, y)
        point.AddPoint(x, y)
        points.append(point)
    point = ogr.Geometry(ogr.wkbPoint)
    inner_ring.CloseRings()
    point.AddPoint(*inner_ring_coordinates[0])
    points.append(point)
    polygon_with_hole.AddGeometry(inner_ring)

    result_point_polygon_with_hole = get_points_from_geometry(
        polygon_with_hole)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_point_polygon_with_hole)

    result_include_inner = get_points_from_geometry(polygon_with_hole, False)

    out_points.append(points[0])  # first point from the first polygon
    out_points.append(points[4])  # first point from the second polygon

    # compare the expected points and the points returned by the function get_points_from_geometry with the argument include_inner_geoms as False
    compare_points_from_geometry(out_points, result_include_inner)


def test_get_points_from_geometry_multi_point():
    """ Tests the function get_points_from_geometry for the geometry multi point
    """
    # create multipoint
    coordinates = [(125, 598),
                   (124, 601)]

    multipoint = ogr.Geometry(ogr.wkbMultiPoint)
    points = []
    for x, y in coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(x, y)
        points.append(point)
        multipoint.AddGeometry(point)

    result_multipoint = get_points_from_geometry(multipoint)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_multipoint)

    # create multipoint25D
    coordinates25D = [(125, 598, 124),
                      (124, 601, 125)]

    multipoint25D = ogr.Geometry(ogr.wkbMultiPoint25D)
    for x, y, z in coordinates25D:
        point25D = ogr.Geometry(ogr.wkbPoint25D)
        point25D.AddPoint(x, y, z)
        multipoint25D.AddGeometry(point25D)

    result_multipoints25D = get_points_from_geometry(multipoint25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_multipoints25D)


def test_get_points_from_geometry_multi_line():
    """ Tests the function get_points_from_geometry for the geometry multi linestring
    """
    # create multilinestring
    coordinates = [(121, 617, 415),
                   (123, 629, 450),
                   (118, 626, 455),
                   (121, 606, 451)]
    points = []
    multiline = ogr.Geometry(ogr.wkbMultiLineString)

    line1 = ogr.Geometry(ogr.wkbLineString)
    line2 = ogr.Geometry(ogr.wkbLineString)

    for i in range(4):
        point = ogr.Geometry(ogr.wkbPoint)
        if i < 2:
            line1.AddPoint(*coordinates[i][:-1])
        else:
            line2.AddPoint(*coordinates[i][:-1])
        point.AddPoint(*coordinates[i][:-1])
        points.append(point)
        i += 1

    multiline.AddGeometry(line1)
    multiline.AddGeometry(line2)

    result_multiline = get_points_from_geometry(multiline)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_multiline)

    # create multilinestring25D
    multiline25D = ogr.Geometry(ogr.wkbMultiLineString25D)

    line1_25D = ogr.Geometry(ogr.wkbLineString)
    line2_25D = ogr.Geometry(ogr.wkbLineString)

    for i in range(4):
        point = ogr.Geometry(ogr.wkbPoint)
        if i < 2:
            line1_25D.AddPoint(*coordinates[i])
        else:
            line2_25D.AddPoint(*coordinates[i])
        i += 1

    multiline25D.AddGeometry(line1_25D)
    multiline25D.AddGeometry(line2_25D)

    result_multiline25D = get_points_from_geometry(multiline25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_multiline25D)


def test_get_points_from_geometry_multi_polygon():
    """ Tests the function get_points_from_geometry for the geometry multi polygon
    """
    coordinates_ring1 = [(120, 634, 541),
                         (120, 620, 540),
                         (121, 620, 539),
                         (121, 634, 521)]

    coordinates_ring2 = [(117, 681, 540),
                         (117, 626, 561),
                         (119, 626, 555),
                         (119, 647, 551)]

    rings = [coordinates_ring1, coordinates_ring2]

    # create multipolygon
    multipolygon = ogr.Geometry(ogr.wkbMultiPolygon)
    points = []

    for ring_coord in rings:
        ring = ogr.Geometry(ogr.wkbLinearRing)
        poly = ogr.Geometry(ogr.wkbPolygon)
        for x, y, z in ring_coord:
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(x, y)
            ring.AddPoint(x, y)
            points.append(point)
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(*ring_coord[0][:-1])
        points.append(point)
        ring.CloseRings()
        poly.AddGeometry(ring)
        multipolygon.AddGeometry(poly)

    result_points = get_points_from_geometry(multipolygon)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points)

    # polygon25D
    multipolygon25D = ogr.Geometry(ogr.wkbMultiPolygon25D)

    for ring_coord in rings:
        ring = ogr.Geometry(ogr.wkbLinearRing)
        poly = ogr.Geometry(ogr.wkbPolygon25D)
        for x, y, z in ring_coord:
            ring.AddPoint(x, y, z)
        ring.CloseRings()
        poly.AddGeometry(ring)
        multipolygon25D.AddGeometry(poly)

    result_points25D = get_points_from_geometry(multipolygon25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points25D)


def test_get_points_from_geometry_multi_collection():
    """ Tests the function get_points_from_geometry for the geometry geometry collection
    """
    # geometry collection
    geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
    points = []
    point_arg = ogr.Geometry(ogr.wkbPoint)
    point_arg.AddPoint(132, 57)
    points.append(point_arg)
    geomcol.AddGeometry(point_arg)

    line = ogr.Geometry(ogr.wkbLineString)
    coordinates = [(133, 58, 22), (131, 56, 21)]
    for x, y, z in coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        line.AddPoint(x, y)
        point.AddPoint(x, y)
        points.append(point)

    geomcol.AddGeometry(line)

    result_points = get_points_from_geometry(geomcol)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points)

    result_True = get_points_from_geometry(geomcol, True, True)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry([point_arg], result_True)

    # geometry collection 25D
    geomcol25D = ogr.Geometry(ogr.wkbGeometryCollection25D)

    point = ogr.Geometry(ogr.wkbPoint25D)
    point.AddPoint(132, 57, 21)
    geomcol25D.AddGeometry(point)

    line = ogr.Geometry(ogr.wkbLineString25D)
    for x, y, z in coordinates:
        point = ogr.Geometry(ogr.wkbPoint25D)
        line.AddPoint(x, y, z)

    geomcol25D.AddGeometry(line)

    result_points25D = get_points_from_geometry(geomcol25D)

    # compare the expected points and the points returned by the function get_points_from_geometry
    compare_points_from_geometry(points, result_points25D)


def test_get_points_from_geometry_type_not_supported():
    """ Tests to confirm that the function get_points_from_geometry raises an error dur to some geometries not being supported (e.g : Triangle)
    """
    # type not supported
    not_supported = [ogr.Geometry(ogr.wkbMultiSurface), ogr.Geometry(
        ogr.wkbPolyhedralSurface), ogr.Geometry(ogr.wkbTriangle), ogr.Geometry(ogr.wkbMultiCurve)]
    for type in not_supported:
        with pytest.raises(NotImplementedError):
            get_points_from_geometry(type)


def test_get_representative_point_from_geometry():
    """ Tests the function get_representative_point_from_geometry
    """
    # create simple geometry
    coordinates = [(117, 712),
                   (116, 667),
                   (121, 641),
                   (122, 682),
                   (121, 721)]

    ring = ogr.Geometry(ogr.wkbLinearRing)
    polygon = ogr.Geometry(ogr.wkbPolygon)
    for x, y in coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(x, y)
        ring.AddPoint(x, y)
    ring.CloseRings()
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(*coordinates[0])

    polygon.AddGeometry(ring)

    result_points = get_representative_point_from_geometry(polygon)

    # compare the expected point and the point returned by the function get_representative_point_from_geometry
    compare_points_from_geometry([point], [result_points])

    # create multi geometry
    geomcol = ogr.Geometry(ogr.wkbGeometryCollection)

    line = ogr.Geometry(ogr.wkbLineString)
    coordinates_line = [(133, 58), (131, 56)]
    for x, y in coordinates_line:
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(x, y)
        line.AddPoint(x, y)

    geomcol.AddGeometry(point)
    geomcol.AddGeometry(line)

    result_points = get_representative_point_from_geometry(geomcol)

    # compare the expected point and the point returned by the function get_representative_point_from_geometry
    compare_points_from_geometry([point], [result_points])

    # create 25D geometry
    point25D = ogr.Geometry(ogr.wkbPoint25D)
    expected_point25D = ogr.Geometry(ogr.wkbPoint)
    coordinates = [(119, 64, 59)]
    for x, y, z in coordinates:
        point25D.AddPoint(x, y, z)
        expected_point25D.AddPoint(x, y)

    # compare the expected point and the point returned by the function get_representative_point_from_geometry
    result_point25D = get_representative_point_from_geometry(point25D)
    compare_points_from_geometry([expected_point25D], [result_point25D])


def test_are_geometries_2d_equal():
    """ Tests the function are_geometries_2d_equal by running it with its two possible
        outcomes False and True and also testing if the 25D convertion works
    """
    # True

    # create the geometries and add them to a list
    coordinates = [(141, 250),
                   (120, 410),
                   (130, 410)]
    geoms = []
    for x, y in coordinates:
        geom = ogr.Geometry(ogr.wkbPoint)
        geom.AddPoint(x, y)
        geoms.append(geom)

    # verify that the funtion says a point is equal to a point of same coordinates and type
    assert are_geometries_2d_equal(
        geoms[0], geoms[0], 0), "%s isn't in a %s distance to %s" % (geoms[0], 0, geoms[0])
    # verify that the funtion says a point is equal to a point of same coordinates and type even with a bigger max distance
    assert are_geometries_2d_equal(
        geoms[0], geoms[0], 200), "%s isn't in a %s distance to %s" % (geoms[0], 200, geoms[0])
    # verify that the funtion says a point is equal to a point of same type but different coordinate but with a correct max distance
    assert are_geometries_2d_equal(
        geoms[1], geoms[2], 10), "%s isn't in a %s distance to %s" % (geoms[1], 10, geoms[2])

    # False

    # create linestring
    geom_line = ogr.Geometry(ogr.wkbLineString)
    geom_line.AddPoint(*coordinates[0])

    # verify that the funtion doesn't say that a point is equal to a linestring
    assert not are_geometries_2d_equal(
        geoms[0], geom_line, 0), "%s isn't supposed to be in a %s distance to %s" % (geoms[0], 0, geom_line)
    # verify that the funtion doesn't say that a point is equal to a linestring even with a bigger max distance
    assert not are_geometries_2d_equal(
        geoms[0], geom_line, 200), "%s isn't supposed to be in a %s distance to %s" % (geoms[0], 200, geom_line)
    # verify that the funtion doesn't say that a point is equal to a point in another location
    assert not are_geometries_2d_equal(
        geoms[1], geoms[2], 5), "%s isn't supposed to be in a %s distance to %s" % (geoms[1], 5, geoms[2])

    # create two geometries but one is a 25D
    coordinates = [(214, 147, 45)]
    for x, y, z in coordinates:
        point = ogr.Geometry(ogr.wkbPoint)
        point25D = ogr.Geometry(ogr.wkbPoint25D)
        point.AddPoint(x, y)
        point25D.AddPoint(x, y, z)

    # assert that the 25D geometry has been converted
    assert are_geometries_2d_equal(
        point, point25D, 0), "%s isn't in a %s distance to %s" % (point, 0, point25D)


def test_are_geometries_2d_equal_type_not_supported():
    """ Tests to confirm that the function are_geometries_2d_equal raises an error due to some geometries type not being supported (e.g : Triangle)
    """
    # create a list that contains some geometry types that are not supported
    not_supported = [ogr.Geometry(ogr.wkbMultiSurface), ogr.Geometry(
        ogr.wkbPolyhedralSurface), ogr.Geometry(ogr.wkbTriangle), ogr.Geometry(ogr.wkbMultiCurve)]
    for type in not_supported:
        with pytest.raises(NotImplementedError):
            get_points_from_geometry(type)


def test_get_feature_verbose_name():
    """ Tests the function get_feature_verbose_name
    """
    verbose = "layer:-1"
    layer_name = "layer"
    feature_defn = ogr.FeatureDefn()
    feature = ogr.Feature(feature_defn)

    # compare the verbose expected and the verbose returned by the function
    assert verbose == get_feature_verbose_name(layer_name, feature)


def test_exterior_ring():
    """ Tests the function exterior_ring
    """
    # simple polygon
    coordinates = [(117, 712, 124),
                   (116, 667, 125),
                   (121, 641, 122),
                   (122, 682, 121),
                   (121, 721, 121)]
    # create a polygon
    ring = ogr.Geometry(ogr.wkbLinearRing)
    polygon = ogr.Geometry(ogr.wkbPolygon)
    for x, y, z in coordinates:
        ring.AddPoint(x, y)
    ring.CloseRings()
    polygon.AddGeometry(ring)

    result = exterior_ring(polygon)

    # compare the expected ring and the ring returned by the function
    compare_points_from_geometry([ring], [result])

    # polygon25D
    # create a polygon25D
    ring25D = ogr.Geometry(ogr.wkbLinearRing)
    polygon25D = ogr.Geometry(ogr.wkbPolygon25D)
    for x, y, z in coordinates:
        ring25D.AddPoint(x, y, z)
    ring25D.CloseRings()
    polygon25D.AddGeometry(ring25D)

    result_25D = exterior_ring(polygon25D)

    # compare the expected ring and the ring returned by the function
    compare_points_from_geometry([ring25D], [result_25D])

    # polygon with hole
    inner_ring_coordinates = [(114, 691),
                              (114, 648),
                              (119, 648),
                              (119, 691)]

    inner_ring = ogr.Geometry(ogr.wkbLinearRing)
    polygon_with_hole = ogr.Geometry(ogr.wkbPolygon)

    polygon_with_hole.AddGeometry(ring)

    # create a polygon with hole
    for x, y in inner_ring_coordinates:
        inner_ring.AddPoint(x, y)
    inner_ring.CloseRings()
    polygon_with_hole.AddGeometry(inner_ring)

    result_ring_polygon_with_hole = exterior_ring(polygon_with_hole)

    # compare the expected ring and the ring returned by the function
    compare_points_from_geometry([ring], [result_ring_polygon_with_hole])


def test_as_multipoint():
    """ Tests the function as_multipoint by converting a point, a point25D 
        and a linestring to a multi point with the function hoping to raise an error for the linestring
    """
    coordinates = [(120, 475, 651)]
    for x, y, z in coordinates:
        # create a point and multi point
        point = ogr.Geometry(ogr.wkbPoint)
        expected_point = ogr.Geometry(ogr.wkbMultiPoint)
        point.AddPoint(x, y)
        expected_point.AddGeometry(point)

        # create a point25D and multi point25D
        point25D = ogr.Geometry(ogr.wkbPoint25D)
        expected_point25D = ogr.Geometry(ogr.wkbMultiPoint25D)
        point25D.AddPoint(x, y, z)
        expected_point25D.AddGeometry(point25D)

        # create a linestring
        line = ogr.Geometry(ogr.wkbLineString)
        line.AddPoint(x, y)

    # compare the expected multi point to the multi point returned by the function
    assert expected_point.ExportToWkt() == as_multipoint(point).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_point, as_multipoint(point))

    # compare the expected multi point25D to the multi point25D returned by the function
    assert expected_point25D.ExportToWkt() == as_multipoint(point25D).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_point25D, as_multipoint(point25D))

    # expect an error
    with pytest.raises(UnboundLocalError, match=r".*multi_geom.*"):
        as_multipoint(line)


def test_as_multilinestring():
    """ Tests the function as_multilinestring by converting a linestring, a linestring25D 
        and a point to a multi linestring with the function hoping to raise an error for the point
    """
    # create linstring and multi linestring
    coordinates = [(120, 475, 651)]
    line = ogr.Geometry(ogr.wkbLineString)
    expected_line = ogr.Geometry(ogr.wkbMultiLineString)
    for x, y, z in coordinates:
        line.AddPoint(x, y)
    expected_line.AddGeometry(line)

    # create linstring25D and multi linestring25D
    line25D = ogr.Geometry(ogr.wkbLineString25D)
    expected_line25D = ogr.Geometry(ogr.wkbMultiLineString25D)
    for x, y, z in coordinates:
        line25D.AddPoint(x, y, z)
    expected_line25D.AddGeometry(line25D)

    # create point
    point = ogr.Geometry(ogr.wkbPoint)
    for x, y, z in coordinates:
        point.AddPoint(x, y)

    # compare the expected multi linestring to the multi linestring returned by the function
    assert expected_line.ExportToWkt() == as_multilinestring(line).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_line, as_multilinestring(line))

    # compare the expected multi linestring25D to the multi linestring25D returned by the function
    assert expected_line25D.ExportToWkt() == as_multilinestring(line25D).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_line25D, as_multilinestring(line25D))

    # expect an error
    with pytest.raises(UnboundLocalError, match=r".*multi_geom.*"):
        as_multilinestring(point)


def test_as_polygon():
    """ Tests the function as_polygon by converting a linestring, a linestring25D 
        and a point with the function hoping to raise an error for the point
    """
    # create polygon
    coordinates = [(120, 475, 651), (250, 654, 410)]
    ring = ogr.Geometry(ogr.wkbLinearRing)
    line = ogr.Geometry(ogr.wkbLineString)
    expected_polygon = ogr.Geometry(ogr.wkbPolygon)
    for x, y, z in coordinates:
        line.AddPoint(x, y)
        ring.AddPoint(x, y)
    ring.CloseRings()
    expected_polygon.AddGeometry(ring)

    # create polygon25D
    ring25D = ogr.Geometry(ogr.wkbLinearRing)
    line25D = ogr.Geometry(ogr.wkbLineString25D)
    expected_polygon25D = ogr.Geometry(ogr.wkbPolygon25D)
    for x, y, z in coordinates:
        line25D.AddPoint(x, y, z)
        ring25D.AddPoint(x, y, z)
    ring25D.CloseRings()
    expected_polygon25D.AddGeometry(ring25D)

    # create point
    point = ogr.Geometry(ogr.wkbPoint)
    for x, y, z in coordinates:
        point.AddPoint(x, y)

    # compare the expected polygon to the polygon returned by the function
    assert expected_polygon.ExportToWkt() == as_polygon(line).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_polygon, as_polygon(line))

    # compare the expected polygon25D to the polygon25D returned by the function
    assert expected_polygon25D.ExportToWkt() == as_polygon(line25D).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_polygon25D, as_polygon(line25D))

    # expect an error
    with pytest.raises(UnboundLocalError, match=r".*polygon.*"):
        as_polygon(point)


def test_as_linestring():
    """ Tests the function as_linestring by converting a point and a point25D with the function
    """
    # initiate the arg for the function
    block_segment_length = 3
    xscale = 1
    rotation = 2

    # create point
    coordinates = [(120, 475, 631), (122.998172481057, 475.104698490107, 631)]
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(*coordinates[0][:-1])

    # create line
    expected_line = ogr.Geometry(ogr.wkbLineString)
    for x, y, z in coordinates:
        expected_line.AddPoint(x, y)

    # create point25D
    point25D = ogr.Geometry(ogr.wkbPoint25D)
    point25D.AddPoint(*coordinates[0])

    # create linestring25D
    expected_line25D = ogr.Geometry(ogr.wkbLineString25D)
    for x, y, z in coordinates:
        expected_line25D.AddPoint(x, y, z)

    # compare the expected linestring to the linestring returned by the function
    assert expected_line.ExportToWkt() == as_linestring(point, block_segment_length, xscale, rotation).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_line, as_linestring(point, block_segment_length, xscale, rotation))

    # compare the expected linestring25D to the linestring25D returned by the function
    assert expected_line25D.ExportToWkt() == as_linestring(point25D, block_segment_length, xscale, rotation).ExportToWkt(
    ), "%s isn't equal to %s" % (expected_line25D, as_linestring(point25D, block_segment_length, xscale, rotation))


def test_class_OGRWriter():
    """ Tests the class OGRWriter by creating a shp file that can be used to run tests
    """
    tmp_dir = tempfile.gettempdir()

    os.mkdir(tmp_dir + "/shp")

    #  creating a point geometry
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(54, 37)

    # set up the shapefile
    driver = ogr.GetDriverByName("ESRI Shapefile")
    ds = driver.CreateDataSource(tmp_dir + "/shp/test.shp")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    # create one layer
    layer = ds.CreateLayer("point", srs, ogr.wkbPoint)

    # create a OGRWriter
    writer = OGRWriter(layer)

    # add an ID field
    idField = ogr.FieldDefn("test_name", ogr.OFTInteger)
    layer.CreateField(idField)

    # create the feature and set values
    featureDefn = layer.GetLayerDefn()
    feature = ogr.Feature(featureDefn)
    feature.SetGeometry(point)
    feature.SetField("test_name", 1)
    layer.CreateFeature(feature)

    # test that the method of the class are returning what is expected
    assert writer.add_feature(feature), "%s cannot add %s" % (writer, feature)

    assert writer.CreateFeature(
        feature), "%s cannot add %s" % (writer, feature)

    assert writer.has_field_defn("test_name")

    assert not writer.has_field_defn("test_false_name")

    assert writer.GetLayerDefn().IsSame(featureDefn), "%s isn't equal to %s" % (
        writer.GetLayerDefn(), featureDefn)

    # remove shp
    shutil.rmtree(tmp_dir + "/shp")
