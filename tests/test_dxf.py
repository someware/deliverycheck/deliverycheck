import math
from deliverycheck.util.formats.dxf import *
from helpers.utils import assert_get_ogr_geometry
from ezdxf.gfxattribs import GfxAttribs
from ezdxf.enums import TextEntityAlignment
import pytest


### Simple Entities ###

def test_get_ogr_geometry_simple_entities():
    """ Tests the function get_ogr_geometry for entities that are simple which means they are 2D
        The entities are :
            Point
            Circle
            Line
            Ellipse
    """

    # create drawing and modelspace
    doc = ezdxf.new()  # DXF version R2013 by default
    doc.layers.new("ENTITY")
    msp = doc.modelspace()
    attribs = GfxAttribs(layer="ENTITY")

    # create point
    expected_point = ogr.Geometry(ogr.wkbPoint)
    coordinates_point = [(117, 186)]
    for x, y in coordinates_point:
        point = msp.add_point((x, y), dxfattribs=attribs)
        expected_point.AddPoint(x, y)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(point, expected_point)

    # create line
    expected_line = ogr.Geometry(ogr.wkbLineString)
    coordinates_line = [(120, 451), (121, 462)]
    line = msp.add_line(*coordinates_line, dxfattribs=attribs)
    for x, y in coordinates_line:
        expected_line.AddPoint(x, y)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(line, expected_line)

    # create circle
    expected_circle = ogr.Geometry(ogr.wkbPoint)
    coordinates_circle = [(123, 190)]
    for x, y in coordinates_circle:
        circle = msp.add_circle((x, y), radius=3, dxfattribs=attribs)
        expected_circle.AddPoint(x, y)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(circle, expected_circle)

    # create ellipse
    ellipse = msp.add_ellipse((10, 10), major_axis=(
        5, 0), ratio=0.5, start_param=0, end_param=math.pi, dxfattribs=attribs)

    expected_ellipse = ogr.CreateGeometryFromWkt('LINESTRING (15 10 0,14.9797745430644 10.2246360947443 0,14.9165268420829 10.4549076310304 0,14.8069947909352 10.6878955371134 0,14.6489415053122 10.9202367739047 0,14.4414200341488 11.14823648264 0,14.1849775560933 11.3680243834629 0,13.8817659378742 11.5757453159981 0,13.5355339059327 11.7677669529664 0,13.1514906319962 11.9408829689371 0,12.7360487669257 12.0924887780466 0,12.29647296528 12.2207100170744 0,11.8404735478094 12.3244707526561 0,11.3757910742268 12.4034973954676 0,10.9098152620607 12.4582634210414 0,10.4492721894885 12.4898872715322 0,10.0 12.5 0,9.55072781051148 12.4898872715322 0,9.09018473793928 12.4582634210414 0,8.62420892577316 12.4034973954676 0,8.15952645219064 12.3244707526561 0,7.70352703471998 12.2207100170744 0,7.26395123307429 12.0924887780466 0,6.84850936800385 11.9408829689371 0,6.46446609406726 11.7677669529664 0,6.11823406212578 11.5757453159981 0,5.81502244390674 11.3680243834629 0,5.55857996585124 11.14823648264 0,5.35105849468785 10.9202367739047 0,5.19300520906484 10.6878955371134 0,5.08347315791713 10.4549076310304 0,5.02022545693561 10.2246360947443 0,5 10 0)')

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(ellipse, expected_ellipse)


def test_get_ogr_geometry_R2000():
    """ Tests the function get_ogr_geometry for entities that has a drawing recommended in the DXF version of AutoCAD 2000 or later
        The entities are :
            Spline
            LWpolyline
            Hatch
            mpolygon
    """
    # create drawing and modelsapce
    doc = ezdxf.new("R2000")  # entities require DXF R2000 or later
    msp = doc.modelspace()

    # create an open lwpolyline
    points = [(0, 0), (3, 0), (6, 3), (6, 6)]
    polyline = msp.add_lwpolyline(points)

    expected_polyline = ogr.Geometry(ogr.wkbLineString)
    for x, y in points:
        expected_polyline.AddPoint(x, y)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(polyline, expected_polyline)

    # create closed lwpolyline
    points_closed = [(90, 0), (90, 50), (140, 50), (140, 0)]
    polyline_closed = msp.add_lwpolyline(points_closed)
    polyline_closed.close(True)

    expected_polyline_closed = ogr.Geometry(ogr.wkbPolygon)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for x, y in points_closed:
        ring.AddPoint(x, y)
    ring.CloseRings()
    expected_polyline_closed.AddGeometry(ring)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(polyline_closed, expected_polyline_closed)

    # create spline
    fit_points = [(10, 20, 30), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
    spline = msp.add_rational_spline(fit_points, weights=[3, 1, 1, 1])
    spline.closed = True

    expected_spline = ogr.CreateGeometryFromWkt('POLYGON ((10 20 30,15.8882858959386 23.7794930446286 29.7637892526185,21.9939966257258 27.6187621189182 29.5238888641081,28.322191317523 31.519003724888 29.2802730851607,34.8779642663779 35.4814293448836 29.032918245804,41.666439246275 39.5072647926081 28.7818029086667,48.6927633618487 43.5977494951865 28.5269080275825,55.9621004227809 47.7541357033364 28.2682171114382,63.4796238244514 51.9776876267749 28.0057163931403,71.2505089191162 56.269680492074 27.7393950035391,79.2799248627586 60.6313995202868 27.469245150111,87.5730259238115 65.0641388217972 27.1952623001605,96.134942241189 69.5692002060187 26.9174453682584,104.970770020522 74.1478919037569 26.6357969075884,114.085561159155 78.8015272002909 26.3503233048235,123.484312292367 83.5314229774916 26.0610349781033,133.171953255426 88.338898163606 25.7679465776294,143.15333495847 93.2252720896844 25.4710771883369,153.433216673895 98.1918627520197 25.1704505340447,164.016252738836 103.239984980404 24.866095182422,174.90697867857 108.370948512491 24.5580447500485,186.109796760143 113.586055975078 24.2463381067804,197.628960989323 118.886600773712 23.9310195785661,209.468561568053 124.273864892637 23.612139147793,221.632508833922 129.749116607774 23.2897526501767,234.124516707818 135.31360811618 22.9639219671387,246.948085680809 140.968573086155 22.6347152125526,260.10648537648 146.715224133029 22.3022069126745,273.602736730295 152.554750226499 21.9664781780124,287.439593833199 158.488314036283 21.6276168658287,301.619525492419 164.517049223829 21.2857177319165,316.14469656836 170.642057688748 20.9408825702397,331.016949152542 176.864406779661 20.593220338983,346.237783657607 183.185126480167 20.2428472715196,361.80833989655 189.605206581664 19.8898869707742,377.729378234399 196.125593855795 19.5344704854412,394.001260901545 202.747189240307 19.176736366502,410.623933563688 209.47084505315 18.8168307024899,427.596907248973 216.297362250615 18.4549071319595,444.919240738048 223.227487746251 18.0911268316468,462.589523527671 230.261911808227 17.7256584788399,480.605859482777 237.401265553615 17.3586781865397,498.965851295713 244.646118558831 16.9903694100551,517.66658587443 251.996976606137 16.6209228237669,536.704620783778 259.454279586664 16.2505361668941,556.075971865563 267.018399580835 15.8794140572205,575.776102163576 274.68963913737 15.507767771872,616.141732283465 290.354330708661 14.7637795275591,657.753832901765 306.44933321428 14.0203843669081,700.555436045261 322.974443918599 13.279481994579,744.480231118448 339.928270738754 12.5430458109781,789.452646741088 357.308246308966 11.8131076701476,835.388077977775 375.110665892484 11.0917405876503,858.688105672489 384.168838524855 10.7349255855666,882.193266657276 393.330750809973 10.3810395830399,905.890639787333 402.5956523898 10.0303444871897,929.766838912873 411.962738535049 9.68310091072936,953.808038234554 421.431154450458 9.33956745800525,978 431 9,1002.32810446142 440.668334843908 8.66465094180528,1026.77738199586 450.435183976427 8.33376848622741,1051.33254727151 460.299543650342 8.00759589732172,1075.97803532567 470.260387671441 7.6863707677514,1100.69803940333 480.316674043327 7.37032429393399,1125.47655038884 490.467351940358 7.05968056297078,1150.29739764899 500.711368983983 6.75465585535056,1175.14429109159 511.047678795483 6.45545796737767,1187.57239239424 516.250120051385 6.3081065357255,1200.00086423238 521.475248796042 6.16228555719531,1212.42765605526 526.722939428647 6.01801833340097,1224.85071805247 531.993068223276 5.8753275181586,1237.26800255179 537.285513588958 5.73423509965153,1249.67746542035 542.600156331814 5.59476238315825,1262.07706746735 547.936879919022 5.4569299743679,1274.46477584629 553.295570744309 5.3207577633066,1286.83856545518 558.67611839472 5.18626490889732,1299.19642033242 564.078415918363 5.05346982417435,1311.53633504687 569.502360092873 4.92239016217271,1323.85631608008 574.947851694309 4.7930428025113,1336.15438319881 580.414795766215 4.66544383868757,1348.42857081609 585.903101888572 4.53960856610002,1360.67692933909 591.412684446368 4.41555147081354,1372.89752650177 596.943462897527 4.29328621908127,1385.0884486809 602.495362039926 4.1728256476353,1397.24780219351 608.068312277258 4.05418175475682,1409.37371457413 613.662249883471 3.93736569213524,1421.46433583033 619.277117265549 3.82238775752414,1433.51783967476 624.912863224402 3.70925738820034,1445.53242473245 630.569443213615 3.59798315523107,1457.50631572152 636.246819595853 3.48857275855255,1469.43776460627 641.944961896698 3.38103302286198,1481.32505172093 647.663847055707 3.27536989432317,1493.16648686311 653.40345967451 3.17158843808479,1504.96041035539 659.16379226175 3.06969283660862,1516.7051940741 664.944845474692 2.96968638880377,1528.39924244411 670.746628357344 2.87157150996134,1540.04099339864 676.569158574922 2.7753497324825,1551.62891930302 682.412462644544 2.68102170739197,1563.16152784169 688.276576161988 2.58858720662678,1574.6373628675 694.161544024436 2.49804512608979,1586.05500521259 700.067420649069 2.40939348945523,1597.41307346033 705.99427018744 2.32262945271308,1608.71022467756 711.942166735542 2.23774930943733,1619.94515510671 717.911194539505 2.15474849676232,1631.11660081748 723.901448196872 2.07362160205016,1642.2233383176 729.91303285342 1.99436237023113,1653.26418512242 735.946064395489 1.91696371179805,1664.23800028331 742.00066963783 1.84141771143469,1675.14368487454 748.076986506953 1.76771563725719,1685.98018243876 754.175164220005 1.69584795064699,1696.74647939111 760.295363459205 1.62580431665257,1707.44160538202 766.437756541872 1.55757361493715,1718.06463361896 772.602527586115 1.49114395124816,1728.61468114733 778.789872672246 1.42650266938451,1739.09090909091 785.0 1.36363636363636,1749.49252285223 791.233130041656 1.30253089167237,1759.81877227326 797.48949569117 1.24317138784848,1770.0689517571 803.769342409439 1.18554227691223,1780.24240035107 810.072928365824 1.12962728807634,1790.33850179201 816.400524576079 1.07540946943485,1800.35668451441 822.752415036837 1.02287120269533,1810.2964216221 829.128896856832 0.971994218200306,1820.15723082442 835.530280385006 0.922759610211036,1829.93867433752 841.956889335724 0.875147852427059,1839.64035875195 848.409060911258 0.829138813714652,1849.26193486724 854.887145921779 0.784711774017758,1858.80309749454 861.391508903046 0.741845440425005,1868.26358522843 867.922528232037 0.700517963366681,1877.6431801887 874.480596240748 0.660706952915849,1886.9417077335 881.066119328395 0.622389495168086,1896.15903614458 887.679518072289 0.585542168674699,1905.29507628617 894.321227337614 0.550141060904704,1914.34978123825 900.991696386401 0.516161784711283,1923.32314590566 907.69138898595 0.483579494778904,1932.21520660408 914.42078351699 0.452368904027854,1941.02604062415 921.180373081849 0.422504299953413,1949.75576577488 927.970665612944 0.393959560877526,1958.40453990758 934.79218398187 0.366708172091399,1966.97256042159 941.645466109395 0.340723241868072,1975.46006375292 948.531065076675 0.315977517324664,1983.8673248471 955.449549237992 0.292443400114645,1992.19465661745 962.401502335335 0.270092961931145,2000.44240938991 969.387523615147 0.248897959803036,2008.61097033582 976.408227947563 0.228829851166159,2016.70076289358 983.464245948463 0.209859808692829,2024.71224618065 990.556224104685 0.191958734863426,2032.64591439689 997.684824902724 0.175097276264591,2040.50229622046 1004.85072696127 0.159245837599287,2044.40167922449 1008.44788247627 0.151689564451148,2048.28195419749 1012.0546251679 0.144374595394652,2052.14319664859 1015.67104446231 0.137297169040436,2055.98548412652 1019.29723082036 0.130453511394334,2059.80889620015 1022.93327574231 0.123839836257755,2063.61351443899 1026.57927177263 0.117452345622646,2067.39942239373 1030.2353125048 0.111287230060974,2071.16670557667 1033.90149258631 0.105340669108621,2074.91545144224 1037.57790772366 0.0996088316436319,2078.64574936737 1041.26465468755 0.0940878762587162,2082.35769063204 1044.96183131805 0.0887739516279489,2086.05136839971 1048.66953652998 0.0836631968675785,2089.72687769779 1052.38787031837 0.0787517418908817,2093.38431539821 1056.11693376394 0.0740357077569923,2097.02378019793 1059.85682903884 0.0695112070136401,2100.64537259955 1063.60765941238 0.065174344033735,2104.24919489198 1067.36952925692 0.0610212153457347,2107.8353511311 1071.1425440539 0.0570479099577391,2111.40394712055 1074.92681039997 0.0532505096752532,2114.95509039257 1078.72243601323 0.0496250894125641,2118.48889018892 1082.52952973963 0.0461677174976817,2122.00545744186 1086.34820155949 0.042874455970792,2125.50490475528 1090.17856259414 0.0397413608761756,2128.98734638585 1094.02072511273 0.0367644825475462,2132.45289822439 1097.87480253915 0.0339398658867663,2135.9016777772 1101.74090945912 0.0312635506358971,2139.33380414765 1105.61916162737 0.0287315716425453,2142.74939801783 1109.50967597508 0.0263399591184677,2146.14858163032 1113.41257061734 0.0240847388913998,2149.53147877013 1117.32796486091 0.0219619326500732,2152.89821474678 1121.25597921198 0.0199675581823913,2156.2489163765 1125.19673538424 0.018097629606733,2159.58371196462 1129.15035630703 0.0163481575963554,2162.90273128812 1133.11696613371 0.0147151495968707,2166.2061055783 1137.09669025013 0.0131946100367702,2169.49396750371 1141.08965528337 0.0117825405309741,2172.76645115317 1145.09598911062 0.0104749400773837,2176.02369201901 1149.1158208682 0.00926780524641683,2179.26582698052 1153.14928096084 0.00815713036350708,2182.49299428756 1157.1965010711 0.00713890768454932,2185.7053335444 1161.25761416905 0.00620912756427567,2188.90298569373 1165.33275452202 0.00536377861754685,2192.08609300089 1169.42205770472 0.00459884787354522,2195.25479903835 1173.52566060941 0.00391032092285719,2198.40924867036 1177.64370145641 0.0032941820574336,2201.54958803784 1181.7763198047 0.00274641440341776,2207.78852683726 1190.0858540001 0.00183992015211437,2213.97280953802 1198.45540686037 0.00115868445922814,2220.10364974067 1206.88614418888 0.000670542261864813,2226.18228010345 1215.37925430867 0.000343320134581493,2232.20995191709 1223.93594845884 0.000144838820618611,2238.18793469626 1232.55746120695 4.29153033110587E-05,2244.11751578837 1241.24505087792 5.36441739029677E-06,2250 1250 0,10 20 30))')

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(spline, expected_spline)

    # create a one geometry Hatch
    expected_hatch_one_geom = ogr.Geometry(ogr.wkbPolygon)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    hatch_simple = msp.add_hatch()

    coordinates = [(120, 45), (117, 14)]
    for x, y in coordinates:
        ring.AddPoint(x, y)
    ring.CloseRings()
    hatch_simple.paths.add_polyline_path(coordinates, is_closed=True)
    expected_hatch_one_geom.AddGeometry(ring)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(hatch_simple, expected_hatch_one_geom)

    # create a multi geometry Hatch
    expected_hatch_multi_geom = ogr.Geometry(ogr.wkbGeometryCollection)
    polygon = ogr.Geometry(ogr.wkbPolygon)
    multi_line = ogr.Geometry(ogr.wkbMultiLineString)
    polygon.AddGeometry(ring)

    line = ogr.Geometry(ogr.wkbLineString)
    hatch_multi = msp.add_hatch()
    edge_path = hatch_multi.paths.add_edge_path()
    hatch_multi.paths.add_polyline_path(coordinates, is_closed=True)

    coordinates_line = [(122, 145), (117, 14)]
    for x, y in coordinates_line:
        line.AddPoint(x, y)
    edge_path.add_line(*coordinates_line)
    multi_line.AddGeometry(line)
    expected_hatch_multi_geom.AddGeometry(multi_line)
    expected_hatch_multi_geom.AddGeometry(polygon)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(hatch_multi, expected_hatch_multi_geom)

    # create a one geometry MPolygon
    expected_hatch_one_geom = ogr.Geometry(ogr.wkbPolygon)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    coordinates = [(120, 45), (117, 14)]
    for x, y in coordinates:
        ring.AddPoint(x, y)
    ring.CloseRings()
    expected_hatch_one_geom.AddGeometry(ring)
    hatch_simple = msp.add_mpolygon()
    hatch_simple.paths.add_polyline_path(coordinates, is_closed=True)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(hatch_simple, expected_hatch_one_geom)

    # create a multi geometry MPolygon
    expected_hatch_multi_geom = ogr.Geometry(ogr.wkbGeometryCollection)
    polygon = ogr.Geometry(ogr.wkbPolygon)
    multi_line = ogr.Geometry(ogr.wkbMultiLineString)
    line = ogr.Geometry(ogr.wkbLineString)
    coordinates_multi = [(122, 145), (117, 14)]
    for x, y in coordinates_multi:
        line.AddPoint(x, y)
    multi_line.AddGeometry(line)
    polygon.AddGeometry(ring)
    expected_hatch_multi_geom.AddGeometry(multi_line)
    expected_hatch_multi_geom.AddGeometry(polygon)

    hatch_multi = msp.add_mpolygon()
    edge_path = hatch_multi.paths.add_edge_path()
    hatch_multi.paths.add_polyline_path(coordinates, is_closed=True)
    edge_path.add_line(*coordinates_multi)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(hatch_multi, expected_hatch_multi_geom)


def test_get_ogr_geometry_Text():
    """Tests the function get_ogr_geometry for the entity text
    """
    # create drawing and modelspace
    # TEXT entity is a DXF primitive and is supported in all DXF versions
    doc_text = ezdxf.new("R12", setup=True)
    msp_text = doc_text.modelspace()

    # create text
    expected_text = ogr.Geometry(ogr.wkbPoint)
    coordinates = [(120, 465)]
    for x, y in coordinates:
        expected_text.AddPoint(x, y)
    text = msp_text.add_text("A Simple Text").set_placement(*coordinates)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(text, expected_text)


def test_get_ogr_geometry_R2007():
    """Tests the function get_ogr_geometry for the entity Mtext that has a drawing recommended in the DXF version of AutoCAD 12 or later
    """
    # create drawing and modelspace
    # MTEXT requires DXF 2007 to use all features
    doc_Mtext = ezdxf.new("R2007", setup=True)
    msp_Mtext = doc_Mtext.modelspace()

    # create MText
    expected_Mtext = ogr.Geometry(ogr.wkbPoint)
    coordinates = [(120, 465)]
    for x, y in coordinates:
        expected_Mtext.AddPoint(x, y)

    mtext = msp_Mtext.add_mtext("A Simple MText").set_location(*coordinates)

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(mtext, expected_Mtext)


def test_get_ogr_geometry_R2010():
    """ Tests the function get_ogr_geometry for entities that has a drawing recommended in the DXF version of AutoCAD 2010 or later
        The entities are :
            Insert
            Dimension
    """
    # create drawing and modelspace
    # DIMENSION is DXF 2010 and INSERT require DXF 2000 or later
    doc = ezdxf.new('R2010')
    insert = doc.blocks.new("INSERT")
    msp = doc.modelspace()

    # create insert
    expected = ogr.Geometry(ogr.wkbPoint)
    coordinates = [(120, 14)]
    for x, y in coordinates:
        expected.AddPoint(x, y)
        insert = msp.add_blockref('INSERT', (x, y))

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(insert, expected)

    # create dimension
    expected_dimension = ogr.Geometry(ogr.wkbPoint)
    coordinates = [(120, 465)]
    for x, y in coordinates:
        expected_dimension.AddPoint(x, y)
        dim = msp.add_linear_dim(base=(3, 0), p1=(
            10, 7), p2=(3, 0)).render().dimension
        dim = msp.add_blockref(dim.dxf.geometry, (x, y))

    # compare the expected geometry to the point returned by the function
    assert_get_ogr_geometry(dim, expected_dimension)


def test_get_ogr_geometry_not_implemented_edges():
    """ Tests to confirm that the function get_ogr_geometry raises an error due to arcEdge and splineEdge not being implemented 
        for the hatch entity
    """
    # create drawing and modelspace
    doc = ezdxf.new("R2000")  # HATCH requires DXF R2000 or later
    msp = doc.modelspace()

    # create hatch with arc edges
    coordinates = [(120, 44)]
    for x, y in coordinates:
        msp.add_arc((x, y), 1, 10, 20)
        hatch_arc = msp.add_hatch()
        arc_edges = hatch_arc.paths.add_edge_path()
        arc_edges.add_arc((x, y), 1, 10, 20)

    # create hatch with spline edges
    fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
    msp.add_spline(fit_points)
    hatch_spline = msp.add_hatch()
    spline_edges = hatch_spline.paths.add_edge_path()
    spline_edges.add_spline(fit_points)

    # no exception should be raise
    get_ogr_geometry(hatch_arc, 1)
    get_ogr_geometry(hatch_spline, 1)


def test_get_ogr_geometry_type_not_supported():
    """ Tests to confirm that the function get_ogr_geometry raises an error due to some type of entities not being implemented (e.g : mesh)
    """
    # create drawing and modelspace
    doc = ezdxf.new("R2000")  # MESH requires DXF R2000 or later
    msp = doc.modelspace()

    # create mesh
    mesh = msp.add_mesh()

    # expect an error
    with pytest.raises(NotImplementedError):
        get_ogr_geometry(mesh, 1)


def test_get_insert_block_name():
    """ Tests the function get_insert_block_name
    """
    # create drawing and modelspace
    doc = ezdxf.new('R2010')  # INSERT requires DXF 2000 or later
    msp = doc.modelspace()

    # create block
    insert = doc.blocks.new("INSERT")
    expected = "INSERT"

    insert = msp.add_blockref('INSERT', (120, 14))

    # compare the expected block name to the block name returned by the function
    assert expected == get_insert_block_name(insert), "%s ins't equal to %s" % (
        expected, get_insert_block_name(insert))


def test_get_entity_type_id():
    """ Tests the function get_entity_type_id
    """
    # create a drawing and a modelspace
    doc = ezdxf.new('R2010')  # INSERT requires DXF 2000 or later
    msp = doc.modelspace()

    # create block
    doc.blocks.new('BLOCK')
    insert = msp.add_blockref('type_id', (120, 14))

    expected_insert = "type_id"

    # compare the expected entity id and the entity id returned by the function
    assert expected_insert == get_entity_type_id(
        insert), "%s isn't equal to %s" % (expected_point, get_entity_type_id(insert))

    # create a drawing and a modelspace
    doc_point = ezdxf.new()
    doc_point.layers.new("ENTITY")
    msp_point = doc_point.modelspace()

    # create point
    expected_point = "BYLAYER"
    point = msp_point.add_point((117, 186))

    # compare the expected entity id and the entity id returned by the function
    assert expected_point == get_entity_type_id(point), "%s isn't equal to %s" % (
        expected_point, get_entity_type_id(point))

    # Hatch
    # create a drawing and a modelspace
    doc_hatch = ezdxf.new("R2000")  # HATCH requires DXF 2000 or later
    msp_hatch = doc_hatch.modelspace()

    # create hatch
    hatch = msp_hatch.add_hatch()
    hatch.paths.add_polyline_path([(120, 45), (117, 14)], is_closed=True)
    hatch.set_pattern_fill("SWAMP", scale=0.5)
    expected = "BYLAYER"

    # compare the expected entity id and the entity id returned by the function
    assert expected == get_entity_type_id(hatch), "%s isn't equal to %s. get_entity_type_id should return %s" % (
        expected, get_entity_type_id(hatch), hatch.dxf.pattern_name)


def test_get_entity_verbose_name():
    """ Tests the function get_entity_verbose_name
    """
    # create drawing and modelspace
    doc_point = ezdxf.new()
    doc_point.layers.new("ENTITY")
    msp_point = doc_point.modelspace()

    # create point
    point = msp_point.add_point((117, 186))
    expected = "ENTITY:POINT:BYLAYER:30"

    # compare the expected verbose to the verbose returned by the function
    assert expected == get_entity_verbose_name(point, "ENTITY"), "%s isn't equal to %s" % (
        expected, get_entity_verbose_name(point, "ENTITY"))


def test_get_entity_aci_rgb_color():
    """ Tests the function get_entity_aci_rgb_color
    """
    # create drawing and modelspace
    doc_Mtext = ezdxf.new("R2007", setup=True)
    msp_Mtext = doc_Mtext.modelspace()

    # create mtext with rgb color
    rgb_color = (255, 170, 170)
    expected = (None, rgb_color)
    mtext = msp_Mtext.add_mtext("A Simple MText")
    mtext.rgb = rgb_color

    # compare the expected rgb color to the rgb color returned by the function
    assert expected == get_entity_aci_rgb_color(doc_Mtext, mtext), "%s isn't the color %s" % (
        expected, get_entity_aci_rgb_color(doc_Mtext, mtext))

    # create drawing and modelspace
    doc_point = ezdxf.new()
    doc_point.layers.new("ENTITY").color = 31
    msp_point = doc_point.modelspace()

    # create mtext with aci color
    attribs = GfxAttribs(layer="ENTITY")
    point = msp_point.add_point((117, 186), dxfattribs=attribs)
    expected_point = (31, (255, 191, 127))

    # compare the expected aci color to the aci color returned by the function
    assert expected_point == get_entity_aci_rgb_color(doc_point, point), "%s isn't the color %s" % (
        expected_point, get_entity_aci_rgb_color(doc_point, point))


def test_get_entity_rgb_color():
    """ Tests the function get_entity_rgb_color
    """
    # create drawing and modelspace
    # MTEXT requires DXF 2007 to use all features
    doc_Mtext = ezdxf.new("R2007", setup=True)
    msp_Mtext = doc_Mtext.modelspace()
    mtext = msp_Mtext.add_mtext("A Simple MText")

    # create mtext with rgb color
    rgb_color = (255, 170, 170)
    expected = rgb_color
    mtext.rgb = rgb_color

    # compare the expected rgb color to the rgb color returned by the function
    assert expected == get_entity_rgb_color(doc_Mtext, mtext), "%s isn't the color %s" % (
        expected, get_entity_rgb_color(doc_Mtext, mtext))


def test_get_entity_text():
    """ Tests the function get_entity_text
    """
    # create drawing and modelspace
    # TEXT entity is a DXF primitive and is supported in all DXF versions
    doc_text = ezdxf.new("R12", setup=True)
    msp_text = doc_text.modelspace()

    # create text
    text_text = "Text"
    expected_text = text_text
    text = msp_text.add_text(text_text)

    # compare the expected text to the text returned by the function
    assert expected_text == get_entity_text(text), "%s isn't the color %s" % (
        expected_text, get_entity_text(text))

    # create drwing and modelspace
    # MTEXT requires DXF 2007 to use all features
    doc_Mtext = ezdxf.new("R2007", setup=True)
    msp_Mtext = doc_Mtext.modelspace()

    # create mtext
    text_mtext = "MText"
    expected_mtext = text_mtext
    mtext = msp_Mtext.add_mtext(text_mtext)

    # compare the expected text to the text returned by the function
    assert expected_mtext == get_entity_text(mtext), "%s isn't the color %s" % (
        expected_mtext, get_entity_text(mtext))

    # create drawing and modelspace
    doc = ezdxf.new('R2010')  # INSERT requires DXF 2000 or later
    msp_attrib = doc.modelspace()

    # create attrib
    name = "INSERT"
    insert = doc.blocks.new(name)
    insert = msp_attrib.add_blockref(name, (120, 14), dxfattribs={
                                     'xscale': 1, 'yscale': 1, 'rotation': 0})
    text_attrib = "Attrib"
    attrib = insert.add_attrib(name, text_attrib)
    expected_attrib = text_attrib

    # compare the expected text to the text returned by the function
    assert expected_attrib == get_entity_text(attrib), "%s isn't the color %s" % (
        expected_attrib, get_entity_text(attrib))

    # create drawing and modelspace
    doc_point = ezdxf.new()
    msp_point = doc_point.modelspace()

    # create point
    expected_point = None
    point = msp_point.add_point((120, 456))

    # compare the expected text to the text returned by the function
    assert expected_point == get_entity_text(point), "%s isn't the color %s" % (
        expected_point, get_entity_text(point))


def test_get_insert_entity_measurements():
    """ Tests the function get_insert_entity_measurement
    """

    # create drawing and modelspace
    doc = ezdxf.new('R2010')  # INSERT requires DXF 2000 or later
    block = doc.blocks.new("INSERT")
    msp = doc.modelspace()

    # create insert and add geometry to block
    coordinates = [(6, 4), (2, 1)]
    for x, y in coordinates:
        block.add_point((x, y))
    insert_entity = msp.add_blockref('INSERT', (120, 42))
    expected_measure = (4, 3)

    result = get_insert_entity_measurements(insert_entity)
    assert expected_measure == result, "%s isn't equal to %s" % (
        expected_measure, result)
