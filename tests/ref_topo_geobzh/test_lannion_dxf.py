import os.path
import tempfile
from deliverycheck.checks.ref_topo_geobzh.lorient import LorientAgglomerationDXFCheck
from helpers.utils import count_logs_by_check_name, compare_log_counts, unzip


def check_lorient_dxf(zip_file_name, tmp_path, srs='EPSG:3948'):

    tmp_dir = tempfile.gettempdir()
    current_dir = os.path.dirname(__file__)

    # extract DXF from ZIP file
    dxf_file_path = unzip(os.path.join(
        current_dir, "data", zip_file_name), "dxf", tmp_dir)

    # check DXF
    checker = LorientAgglomerationDXFCheck(dxf_file_path,
                                           tmp_path,
                                           srs)
    checker.run_checks()
    checker.write_check_reports()

    # remove DXF
    os.remove(dxf_file_path)

    # return log counts
    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)


def test_chaufferie_de_la_roche_derrien(tmp_path):
    error_count, warning_count = check_lorient_dxf("Chaufferie de La Roche Derrien.zip",
                                                   tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 3, 'GEOMETRIES_NON_AUTORISEES': 98, 'OBJETS_NON_AUTORISES': 205}
                       )
    compare_log_counts(warning_count, {'GEOMETRIES_NON_AUTORISEES': 2,
                                       'TEXTES': 12, 'DOUBLON': 1,
                                       'NOMENCLATURE_NOM_DE_FICHIER': 1, 'EXPORT_GML': 1, 'ALTITUDE': 2}
                       )


def test_planfictif_chaleur2_3(tmp_path):
    error_count, warning_count = check_lorient_dxf("planfictif_chaleur2_3.zip",
                                                   tmp_path)
    compare_log_counts(error_count, {'PLACEMENT_TEXTE': 1})
