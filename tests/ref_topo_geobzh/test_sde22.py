import os.path
import tempfile
from deliverycheck.checks.ref_topo_geobzh.lorient import LorientAgglomerationDXFCheck
from helpers.utils import count_logs_by_check_name, compare_log_counts, unzip


def check_lorient_dxf(zip_file_name, tmp_path, srs='EPSG:3948', bounds_file_path=None,
                      bounds_srs=None,
                      control_points_path=None, control_points_ogr_driver_name='GPKG',
                      control_points_srs=None, control_points_accuracy_config_path=None,
                      dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None, gml_export_options={},
                      archive_path=None):

    tmp_dir = tempfile.gettempdir()
    current_dir = os.path.dirname(__file__)

    # extract DXF from ZIP file
    dxf_file_path = unzip(os.path.join(
        current_dir, "data", zip_file_name), "dxf", tmp_dir)

    # check DXF
    checker = LorientAgglomerationDXFCheck(dxf_file_path,
                                           tmp_path, srs,
                                           bounds_file_path,
                                           bounds_srs,
                                           control_points_path, control_points_ogr_driver_name,
                                           control_points_srs, control_points_accuracy_config_path,
                                           dtm_path, dtm_srs, dtm_accuracy_config_path, gml_export_options,
                                           archive_path)
    checker.run_checks()
    checker.write_check_reports()

    # remove DXF
    os.remove(dxf_file_path)

    # return log counts
    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)


def test_export_sans_etiquette(tmp_path):
    gml_export = {"producteur": "SDE22", "default_gestionnaire": "Gestionnaire par défaut", "gestionnaire_map": {}, "default_qualite": "01", "qualite_map": {
    }, "default_precisionxy": 5, "precisionxy_map": {}, "default_precisionz": 5, "precisionz_map": {}, "prefixe_id": "ABC", "date_leve": "2023-01-18", "horodatage": "2023-01-01"}

    error_count, warning_count = check_lorient_dxf("Test_Export_Complet_Sans_Etiquette.zip", tmp_path,
                                                   gml_export_options=gml_export)
    compare_log_counts(error_count, {'OBJETS_NON_AUTORISES': 2}
                       )
    compare_log_counts(warning_count, {'NOMENCLATURE_NOM_DE_FICHIER': 1, 'TEXTES': 1,
                                       'EXPORT_GML': 1, 'DOUBLON': 1}
                       )
