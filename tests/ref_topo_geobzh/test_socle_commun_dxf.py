import os.path
import tempfile
from deliverycheck.checks.ref_topo_geobzh.commun import SocleCommunDXFCheck
from helpers.utils import count_logs_by_check_name, compare_log_counts, unzip

tmp_dir = tempfile.gettempdir()
current_dir = os.path.dirname(__file__)


def check_socle_commun_dxf(zip_file_name, tmp_path, srs='EPSG:3948'):

    # extract DXF from ZIP file
    dxf_file_path = unzip(os.path.join(
        current_dir, "data", zip_file_name), "dxf", tmp_dir)

    # check DXF
    checker = SocleCommunDXFCheck(dxf_file_path,
                                  tmp_path,
                                  srs)
    checker.run_checks()
    checker.write_check_reports()

    # remove DXF
    os.remove(dxf_file_path)

    # return log counts
    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)

# Lorient


def test_locmiquelic(tmp_path):
    error_count, warning_count = check_socle_commun_dxf("locmiquelic.zip",
                                                        tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 1, 'ATTRIBUTS_BLOCS': 240,
                                     'OBJETS_NON_AUTORISES': 499, 'PLACEMENT_TEXTE': 21}
                       )
    compare_log_counts(
        warning_count, {'TEXTES': 394, 'DOUBLON': 7, 'EXPORT_GML': 1})


def test_socle_commun_testeur(tmp_path):
    error_count, warning_count = check_socle_commun_dxf("socle_commun+LA_testeur.zip",
                                                        tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 2283, 'GEOMETRIES_NON_AUTORISEES': 1084,
                                     'ATTRIBUTS_BLOCS': 2, 'OBJETS_NON_AUTORISES': 4822})
    compare_log_counts(warning_count, {'ALTITUDE': 35, 'DOUBLON': 1, 'EXPORT_GML': 1,
                                       'TEXTES': 653, 'ALT_MAT': 1, 'GEOMETRIES_NON_AUTORISEES': 1})


# Quimperlé
def test_quimperle(tmp_path):
    error_count, warning_count = check_socle_commun_dxf("quimperle.zip",
                                                        tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 1, 'ATTRIBUTS_BLOCS': 2, 'OBJETS_NON_AUTORISES': 59,
                                     'PLACEMENT_TEXTE': 3})
    compare_log_counts(
        warning_count, {'TEXTES': 127, 'ALTITUDE': 113, 'EXPORT_GML': 1})
