import os.path
import tempfile
from deliverycheck.checks.ref_topo_geobzh.rennes import RennesMetropoleDXFCheck
from helpers.utils import count_logs_by_check_name, compare_log_counts, unzip

tmp_dir = tempfile.gettempdir()
current_dir = os.path.dirname(__file__)


def check_rennes_dxf(zip_file_name, tmp_path, srs='EPSG:3948'):

    # extract DXF from ZIP file
    dxf_file_path = unzip(os.path.join(
        current_dir, "data", zip_file_name), "dxf", tmp_dir)

    # check DXF
    checker = RennesMetropoleDXFCheck(dxf_file_path,
                                      tmp_path,
                                      srs)

    checker.run_checks()
    checker.write_check_reports()

    # remove DXF
    os.remove(dxf_file_path)

    # return log counts
    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)

# DXF checks


def test_rennes(tmp_path):
    error_count, warning_count = check_rennes_dxf("rennes.zip",
                                                  tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 170, 'GEOMETRIES_NON_AUTORISEES': 435, 'ATTRIBUTS_BLOCS': 53,
                                     'OBJETS_NON_AUTORISES': 3964, 'COULEURS': 559, 'ALTITUDES_CALQUES_SPECIFIQUES': 213,
                                     'ECHELLES': 49, 'MATRICULE_PREFIXE': 372}
                       )
    compare_log_counts(warning_count, {'GEOMETRIES_NON_AUTORISEES': 16, 'COULEURS': 3918, 'TEXTES': 68,
                                       'DOUBLON': 25, 'DOUBLE_ENTITE': 3, 'EXPORT_GML': 1})
