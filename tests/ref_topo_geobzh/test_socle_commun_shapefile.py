import os.path
import shutil
import zipfile
from deliverycheck.checks.ref_topo_geobzh.sdef import SDEFShapefileCheck
from deliverycheck.checks.ref_topo_geobzh.sdef.shapefile import VectorDataType
from helpers.utils import count_logs_by_check_name, compare_log_counts
import tempfile

tmp_dir = tempfile.gettempdir()
current_dir = os.path.dirname(__file__)


def check_socle_commun_shp(zip_file_name, tmp_path, srs='EPSG:3948', remove_small_geometries=False):

    # extract repertory from ZIP file
    with zipfile.ZipFile(os.path.join(current_dir, "data", zip_file_name), 'r') as zip:
        shutil.unpack_archive(os.path.join(
            current_dir, "data", zip_file_name), tmp_dir)
        shp_file_path = os.path.join(tmp_dir, zip_file_name.split(".")[0])

    # check SHP
    checker = SDEFShapefileCheck(shp_file_path,
                                 tmp_path,
                                 srs,
                                 VectorDataType.RTS,
                                 remove_small_geometries=remove_small_geometries)

    checker.run_checks()
    checker.write_check_reports()

    # remove SHP
    shutil.rmtree(shp_file_path)

    # return log counts
    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)

# SDEF


def test_lanmeur(tmp_path):
    error_count, warning_count = check_socle_commun_shp("lanmeur.zip",
                                                        tmp_path)
    compare_log_counts(error_count, {
                       'ATTRIBUTS': 4, 'GEOMETRIES_NON_AUTORISEES': 76, 'OBJETS_NON_AUTORISES': 76})

    compare_log_counts(warning_count, {'EXPORT_GML': 1})


def test_territoire_clean2(tmp_path):
    error_count, warning_count = check_socle_commun_shp("territoire-clean2.zip",
                                                        tmp_path)

    compare_log_counts(error_count, {
                       'ATTRIBUTS': 350, 'GEOMETRIES_NON_AUTORISEES': 1780, 'OBJETS_NON_AUTORISES': 1780, 'VALIDITE_GEOM': 1})

    compare_log_counts(
        warning_count, {'ALT': 228, 'DOUBLON': 17, 'EXPORT_GML': 1})


def test_objets_petits_sans_nettoyage_auto(tmp_path):
    error_count, warning_count = check_socle_commun_shp("CCHC_CLOTU_L.zip",
                                                        tmp_path)
    compare_log_counts(
        error_count, {'VALIDITE_GEOM': 7})

    compare_log_counts(
        warning_count, {'EXPORT_GML': 1})


def test_objets_petits_avec_nettoyage_auto(tmp_path):
    error_count, warning_count = check_socle_commun_shp("CCHC_CLOTU_L.zip",
                                                        tmp_path,
                                                        remove_small_geometries=True)
    compare_log_counts(error_count, {})

    compare_log_counts(warning_count, {'VALIDITE_GEOM': 7})


def test_pilepont(tmp_path):
    error_count, warning_count = check_socle_commun_shp("CCHC_OUVRA_L.zip",
                                                        tmp_path)

    compare_log_counts(error_count, {'PILEPONT': 3})

    compare_log_counts(warning_count, {'EXPORT_GML': 1})


def test_geometry_nulle(tmp_path):
    error_count, warning_count = check_socle_commun_shp("BATIM_L_GEOM_NULL.zip",
                                                        tmp_path)

    compare_log_counts(error_count, {'GEOMETRIES_NON_AUTORISEES': 1, 'VALIDITE_GEOM': 1})

    compare_log_counts(warning_count, {'EXPORT_GML': 1})
