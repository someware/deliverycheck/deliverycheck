import os.path
import tempfile
from deliverycheck.checks.ref_topo_geobzh.lorient import LorientAgglomerationDXFCheck
from helpers.utils import count_logs_by_check_name, compare_log_counts, unzip

tmp_dir = tempfile.gettempdir()
current_dir = os.path.dirname(__file__)


def check_lorient_dxf(zip_file_name, tmp_path, srs='EPSG:3948', bounds_file_path=None,
                      bounds_srs=None,
                      control_points_path=None, control_points_ogr_driver_name='GPKG',
                      control_points_srs=None, control_points_accuracy_config_path=None,
                      dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None, gml_export_options={},
                      archive_path=None):
    # extract DXF from ZIP file
    dxf_file_path = unzip(os.path.join(
        current_dir, "data", zip_file_name), "dxf", tmp_dir)
    # extract files necessary for archive
    if archive_path != None:
        csv_file_path = unzip(os.path.join(
            current_dir, "data", zip_file_name), "csv", tmp_dir)
        archive_file_path = unzip(os.path.join(
            current_dir, "data", zip_file_name), "zip", tmp_dir)
        canevas_file_path = unzip(os.path.join(
            current_dir, "data", zip_file_name), "gpkg", tmp_dir)

    # check DXF
    if archive_path != None:
        checker = LorientAgglomerationDXFCheck(dxf_file_path,
                                               tmp_path, srs,
                                               bounds_file_path,
                                               bounds_srs,
                                               canevas_file_path, control_points_ogr_driver_name,
                                               control_points_srs, csv_file_path,
                                               dtm_path, dtm_srs, dtm_accuracy_config_path, gml_export_options,
                                               archive_file_path)
    else:
        checker = LorientAgglomerationDXFCheck(dxf_file_path,
                                               tmp_path,
                                               srs)
    checker.run_checks()
    checker.write_check_reports()

    # remove DXF and other if needed
    os.remove(dxf_file_path)
    if archive_path != None:
        os.remove(csv_file_path)
        os.remove(archive_file_path)
        os.remove(canevas_file_path)

    return count_logs_by_check_name(checker.error_check_logs), count_logs_by_check_name(checker.warning_check_logs)


def test_socle_commun_testeur(tmp_path):
    error_count, warning_count = check_lorient_dxf("socle_commun+LA_testeur.zip",
                                                   tmp_path)
    compare_log_counts(error_count, {'CALQUE_0': 2283, 'GEOMETRIES_NON_AUTORISEES': 1084,
                                     'ATTRIBUTS_BLOCS': 2, 'OBJETS_NON_AUTORISES': 4732})
    compare_log_counts(warning_count, {'GEOMETRIES_NON_AUTORISEES': 1, 'TEXTES': 653, 'ALTITUDE': 39, 'DOUBLON': 1,
                                       'NOMENCLATURE_NOM_DE_FICHIER': 1, 'EXPORT_GML': 1, 'ALT_MAT': 1})


def test_zip(tmp_path):
    error_count, warning_count = check_lorient_dxf("locmiquelic.zip", tmp_path,
                                                   control_points_path="canevas.gpkg",
                                                   control_points_srs="EPSG:3948",
                                                   archive_path="archive_plugin_la.zip",
                                                   control_points_accuracy_config_path="accuracy_check_sample.csv"
                                                   )
    compare_log_counts(error_count, {'CALQUE_0': 1, 'ATTRIBUTS_BLOCS': 242, 'OBJETS_NON_AUTORISES': 142,
                                     'ALTITUDE': 2, 'PLACEMENT_TEXTE': 21, 'FORMAT_ARCHIVE_ZIP': 1}
                       )
    compare_log_counts(warning_count, {'TEXTES': 394, 'ABERRATIONS_POINTS_DE_CONTROLE': 8, 'DOUBLON': 8, 'ALTITUDE': 6,
                                       'NOMENCLATURE_NOM_DE_FICHIER': 1, 'FORMAT_ARCHIVE_ZIP': 3, 'EXPORT_GML': 1}
                       )
