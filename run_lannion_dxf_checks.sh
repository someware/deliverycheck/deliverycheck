###################################################
# Check Socle Commun DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# Lannion

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/reseauchaleur_ehpad_la-roche-jaudy_sansortho.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/Chaufferie de La Roche Derrien.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/Chaufferie Loguivy-Plougras.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/plan_fictif_aep.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/planfictif_chaleur.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/planfictif_chaleur2_3.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/plan_fictif_eu2_avec_corpderue2.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# fichier avec des objets un peu bizarres qui sont dans le modelspace sans avoir de layer
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/S21-018-MINIHY TREGUIER-REC RESEAUX CHALEUR.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# export gml
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/planfictif_chaleur2_3.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"Lannion\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {\"ELEC\": \"Gestionnaire ELEC\",\"INCE\": \"Gestionnaire Incendie\"},\"default_qualite\" : \"01\",\"qualite_map\" : {\"02\": \"02\"},\"default_precisionxy\" : 5,\"precisionxy_map\" : {\"02\": 5},\"default_precisionz\" : 5,\"precisionz_map\" : {\"02\": 10}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/plan_fictif_reseauxcombines2.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"Lannion\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {\"ELEC\": \"Gestionnaire ELEC\",\"INCE\": \"Gestionnaire Incendie\"},\"default_qualite\" : \"01\",\"qualite_map\" : {\"02\": \"02\"},\"default_precisionxy\" : 5,\"precisionxy_map\" : {\"02\": 5},\"default_precisionz\" : 5,\"precisionz_map\" : {\"02\": 10}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

# check geometries with arcs
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lannion/tests_courbes_20240102.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

