###################################################
# Check SDE22 DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# SDE22

# export gml
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/sde22/Test_Export_AEP_Bati/Test_Export_AEP_Bati.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"SDE22\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {},\"default_qualite\" : \"01\",\"qualite_map\" : {},\"default_precisionxy\" : 5,\"precisionxy_map\" : {},\"default_precisionz\" : 5,\"precisionz_map\" : {}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/sde22/Test_Export_Complet_Sans_Etiquette/Test_Export_Complet_Sans_Etiquette.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"SDE22\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {},\"default_qualite\" : \"01\",\"qualite_map\" : {},\"default_precisionxy\" : 5,\"precisionxy_map\" : {},\"default_precisionz\" : 5,\"precisionz_map\" : {}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/sde22/Test_Export_Complet_Sans_Etiquette_Sans_erreur/Test_Export_Complet_Sans_Etiquette_Sans_erreur.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"SDE22\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {},\"default_qualite\" : \"01\",\"qualite_map\" : {},\"default_precisionxy\" : 5,\"precisionxy_map\" : {},\"default_precisionz\" : 5,\"precisionz_map\" : {}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/sde22/240221_Topo.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"SDE22\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {},\"default_qualite\" : \"01\",\"qualite_map\" : {},\"default_precisionxy\" : 5,\"precisionxy_map\" : {},\"default_precisionz\" : 5,\"precisionz_map\" : {}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

# MNT
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/sde22/QUEVEN_021CheminDeDoued_GeofitExpert_3D(20220405)_.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o dtm_path "${DATAPATH}/sde22/mnt_morbihan.tif" -o dtm_srs EPSG:2154 -o dtm_accuracy_config_path "${DATAPATH}/sde22/fichier_precision_v2.csv"