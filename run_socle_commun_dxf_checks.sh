###################################################
# Check Socle Commun DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# Lorient
deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/BUBRY_022Rue Poulna_CentreSecours_GeofitExpert_3D(20220406).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o bounds_file_path ${DATAPATH}/lorient/contour_bubry.geojson -o bounds_srs EPSG:4326

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/LOCMIQUELIC_018 Rue Roger Tremaré_Geofit Expert_3D(20220408).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/QUEVEN_021CheminDeDoued_GeofitExpert_3D(20220405)_.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/RIANTEC 017_Rue du stade_Geofit Expert_3D(20220209)_.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/RIANTEC_015_Rue Alain Le Blevec_GeofitExpert_3D(20220404).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/lorient/socle_commun+LA_testeur.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# Ploermel

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/ploermel/PLOERMEL_RecolementEU_botrel_20230911.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}


# Quimperlé

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/quimperle/QC_Exemple_ReseauAEP.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# Redon

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/redon/REDON.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# SDEF

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/sdef/crozon/DWG/Crozon.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o bounds_file_path ${DATAPATH}/sdef/crozon/crozon.geojson -o bounds_srs EPSG:4326

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/sdef/lanmeur/FORMAT SDEF/LANMEUR-079042.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# Brest

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/brest/20221109_Rue Saint-Saens.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}
