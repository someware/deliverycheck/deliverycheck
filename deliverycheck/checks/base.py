import logging
import functools
import textwrap

from .logs import BaseCheckLog, InfoCheckLog, ErrorCheckLog, WarningCheckLog


def check_method(check_name, user_doc):
    def decorator(check_func):
        @functools.wraps(check_func)
        def wrapper(*args, **kwargs):
            # display information before running check
            logging.info('%s : Exécution du contrôle...' % check_name)
            logging.info('%s : %s' % (check_name, user_doc))
            # get numbers of errors/warnings before running check
            func_self = args[0]
            num_errors_before = len(func_self.error_check_logs)
            num_warnings_before = len(func_self.warning_check_logs)
            # run check
            kwargs['check_name'] = check_name
            result = check_func(*args, **kwargs)
            # display information after running check
            num_errors_after = len(func_self.error_check_logs)
            num_warnings_after = len(func_self.warning_check_logs)
            logging.info('%s : Contrôle terminé (%d erreurs, %d warnings).' % (check_name,
                                                                               num_errors_after - num_errors_before,
                                                                               num_warnings_after - num_warnings_before))
            return result
        # so that methods with current decorator can be identified without running them
        wrapper.check_method = True
        wrapper.check_name = check_name
        wrapper.user_doc = user_doc
        return wrapper
    return decorator


class BaseCheck:

    def __init__(self, path, report_dir) -> None:
        self.path = path
        self.report_dir = report_dir
        self.__check_logs = []  # ordered list of check logs of any kind
        self.additional_log_attrs = []

    @property
    def error_found(self):
        for l in self.__check_logs:
            if isinstance(l, ErrorCheckLog):
                return True
        return False

    @property
    def check_logs(self):
        return self.__check_logs

    @property
    def error_check_logs(self):
        return [l for l in self.__check_logs if isinstance(l, ErrorCheckLog)]

    @property
    def warning_check_logs(self):
        return [l for l in self.__check_logs if isinstance(l, WarningCheckLog)]

    def log(self, check_log):
        assert isinstance(check_log, BaseCheckLog)
        # log
        log_message = '%s : %s' % (
            check_log.check_name, check_log.check_message)
        if isinstance(check_log, InfoCheckLog):
            logging.info(log_message)
        elif isinstance(check_log, ErrorCheckLog):
            logging.error(log_message)
        elif isinstance(check_log, WarningCheckLog):
            logging.warning(log_message)
        # add to logs list
        self.__check_logs.append(check_log)

    def info(self, *args, **kwargs):
        self.log(InfoCheckLog(*args, **kwargs))

    def error(self, *args, **kwargs):
        self.log(ErrorCheckLog(*args, **kwargs))

    def warn(self, *args, **kwargs):
        self.log(WarningCheckLog(*args, **kwargs))

    def warning(self, *args, **kwargs):
        self.log(WarningCheckLog(*args, **kwargs))

    @classmethod
    def generate_user_doc(cls, first_level=1, add_profile_name=True):
        heading1 = '#' * first_level
        heading2 = '#' * (first_level+1)
        if add_profile_name:
            user_doc = '%s %s\n\n' % (heading1, getattr(cls, 'user_doc', ''))
        else:
            user_doc = ''
        for check in cls.list_checks():
            user_doc += '%s %s\n\n' % (heading2, check.check_name)
            user_doc += textwrap.dedent(check.user_doc)
            user_doc += '\n\n'
        return user_doc

    @classmethod
    def list_checks(cls):
        """Return the list of checks in current check classs

        Returns:
            list: check methods for current check object
        """
        checks = []
        # introspect current object to find methods with decorator @check_method
        for attribute in dir(cls):
            # Get the attribute value
            attribute_value = getattr(cls, attribute)
            # Check that it is callable
            if callable(attribute_value):
                # Check it has attribute check_method
                if hasattr(attribute_value, 'check_method'):
                    checks.append(attribute_value)
        return checks

    @classmethod
    def count_checks(cls):
        """Return number of checks for current check object

        Returns:
            int: number of checks for current check object
        """
        return len(cls.list_checks())

    def run_checks(self):
        self.info(None, 'Exécution des contrôles')
        self.info(None, 'Nombre de contrôles: %d' % self.count_checks())
        try:
            [_ for _ in self.generate_checks()]
        except Exception as e:
            self.error(None, str(e))
            self.error(None,
                       '''Erreur lors de l'exécution des contrôles. Merci de contacter le support technique''')
            raise e
        self.info(None,
                  'Exécution des contrôles terminée (%d erreurs, %d warnings).' % (len(self.error_check_logs),
                                                                                   len(self.warning_check_logs)))

    def generate_checks(self):
        raise NotImplementedError

    def write_check_reports(self):
        raise NotImplementedError
