
class BaseCheckLog:
    def __init__(self, check_name, check_message, **kwargs) -> None:
        self.check_name = check_name
        self.check_message = check_message
        for name, value in kwargs.items():
            setattr(self, name, value)

class InfoCheckLog(BaseCheckLog):
    criticity = 'INFO'

class ErrorCheckLog(BaseCheckLog):
    criticity = 'ERROR'

class WarningCheckLog(BaseCheckLog):
    criticity = 'WARNING'