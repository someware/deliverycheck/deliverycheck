import logging
import os.path
from osgeo import ogr, osr, gdal
import json
import csv
import math

from deliverycheck.checks import BaseCheck, check_method

from deliverycheck.util.formats.ogr import OGRWriter, get_3d_points_from_geometry
from deliverycheck.util.formats.gdal import DTMInfo


current_dir = os.path.dirname(os.path.abspath(__file__))

root_dir = os.path.dirname(os.path.dirname(current_dir))
xsd_dir = os.path.join(root_dir, 'xsd')


class SocleCommunBaseCheck(BaseCheck):

    def __init__(self, path, report_dir, srs, bounds_file_path=None, bounds_srs=None,
                 control_points_path=None, control_points_ogr_driver_name='GPKG', control_points_srs=None, control_points_accuracy_config_path=None,
                 dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None,
                 gml_export_options={}
                 ) -> None:
        super().__init__(path, report_dir)
        self.srs = srs
        self.bounds_file_path = bounds_file_path
        self.bounds_srs = bounds_srs

        # prepare gpkg conversion filename
        self.gpkg_filename = os.path.join(report_dir, 'conversion.gpkg')
        self.gpkg_ds = None  # must be set by each subclass

        # prepare GIS gpkg conversion filename (special GPKG where each layer can only have one geometry type)
        self.gis_gpkg_filename = os.path.join(report_dir,
                                              'conversion_sig.gpkg')

        # init source data SRS
        self.data_srs = osr.SpatialReference()
        self.data_srs.SetFromUserInput(self.srs)
        self.data_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

        # set control points check config
        self.gpkg_control_point_filename = os.path.join(report_dir,
                                                        'points_controle.gpkg')
        self.control_points_path = control_points_path
        self.control_points_ogr_driver_name = control_points_ogr_driver_name
        self.control_points_srs = control_points_srs
        self.control_points_accuracy_config_path = control_points_accuracy_config_path

        # set dtm check config
        self.dtm_path = dtm_path
        self.dtm_srs = dtm_srs
        self.dtm_accuracy_config_path = dtm_accuracy_config_path

        # gml export config
        self.gml_export_options = gml_export_options
        self.gml_xsd_path = os.path.join(xsd_dir, 'CNIG_PCRS_v2.0.xsd')
        self.gml_filename = os.path.join(report_dir, 'conversion.gml')

    @property
    def gpkg_features(self):
        num_layers = self.gpkg_ds.GetLayerCount()
        for idx in range(num_layers):
            layer = self.gpkg_ds.GetLayer(idx)
            layer_name = layer.GetName()
            layer.ResetReading()
            feature = layer.GetNextFeature()
            while feature is not None:
                yield layer_name, feature
                feature = layer.GetNextFeature()

    def _create_control_points_gpkg(self, check_name):
        if not self.control_points_path or not self.control_points_ogr_driver_name or not self.control_points_srs:
            self.info(
                check_name, 'Pas de points de contrôle spécifiés ou options incomplètes')
            return None

        self.info(
            check_name, 'Conversion des points de contrôle au format GEOPACKAGE...')

        # Open input file
        ogr_driver = ogr.GetDriverByName(self.control_points_ogr_driver_name)
        if ogr_driver is None:
            self.error(check_name, "Impossible de charger le driver OGR '%s'" %
                       self.control_points_ogr_driver_name)
            return None

        control_points_ds = ogr_driver.Open(self.control_points_path)
        if control_points_ds is None:
            self.error(check_name, "Impossible d'ouvrir le fichier '%s'" %
                       self.control_points_path)
            return None

        # Open first layer
        control_points_layer = control_points_ds.GetLayer(0)
        if control_points_layer is None:
            self.error(check_name, "Impossible de charger la première couche du fichier '%s'" %
                       self.control_points_path)
            return None

        # Load attributes
        control_points_fields = []
        control_points_layer_defn = control_points_layer.GetLayerDefn()
        for field_index in range(control_points_layer_defn.GetFieldCount()):
            field_defn = control_points_layer_defn.GetFieldDefn(field_index)
            field_name = field_defn.GetName()
            control_points_fields.append(field_name)

        # Load control points srs
        control_points_srs = osr.SpatialReference()
        load_result = control_points_srs.SetFromUserInput(
            self.control_points_srs)
        if load_result != 0:
            self.error(
                check_name, "La projection cartographique du fichier de points de contrôle spécifié n'est pas reconnue.")
            return None
        control_points_srs.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)

        # Prepare srs transformation
        srs_transform = osr.CoordinateTransformation(
            control_points_srs, self.data_srs)

        # Create GPKG output file
        gpkg_driver = ogr.GetDriverByName("GPKG")
        if os.path.exists(self.gpkg_control_point_filename):
            gpkg_driver.DeleteDataSource(self.gpkg_control_point_filename)
        ds = gpkg_driver.CreateDataSource(self.gpkg_control_point_filename)

        # Create output layer
        layer = ds.CreateLayer('control_points',
                               self.data_srs,
                               ogr.wkbPoint25D)
        layer.CreateField(ogr.FieldDefn('attribs',
                                        ogr.OFTString))

        writer = OGRWriter(layer)

        # Fill output layer
        control_points_layer.ResetReading()
        cp_feature = control_points_layer.GetNextFeature()
        while cp_feature is not None:
            geom = cp_feature.GetGeometryRef()

            if geom is None:  # it can occur
                # Move on to next feature
                cp_feature = control_points_layer.GetNextFeature()
                continue

            # Check geom type
            geom_type = geom.GetGeometryType()
            points = None
            if geom_type == ogr.wkbPoint25D:
                points = [geom]
            elif geom_type == ogr.wkbMultiPoint25D:
                points = []
                for i in range(geom.GetGeometryCount()):
                    g = geom.GetGeometryRef(i)
                    points.append(g)
            elif geom_type == ogr.wkbMultiPoint:
                if geom.GetGeometryCount() == 0:  # An empty multipoint can occur
                    # Move on to next feature
                    cp_feature = control_points_layer.GetNextFeature()
                    continue
            if points is None:
                print(geom)
                self.error(
                    check_name, "Une géométrie du fichier de points de contrôle spécifié n'est pas 3D. Arrêt du chargement.")
                return None

            # Load attribs
            attribs = {
                field_name: cp_feature.GetField(field_name)
                for field_name in control_points_fields
            }

            # Write points
            for point_geom in points:
                point_geom.Transform(srs_transform)

                output_feature = ogr.Feature(writer.GetLayerDefn())
                output_feature.SetGeometry(point_geom)
                output_feature.SetField('attribs', json.dumps(attribs))

                writer.CreateFeature(output_feature)

            # Move on to next feature
            cp_feature = control_points_layer.GetNextFeature()

        # Flush pending writes
        writer.close()

        return ds

    def _load_accuracy_config_path(self, check_name, calques_objets_list):
        if self.control_points_accuracy_config_path is None:
            return None

        self.info(
            check_name, 'Lecture de la config de précisions...')

        if not os.path.exists(self.control_points_accuracy_config_path):
            self.error(check_name, "Le fichier '%s' n'a pas été trouvé" %
                       self.control_points_accuracy_config_path)
            return None

        # Prepare dict to check calques/ids
        calques_objets_dict = {}
        for oc in calques_objets_list:
            calque = oc['calque']
            if calque not in calques_objets_dict:
                calques_objets_dict[calque] = []
            id = oc['id']
            calques_objets_dict[calque].append(id)

        # open csv file and check values
        accuracy_calques_objets_dict = {}
        csvfile = open(self.control_points_accuracy_config_path)
        reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for idx, row in enumerate(reader, 2):
            # check calque
            calque = row.get('calque', None)
            if not calque:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : pas de calque défini" % (
                    idx, self.control_points_accuracy_config_path))
                return None
            if calque not in calques_objets_dict:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : le calque '%s' ne fait pas partie du modèle de données" % (
                    idx, self.control_points_accuracy_config_path, calque))
                return None
            # check id
            id = row.get('id', None)
            if not id:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : pas d'identifiant d'objet défini" % (
                    idx, self.control_points_accuracy_config_path))
                return None
            if id not in calques_objets_dict[calque]:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : l'id '%s' ne fait pas partie du calque '%s' dans le modèle de données" % (idx,
                                                                                                                                                       self.control_points_accuracy_config_path,
                                                                                                                                                       id,
                                                                                                                                                       calque))
                return None
            # check prec_plani
            prec_plani = row.get('prec_plani', None)
            try:
                prec_plani = float(prec_plani)
            except:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : la valeur de prec_plani (%s) n'est pas une valeur flottante" % (idx,
                                                                                                                                             self.control_points_accuracy_config_path,
                                                                                                                                             prec_plani,
                                                                                                                                             ))
                return None
            # check prec_alti
            prec_alti = row.get('prec_alti', None)
            try:
                prec_alti = float(prec_alti)
            except:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : la valeur de prec_alti (%s) n'est pas une valeur flottante" % (idx,
                                                                                                                                            self.control_points_accuracy_config_path,
                                                                                                                                            prec_alti,
                                                                                                                                            ))
                return None

            # if everything seems ok
            if calque not in accuracy_calques_objets_dict:
                accuracy_calques_objets_dict[calque] = {}
            accuracy_calques_objets_dict[calque][id] = {
                'prec_plani': prec_plani,
                'prec_alti': prec_alti
            }
        return accuracy_calques_objets_dict

    def _load_dtm_accuracy_config_path(self, check_name, calques_objets_list):
        if self.dtm_accuracy_config_path is None:
            return None

        self.info(
            check_name, 'Lecture de la config de précisions attendues par rapport à un MNT...')

        if not os.path.exists(self.dtm_accuracy_config_path):
            self.error(check_name, "Le fichier '%s' n'a pas été trouvé" %
                       self.dtm_accuracy_config_path)
            return None

        # Prepare dict to check calques/ids
        calques_objets_dict = {}
        for oc in calques_objets_list:
            calque = oc['calque']
            if calque not in calques_objets_dict:
                calques_objets_dict[calque] = []
            id = oc['id']
            calques_objets_dict[calque].append(id)

        # open csv file and check values
        accuracy_calques_objets_dict = {}
        csvfile = open(self.dtm_accuracy_config_path)
        reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for idx, row in enumerate(reader, 2):
            # check calque
            calque = row.get('calque', None)
            if not calque:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : pas de calque défini" % (
                    idx, self.dtm_accuracy_config_path))
                return None
            if calque not in calques_objets_dict:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : le calque '%s' ne fait pas partie du modèle de données" % (
                    idx, self.dtm_accuracy_config_path, calque))
                return None

            # check id
            id = row.get('id', None)
            if not id:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : pas d'identifiant d'objet défini" % (idx,
                                                                                                                  self.dtm_accuracy_config_path))
                return None
            if id not in calques_objets_dict[calque]:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : l'id '%s' ne fait pas partie du calque '%s' dans le modèle de données" % (idx,
                                                                                                                                                       self.dtm_accuracy_config_path,
                                                                                                                                                       id,
                                                                                                                                                       calque))
                return None

            # check prec_alti
            prec_alti = row.get('prec_alti', None)
            try:
                prec_alti = float(prec_alti)
            except:
                self.error(check_name, "Erreur en ligne %d du fichier '%s' : la valeur de prec_alti (%s) n'est pas une valeur flottante" % (idx,
                                                                                                                                            self.dtm_accuracy_config_path,
                                                                                                                                            prec_alti,
                                                                                                                                            ))
                return None

            # if everything seems ok
            if calque not in accuracy_calques_objets_dict:
                accuracy_calques_objets_dict[calque] = {}
            accuracy_calques_objets_dict[calque][id] = prec_alti

        return accuracy_calques_objets_dict

    def _load_dtm(self, check_name):
        if not self.dtm_path or not self.dtm_srs:
            self.info(
                check_name, 'Pas de points de contrôle spécifiés ou options incomplètes')
            return None, None

        # open DTM
        gdal_dataset = gdal.Open(self.dtm_path, gdal.GA_ReadOnly)
        if gdal_dataset is None:
            self.error(
                check_name, 'Impossible de charger le Modèle Numérique de Terrain spécifié')
            return None, None

        # load DTM info class
        dtm_info = DTMInfo(gdal_dataset)
        if not dtm_info._is_valid:
            self.error(
                check_name, '''Erreur lors de la lecture du fichier terrain spécifié. Il ne s'agit pas d'un Modèle Numérique de Terrain''')
            return None, None

        # Load DTM srs
        terrain_srs = osr.SpatialReference()
        load_result = terrain_srs.SetFromUserInput(
            self.dtm_srs)
        if load_result != 0:
            self.error(
                check_name, "La projection cartographique du fichier terrain spécifié n'est pas reconnue.")
            return None
        terrain_srs.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)

        # Prepare srs transformation
        srs_transform = osr.CoordinateTransformation(self.data_srs,
                                                     terrain_srs)

        return dtm_info, srs_transform

    @check_method('ABERRATIONS_POINTS_DE_CONTROLE',
                  """
                  Vérification de la précision planimétrique et altimétrique à partir des points
                  de contrôle du canevas.

                  Ce contrôle recherche des points de contrôle aux abords des objets du fichier
                  en entrée, et compare leur position `(x,y)` ainsi que leur altitude pour détecter
                  d'éventuelles aberrations.

                  Les aberrations sont remontées sous la forme de warnings. Les éventuels points
                  non-3D sont remontés sous la forme d'erreurs.
                  """
                  )
    def check_27_control_points(self, check_name):

        # load control points
        cp_ds = self._create_control_points_gpkg(check_name)
        if cp_ds is None:
            self.info(
                check_name, '''Contrôle non exécuté car les points de contrôle n'ont pas été spécifiés ou une erreur a été détectée''')
            return

        # load accuracy config
        accuracy_calques_objets_dict = self._load_accuracy_config_path(check_name,
                                                                       self.CONSTANTS_CONFIG.CALQUES_OBJETS)
        if accuracy_calques_objets_dict is None:
            self.info(
                check_name, '''Contrôle non exécuté car la config de précisions n'a pas été spécifiée ou une erreur a été détectée''')
            return

        for layer_name, feature in self.gpkg_features:

            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue
            if layer_name not in accuracy_calques_objets_dict:
                continue

            # check if feature must be checked based on its id
            objects_to_check = accuracy_calques_objets_dict[layer_name]
            entity_type_id = feature.GetField('entity_type_id')
            if entity_type_id not in objects_to_check:
                continue
            prec_plani = objects_to_check[entity_type_id]['prec_plani']
            prec_alti = objects_to_check[entity_type_id]['prec_alti']

            # check feature geom is point
            geom = feature.GetGeometryRef()
            if geom.GetGeometryType() != ogr.wkbPoint25D:
                object_name = feature.GetField("object_name")
                self.feature_error(check_name,
                                   '''La géométrie de l'objet %s à contrôler doit être un point 3D''' % object_name,
                                   layer_name,
                                   feature)

            # get all close control points
            x, y, z = geom.GetPoint(0)
            sql_query = '''
                        SELECT
                            geom,
                            attribs
                            FROM control_points cp
                            WHERE
                                cp.ROWID IN (
                                    SELECT
                                        id
                                    FROM rtree_control_points_geom r                                        
                                    WHERE
                                        r.maxx >= %(x)f - %(max_distance)f
                                        AND
                                        r.minx <= %(x)f + %(max_distance)f
                                        AND
                                        r.maxy >= %(y)f - %(max_distance)f
                                        AND
                                        r.miny <= %(y)f + %(max_distance)f
                                )

                                AND                                            

                                ST_Distance(geom, MakePoint(%(x)f, %(y)f)) < %(max_distance)f
                        ''' % {
                        'x': x,
                        'y': y,
                        'max_distance': self.CONSTANTS_CONFIG.MAX_SEARCH_DISTANCE
            }

            # run sql query
            cp_layer = cp_ds.ExecuteSQL(sql_query)

            # if SQL query returned results
            if cp_layer is not None:
                cp_feature = cp_layer.GetNextFeature()
                while cp_feature is not None:
                    cp_geom = cp_feature.GetGeometryRef()

                    # if distance 2D > planimetric accuracy
                    plani_distance = cp_geom.Distance(geom)
                    plani_match_found = (plani_distance <= prec_plani)
                    if not plani_match_found:
                        object_name = feature.GetField("object_name")
                        self.feature_warn(check_name,
                                          '''L'objet %s se trouve à une distance planimétrique (%f) d'un point du canevas non conforme à la précision planimétrique %f attendue''' % (
                                              object_name, plani_distance, prec_plani),
                                          layer_name,
                                          feature)

                    # if height difference > altimetric accuracy
                    cp_z = cp_geom.GetZ()
                    alti_distance = math.fabs(z - cp_z)
                    alti_match_found = (alti_distance <= prec_alti)
                    if not alti_match_found:
                        object_name = feature.GetField("object_name")
                        self.feature_warn(check_name,
                                          '''L'objet %s se trouve à une distance altimétrique (%f) d'un point du canevas non conforme à la précision altimétrique %f attendue''' % (
                                              object_name, alti_distance, prec_alti),
                                          layer_name,
                                          feature)

                    if plani_match_found and alti_match_found:
                        break

                    cp_feature = cp_layer.GetNextFeature()

            # release result set
            cp_ds.ReleaseResultSet(cp_layer)

    @check_method('ABERRATIONS_MNT',
                  """
                  Vérification de l'altimétrie à partir d'un Modèle Numérique de Terrain (MNT).

                  Ce contrôle calcule le Z de chaque point sur le terrain fourni, et vérifie si le
                  Z du point est cohérent avec le Z du MNT (en fonction de la precision altimétrique
                  attendue pour l'objet concerné).
                  """
                  )
    def check_27_dtm(self, check_name):

        # load DTM
        dtm_info, srs_transform = self._load_dtm(check_name)
        if dtm_info is None or srs_transform is None:
            self.info(
                check_name, '''Contrôle non exécuté car le MNT n'a pas été spécifiée ou une erreur a été détectée au chargement''')
            return

        # get objects to check and expected Z accuracy
        accuracy_calques_objets_dict = self._load_dtm_accuracy_config_path(check_name,
                                                                           self.CONSTANTS_CONFIG.CALQUES_OBJETS)
        if accuracy_calques_objets_dict is None:
            self.info(
                check_name, '''Contrôle non exécuté car la config de précisions altimétriques (par rapport au terrain) n'a pas été spécifiée ou une erreur a été détectée''')
            return

        num_objects_checked = 0
        num_altitudes_checked = 0
        num_altitudes_that_seem_ok = 0
        num_points_out_of_dtm_bounds = 0
        for layer_name, feature in self.gpkg_features:

            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue
            # skip layers that must not be checked
            if layer_name not in accuracy_calques_objets_dict:
                continue

            # check if feature must be checked based on its id
            objects_to_check = accuracy_calques_objets_dict[layer_name]
            entity_type_id = feature.GetField('entity_type_id')
            if entity_type_id not in objects_to_check:
                continue
            prec_alti = objects_to_check[entity_type_id]

            # increment num objects checked
            num_objects_checked += 1

            # get object name
            object_name = feature.GetField("object_name")

            # iterate over feature points
            geom = feature.GetGeometryRef()
            for p in get_3d_points_from_geometry(geom):

                # reproject point to terrain srs
                p.Transform(srs_transform)

                # check if point is 3D
                coord_tuple = p.GetPoint()
                if len(coord_tuple) != 3:
                    self.feature_error(check_name,
                                       '''Le point (%s) de l'objet %s doit être un point 3D''' % (
                                           p, object_name),
                                       layer_name,
                                       feature)
                    continue

                # get coordinates
                x, y, z = coord_tuple

                # get terrain height value
                terrain_height = dtm_info.get_terrain_height(x, y)

                # if terrain height is None, point is out of bounds
                if terrain_height is None:
                    num_points_out_of_dtm_bounds += 1
                    continue

                # check accuracy
                num_altitudes_checked += 1
                alti_distance = math.fabs(z - terrain_height)
                if alti_distance > prec_alti:
                    self.feature_warn(check_name,
                                      '''Le point (%s) de l'objet %s se trouve a une distance altimétrique (%f) par rapport au terrain non conforme à la précision altimétrique %f attendue''' % (
                                          p, object_name, alti_distance, prec_alti),
                                      layer_name,
                                      feature)
                else:
                    num_altitudes_that_seem_ok += 1
        
        # log info 
        self.info(check_name, '''Nombre d'objets contrôlés: %d''' %
                  num_objects_checked)
        self.info(check_name, '''Nombre d'altitudes contrôlées: %d (nombre de points hors terrain: %d)''' %
                  (num_altitudes_checked, num_points_out_of_dtm_bounds))
        self.info(check_name, '''Nombre d'altitudes sans aberration: %d''' %
                  num_altitudes_that_seem_ok)

    def write_check_reports(self):
        logging.info('Ecriture du rapport au format XLSX...')
        self._write_xlsx_check_reports()
        logging.info('Ecriture du rapport au format GEOPACKAGE...')
        self._write_gpkg_check_reports()

    def _add_xlsx_check_reports_tabs(self, xls_ds):
        # create global report layer
        report_layer = xls_ds.CreateLayer('rapport')

        # add base fields
        report_layer.CreateField(ogr.FieldDefn('controle', ogr.OFTString))
        report_layer.CreateField(ogr.FieldDefn('couche', ogr.OFTString))
        report_layer.CreateField(ogr.FieldDefn('objet', ogr.OFTString))
        report_layer.CreateField(ogr.FieldDefn('criticite', ogr.OFTString))
        report_layer.CreateField(ogr.FieldDefn('description', ogr.OFTString))

        # add additional log attributes
        for log_attr in self.additional_log_attrs:
            report_layer.CreateField(ogr.FieldDefn(log_attr, ogr.OFTString))

        # create writer
        report_writer = OGRWriter(report_layer)

        for log in self.check_logs:
            report_feature = ogr.Feature(report_writer.GetLayerDefn())

            # set base fields

            report_feature.SetField('controle', log.check_name)

            report_feature.SetField('criticite', log.criticity)

            if hasattr(log, 'layer_name'):
                report_feature.SetField('couche', log.layer_name)

            if hasattr(log, 'check_message'):
                report_feature.SetField('description', str(log.check_message))

            if hasattr(log, 'object_name'):
                report_feature.SetField('objet', log.object_name)

            # add additional log attributes
            for log_attr in self.additional_log_attrs:
                if hasattr(log, log_attr):
                    report_feature.SetField(log_attr, getattr(log, log_attr))

            # add feature to writer
            report_writer.add_feature(report_feature)

        # flush writer
        report_writer.close()

    def _write_xlsx_check_reports(self):
        csv_driver = ogr.GetDriverByName("XLSX")
        output_file_name = os.path.join(self.report_dir, 'rapport.xlsx')
        if os.path.exists(output_file_name):
            csv_driver.DeleteDataSource(output_file_name)

        ds = csv_driver.CreateDataSource(output_file_name)

        self._add_xlsx_check_reports_tabs(ds)

        # close output datasource
        ds = None

    def _write_gpkg_check_reports(self):
        gpkg_driver = ogr.GetDriverByName("GPKG")
        output_file_name = os.path.join(self.report_dir, 'rapport.gpkg')
        if os.path.exists(output_file_name):
            gpkg_driver.DeleteDataSource(output_file_name)
        ds = gpkg_driver.CreateDataSource(output_file_name)
        writers = {}
        for log in self.check_logs:
            # if log does not have a geometry or check_name, move on to next log
            if not hasattr(log, 'geometry') or not hasattr(log, 'check_name'):
                continue

            # access existing writer or create layer/writer
            # for layer name
            if log.check_name in writers:
                writer = writers[log.check_name]
            else:
                # create layer
                layer = ds.CreateLayer(log.check_name,
                                       self.data_srs,
                                       ogr.wkbUnknown)
                # add base fields
                layer.CreateField(ogr.FieldDefn('couche', ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('criticite', ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('description', ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('objet', ogr.OFTString))

                # add additional log attributes
                for log_attr in self.additional_log_attrs:
                    layer.CreateField(ogr.FieldDefn(log_attr, ogr.OFTString))

                # initialize writer
                writer = OGRWriter(layer)
                writers[log.check_name] = writer

            # create OGR feature
            feature = ogr.Feature(writer.GetLayerDefn())

            # set base fields
            feature.SetGeometry(log.geometry)
            feature.SetField('criticite', log.criticity)
            if log.layer_name:
                feature.SetField('couche', log.layer_name)
            if log.check_message:
                feature.SetField('description', log.check_message)
            if log.object_name:
                feature.SetField('objet', log.object_name)

            # add additional log attributes
            for log_attr in self.additional_log_attrs:
                if hasattr(log, log_attr):
                    feature.SetField(log_attr, getattr(log, log_attr))

            # add feature to writer
            writer.add_feature(feature)

        # flush writers
        for writer in writers.values():
            writer.close()

        # close output datasource
        ds = None
