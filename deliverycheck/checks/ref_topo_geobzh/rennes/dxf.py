from deliverycheck.checks.ref_topo_geobzh.commun.dxf import SocleCommunDXFCheck
from deliverycheck.checks.ref_topo_geobzh.commun.dxf import selection_attr
from deliverycheck.checks import check_method
from .constants import rennes_metropole_dxf_config
from deliverycheck.util.formats.dxf import (
    get_entity_type_id,
    get_ogr_geometry,
    get_entity_verbose_name,
    get_entity_aci_rgb_color)
import ezdxf
from osgeo import ogr
import os.path
from decimal import Decimal, ROUND_HALF_UP


class RennesMetropoleDXFCheck(SocleCommunDXFCheck):
    user_doc = '''Standard topographique régional / Profil Rennes Métropole / Format DXF'''

    CONSTANTS_CONFIG = rennes_metropole_dxf_config

    def __init__(self, path, report_dir, srs,
                 bounds_file_path=None, bounds_srs=None,
                 control_points_path=None, control_points_ogr_driver_name='GPKG', control_points_srs=None, control_points_accuracy_config_path=None,
                 dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None,
                 gml_export_options={}
                 ) -> None:
        super().__init__(path, report_dir, srs, bounds_file_path, bounds_srs,
                         control_points_path, control_points_ogr_driver_name, control_points_srs, control_points_accuracy_config_path,
                         dtm_path, dtm_srs, dtm_accuracy_config_path, gml_export_options)

    def _generate_initial_checks(self):
        yield from super()._generate_initial_checks()
        yield self.check_13_text_style()
        yield self.check_r4_hatch_codes()
        yield self.check_r7_z_above_zero()
        yield self.check_r8_altitude_in_different_layers()
        yield self.check_r9_polylines_with_double_entity()
        yield self.check_r10_scales()
        yield self.check_r11_matricule_prefix()
        yield self.check_r12_unique_matricule()

    @check_method('HACHURES',
                  """
                  Test non exécuté pour le profil Rennes Metropole car il est déjà vérifié par
                  le contrôle 13 **OBJETS_NON_AUTORISES**.
                  """
                  )
    def check_11_hatch_fill(self, check_name):
        self.info(
            check_name, 'Test non exécuté car il est déjà vérifié par le contrôle 13 (OBJETS_NON_AUTORISES).')
        return
        # TODO : implémenter ici le test R4 ?!

    # overrides check done for socle commun
    # implements check R5
    @check_method('COULEURS',
                  """
                  Vérifie que la table de couleurs utilisée est impérativement celle prescrite
                  dans le cahier des charges.

                  La couleur ACI 7 (table de couleur par défaut AutoCAD) est considérée comme
                  valant `0,0,0` ou `255,255,255`.
                  """
                  )
    def check_14_colors(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                aci, (r, g, b) = get_entity_aci_rgb_color(self.dxf_doc, entity)

                entity_type_id = get_entity_type_id(entity)
                # if entity type id is not referenced in expected colors
                # just warn this could be an issue...
                if entity_type_id not in self.CONSTANTS_CONFIG.COULEURS:
                    object_name = get_entity_verbose_name(entity)
                    self.warn(
                        check_name,
                        '''Les objets de type %s n'ont pas de couleur attendue dans la table de couleurs''' % (
                            entity_type_id),
                        layer_name=entity.dxf.layer,
                        object_name=object_name)
                    continue

                # get expected color
                fr, fg, fb = self.CONSTANTS_CONFIG.COULEURS[entity_type_id]

                if r == fr and g == fg and b == fb:
                    # if colors match, move on to next entity
                    continue

                if aci == 7:  # special AutoCAD color that means object is white on black background, and black on white background
                    # so we allow both colors
                    if (fr, fg, fb) in ((0, 0, 0), (255, 255, 255)):
                        # if colors match, move on to next entity
                        continue

                # an error was detected
                object_name = get_entity_verbose_name(entity)
                self.entity_error(
                    check_name,
                    '''L'objet a une couleur non autorisée dans la charte''',
                    entity.dxf.layer,
                    entity)

    # implements check R13
    @check_method('STYLE_TEXTES',
                  """Vérifie le style des textes (police) en fonction du type d’objet."""
                  )
    def check_13_text_style(self, check_name):

        # list layers to check
        dxf_layers_to_check = {}
        for row in self.CONSTANTS_CONFIG.CALQUES_OBJETS:
            if row['type'].lower() == 'texte' and row.get('police', '') != '':
                dxf_layers_to_check[row['calque']] = row['police'].lower()

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                # if entity layer must not be checked, move on to next entity
                if entity.dxf.layer not in dxf_layers_to_check:
                    continue

                # if entity is not of type Text, move on to next entity
                if not isinstance(entity, ezdxf.entities.Text):
                    continue

                # get expected font
                expected_font = dxf_layers_to_check[entity.dxf.layer]

                # get entity font name (lower case, no extension if this is a font file name like .ttf)
                entity_font_name = os.path.splitext(
                    entity.font_name().lower())[0]

                if entity_font_name != expected_font:
                    object_name = get_entity_verbose_name(entity)
                    self.entity_error(
                        check_name,
                        '''L'objet a une police non autorisée''',
                        entity.dxf.layer,
                        entity)

    @check_method('ALT_MAT',
                  """
                  Les points `YP_1521` (Point topo) ont un point homologue dans les calques
                  `ALT` et `MAT` correspondant, avec un Z et Matricule identique au point topo.
                  """
                  )
    def check_21_leve_topo_alt_mat(self, check_name):

        self.info(None, 'Contrôle non exécuté par le modèle topographique ne supporte pas la notion de calque MAT, et gère différemment les calques ALT...')

    @check_method('CODE_HACHURES',
                  """
                  Vérifie que le type employé pour les hachures (ex: `BZ_4080_1`) est cohérent avec
                  celui du contour extérieur de la surface (ex: `BZ_4080`).
                  """
                  )
    def check_r4_hatch_codes(self, check_name):

        for layer_name in self.layer_names:

            if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES:
                continue

            # liste polyline / hatch type that can be in current layer
            layer_polyline_entity_types = []
            layer_hatch_entity_types = []
            for polyline_entity_type, polyline_hatch_entity_types in self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES[layer_name].items():
                layer_polyline_entity_types.append(polyline_entity_type)
                layer_hatch_entity_types.extend(polyline_hatch_entity_types)

            # if no hatch expected, move on to next layer
            if len(layer_hatch_entity_types) == 0:
                continue

            ##############################################################################
            # check if some POLYLINE objects do not have the right type and geom type
            ##############################################################################

            poly_sql_query = '''
                        SELECT
                            geom,
                            object_name,
                            block_name,
                            handle
                        FROM "%(layer)s"
                        WHERE                            
                            entity_type_id IN (%(polyline_entity_types)s)
                            
                            AND
                            (
                            NOT (type IN ('POLYLINE', 'LWPOLYLINE'))
                            OR
                            ST_GeometryType(geom) != 'POLYGON'
                            )                                
                        ''' % {
                'layer': layer_name,
                'polyline_entity_types': ','.join(['"%s"' % t for t in layer_polyline_entity_types]),
            }

            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(poly_sql_query)

            # if SQL query returned results
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    object_name = feature.GetField("object_name")
                    self.feature_error(
                        check_name,
                        '''L'objet doit être de type POLYLINE ou LWPOLYLINE, et doit être fermé''',
                        layer_name,
                        feature)
                    feature = layer.GetNextFeature()

            ##############################################################################
            # check if some HATCH objects do not have the right type and geom type
            ##############################################################################
            hatch_sql_query = '''
                        SELECT
                            geom,
                            object_name,
                            block_name,
                            handle
                        FROM "%(layer)s"
                        WHERE                            
                            entity_type_id IN (%(hatch_entity_types)s)
                            
                            AND

                            NOT (type IN ('HATCH', 'POLYLINE', 'LWPOLYLINE'))

                        ''' % {
                'layer': layer_name,
                'hatch_entity_types': ','.join(['"%s"' % t for t in layer_hatch_entity_types]),
            }

            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(hatch_sql_query)

            # if SQL query returned results
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    object_name = feature.GetField("object_name")
                    self.feature_error(
                        check_name,
                        '''L'objet doit être de type HATCH, POLYLINE ou LWPOLYLINE''',
                        layer_name,
                        feature)
                    feature = layer.GetNextFeature()

            ##############################################################################
            # check if POLYLINE objects have a HATCH/POLYLINE object closeby
            ##############################################################################

            poly_hatch_sql_query = '''
                        SELECT
                            poly_obj.geom as geom,
                            poly_obj.object_name as poly_object_name,
                            poly_obj.handle as poly_handle,
                            hatch_obj.object_name AS hatch_object_name
                            FROM "%(layer)s" poly_obj
                            LEFT JOIN "%(layer)s" hatch_obj
                            ON                                 
                                hatch_obj.entity_type_id IN (%(hatch_entity_types)s)

                                AND

                                INSTR(hatch_obj.entity_type_id, poly_obj.entity_type_id) = 1

                                AND

                                hatch_obj.type IN ('HATCH', 'POLYLINE', 'LWPOLYLINE')

                                AND

                                hatch_obj.ROWID IN (
                                    SELECT
                                        id
                                    FROM rtree_%(layer)s_geom r                                        
                                    WHERE
                                        r.maxx >= ST_MinX(poly_obj.geom)
                                        AND
                                        r.minx <= ST_MaxX(poly_obj.geom)
                                        AND
                                        r.maxy >= ST_MinY(poly_obj.geom)
                                        AND
                                        r.miny <= ST_MaxY(poly_obj.geom)
                                )

                                AND                                            

                                ST_Contains(poly_obj.geom, hatch_obj.geom)
                            
                        WHERE
                            poly_obj.type IN ('POLYLINE', 'LWPOLYLINE')
                            AND
                            poly_obj.entity_type_id IN (%(polyline_entity_types)s)
                            AND
                            ST_GeometryType(poly_obj.geom) = 'POLYGON'
                                
                                ''' % {
                'layer': layer_name,
                'polyline_entity_types': ','.join(['"%s"' % t for t in layer_polyline_entity_types]),
                'hatch_entity_types': ','.join(['"%s"' % t for t in layer_hatch_entity_types]),
            }

            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(poly_hatch_sql_query)

            # if SQL query returned results
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    poly_geom = feature.GetGeometryRef()
                    poly_object_name = feature.GetField(
                        "poly_object_name")
                    poly_handle = feature.GetField(
                        "poly_handle")
                    hatch_object_name = feature.GetField("hatch_object_name")

                    if hatch_object_name is None:
                        self.error(
                            check_name,
                            '''L'object doit avoir des hachures associées''',
                            layer_name=layer_name,
                            object_name=poly_object_name,
                            geometry=poly_geom.Clone(),
                            handle=poly_handle,
                            selection=selection_attr(poly_handle))

                    feature = layer.GetNextFeature()

    @check_method('AFFLEURANT_Z_NON_NUL',
                  """Vérifie que les objets de type Affleurant ont bien un Z non nul."""
                  )
    def check_r7_z_above_zero(self, check_name):

        for layer_name in self.layer_names:
            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue

            # get affleurant IDs, and skip if there is no affleurant for layer
            affleurant_ids = self.CONSTANTS_CONFIG.CALQUES_OBJETS_AFFLEURANTS.get(
                layer_name, {}).keys()
            if not affleurant_ids:
                continue

            sql_query = '''
                        SELECT
                            geom,
                            object_name,
                            block_name,
                            handle
                            FROM "%(layer)s"
                            WHERE
                                entity_type_id IN (%(affleurant_ids)s)
                                AND
                                ST_GeometryType(geom) = 'POINT'
                                AND                                
                                ST_Z(geom) <= 0.0
                                ''' % {
                        'layer': layer_name,
                        'affleurant_ids': ','.join(["'%s'" % entity_type_id for entity_type_id in affleurant_ids])
            }

            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(sql_query)

            # if SQL query returned results
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    object_name = feature.GetField("object_name")
                    self.feature_error(
                        check_name,
                        '''L'objet doit avoir un Z non nul''',
                        layer_name,
                        feature)

                    feature = layer.GetNextFeature()

            # release result set
            self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('ALTITUDES_CALQUES_SPECIFIQUES',
                  """
                  Vérifie que les altitudes stockées dans certains calques spécifiques
                  (ex: `EA_AL500`, `EA_AL200`) comme attribut d'objets `INSERT` sont conformes
                  à l'altitude écrite dans le texte à côté de l'`INSERT`.
                  """
                  )
    def check_r8_altitude_in_different_layers(self, check_name):

        for layer_name in self.layer_names:

            if layer_name not in self.CONSTANTS_CONFIG.CALQUES_ALT:
                continue

            alt_layer_name = layer_name

            # check if INSERTs objects have a TEXT object closeby
            sql_query_1 = '''
                        SELECT
                            insert_obj.geom as geom,
                            insert_obj.object_name as insert_object_name,
                            insert_obj.handle as insert_handle,
                            text_obj.object_name AS text_object_name,
                            text_obj.text as alt_text
                            FROM "%(alt_layer)s" insert_obj
                            LEFT JOIN "%(alt_layer)s" text_obj
                            ON                                 
                                text_obj.type = 'TEXT'

                                AND

                                text_obj.ROWID IN (
                                    SELECT
                                        id
                                    FROM rtree_%(alt_layer)s_geom r                                        
                                    WHERE
                                        r.maxx >= ST_MinX(insert_obj.geom) - %(max_distance)f
                                        AND
                                        r.minx <= ST_MaxX(insert_obj.geom) + %(max_distance)f
                                        AND
                                        r.maxy >= ST_MinY(insert_obj.geom) - %(max_distance)f
                                        AND
                                        r.miny <= ST_MaxY(insert_obj.geom) + %(max_distance)f
                                )

                                AND                                            

                                ST_Distance(insert_obj.geom, text_obj.geom) < %(max_distance)f
                            
                            WHERE  insert_obj.type = 'INSERT'
                                ''' % {
                'alt_layer': alt_layer_name,
                'max_distance': self.CONSTANTS_CONFIG.CALQUES_ALT_MAXIMUM_DISTANCE
            }

            # check if TEXTs objects have a INSERT object closeby
            sql_query_2 = '''
                        SELECT
                            text_obj.geom as geom,
                            insert_obj.object_name as insert_object_name,
                            insert_obj.handle as insert_handle,
                            text_obj.object_name AS text_object_name,
                            text_obj.text as alt_text
                            FROM "%(alt_layer)s" text_obj
                            LEFT JOIN "%(alt_layer)s" insert_obj
                            ON                                 
                                insert_obj.type = 'INSERT'

                                AND

                                insert_obj.ROWID IN (
                                    SELECT
                                        id
                                    FROM rtree_%(alt_layer)s_geom r                                        
                                    WHERE
                                        r.maxx >= ST_MinX(text_obj.geom) - %(max_distance)f
                                        AND
                                        r.minx <= ST_MaxX(text_obj.geom) + %(max_distance)f
                                        AND
                                        r.maxy >= ST_MinY(text_obj.geom) - %(max_distance)f
                                        AND
                                        r.miny <= ST_MaxY(text_obj.geom) + %(max_distance)f
                                )

                                AND                                            

                                ST_Distance(insert_obj.geom, text_obj.geom) < %(max_distance)f

                            WHERE  text_obj.type = 'TEXT'
                                ''' % {
                'alt_layer': alt_layer_name,
                'max_distance': self.CONSTANTS_CONFIG.CALQUES_ALT_MAXIMUM_DISTANCE
            }

            for sql_query in (sql_query_1, sql_query_2):
                # run sql query
                layer = self.gpkg_ds.ExecuteSQL(sql_query)

                # if SQL query returned results
                if layer is not None:
                    feature = layer.GetNextFeature()
                    while feature is not None:
                        insert_geom = feature.GetGeometryRef()
                        insert_object_name = feature.GetField(
                            "insert_object_name")
                        insert_handle = feature.GetField(
                            "insert_handle")
                        text_object_name = feature.GetField("text_object_name")

                        # if both INSERT and TEXT are found, check that their altitudes match
                        if insert_object_name is not None and text_object_name is not None:
                            if not insert_geom.GetGeometryType() == ogr.wkbPoint25D:
                                self.error(
                                    check_name,
                                    '''L'objet doit avoir un point 3D (coordonnée en Z)''',
                                    layer_name=alt_layer_name,
                                    object_name=insert_object_name,
                                    geometry=insert_geom.Clone(),
                                    handle=insert_handle,
                                    selection=selection_attr(insert_handle))
                                continue

                            # check altitude text is a float
                            alt_text = feature.GetField("alt_text")
                            try:
                                float_alt_text = float(alt_text)
                            except:
                                self.error(
                                    check_name,
                                    '''L'objet TEXTE a une valeur qui n'est pas une valeur flottante correspondant à une altitude''',
                                    layer_name=alt_layer_name,
                                    object_name=insert_object_name,
                                    geometry=insert_geom.Clone(),
                                    handle=insert_handle,
                                    selection=selection_attr(insert_handle))
                                continue

                            # compare altitudes
                            altitude = insert_geom.GetZ(0)
                            rounded_altitude = Decimal(altitude).quantize(Decimal('0.01'),
                                                                          ROUND_HALF_UP)
                            rounded_float_alt_text = Decimal(float_alt_text).quantize(Decimal('0.01'),
                                                                                      ROUND_HALF_UP)
                            if rounded_float_alt_text != rounded_altitude:
                                self.error(
                                    check_name,
                                    '''Les objets %s et %s du calque %s referencent des altitudes différentes à 0.01 près (%f et %s)''' % (insert_object_name,
                                                                                                                                           text_object_name,
                                                                                                                                           alt_layer_name,
                                                                                                                                           altitude,
                                                                                                                                           alt_text
                                                                                                                                           ),
                                    layer_name=alt_layer_name,
                                    object_name=insert_object_name,
                                    geometry=insert_geom.Clone(),
                                    handle=insert_handle,
                                    selection=selection_attr(insert_handle))

                        elif insert_object_name is None:
                            self.error(
                                check_name,
                                '''L'objet n'a pas d'objet INSERT associé dans le calque %s (à une distance < %f)''' % (
                                    alt_layer_name,
                                    self.CONSTANTS_CONFIG.CALQUES_ALT_MAXIMUM_DISTANCE,
                                ),
                                layer_name=alt_layer_name,
                                object_name=text_object_name,
                                geometry=insert_geom.Clone(),
                                handle=insert_handle,
                                selection=selection_attr(insert_handle))

                        elif text_object_name is None:
                            self.error(
                                check_name,
                                '''L'objet n'a pas d'objet TEXTE associé dans le calque %s (à une distance < %f)''' % (
                                    alt_layer_name,
                                    self.CONSTANTS_CONFIG.CALQUES_ALT_MAXIMUM_DISTANCE,
                                ),
                                layer_name=alt_layer_name,
                                object_name=insert_object_name,
                                geometry=insert_geom.Clone(),
                                handle=insert_handle,
                                selection=selection_attr(insert_handle))

                        feature = layer.GetNextFeature()

                # release result set
                self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('DOUBLE_ENTITE',
                  """
                  Vérifie que certaines lignes ont d’une double entité (deux lignes parallèles,
                  l’une étant déduite de l’autre à partir d’une distance d’écartement soit fixe,
                  soit spécifiée par l’opérateur en fonction du type d’objet).

                  Le contrôle vérifie juste que, dans chaque calque où ce type d’entité est présent,
                  il y a le même nombre de lignes du premier type et du second (avec `_1`, `_2`
                  comme suffixe...).
                  """
                  )
    def check_r9_polylines_with_double_entity(self, check_name):

        # list all entity types that must be counted
        entity_types_to_count = {}
        for layer_name in self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE:
            if layer_name not in entity_types_to_count:
                entity_types_to_count[layer_name] = []
            for main_entity_type, double_entity_types in self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE[layer_name].items():
                entity_types_to_count[layer_name].append(main_entity_type)
                entity_types_to_count[layer_name].extend(double_entity_types)

        # count entities of main and double/secondary entity types
        entity_count = {}
        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                layer_name = entity.dxf.layer

                # check if layer expects some objects with a matricule first letter equal to first letter of block name
                if layer_name not in entity_types_to_count:
                    continue

                # check if object must have or must be part of a double (or more) entity
                entity_type_id = get_entity_type_id(entity)
                if entity_type_id not in entity_types_to_count[layer_name]:
                    continue

                if layer_name not in entity_count:
                    entity_count[layer_name] = {}

                if entity_type_id in entity_count[layer_name]:
                    entity_count[layer_name][entity_type_id] += 1
                else:
                    entity_count[layer_name][entity_type_id] = 1

        # for each layer, get main entity types and compare the number of objects
        # to the number of double entity objects
        for layer_name, layer_entity_count in entity_count.items():
            main_entity_types = self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE[layer_name].keys(
            )
            for main_entity_type in main_entity_types:
                if main_entity_type in layer_entity_count:
                    num_main_entities = layer_entity_count[main_entity_type]
                    double_entity_types = self.CONSTANTS_CONFIG.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE[
                        layer_name][main_entity_type]
                    for double_entity_type in double_entity_types:
                        num_double_entities = layer_entity_count[double_entity_type]
                        if num_double_entities != num_main_entities:
                            self.warn(
                                check_name,
                                '''Le calque %s comporte %d objets de type %s, ce nombre devrait être égal au nombre d'objets (%d) de type %s (notion de double entité)''' % (layer_name,
                                                                                                                                                                              num_double_entities,
                                                                                                                                                                              double_entity_type,
                                                                                                                                                                              num_main_entities,
                                                                                                                                                                              main_entity_type),
                                layer_name=layer_name)

    @check_method('ECHELLES',
                  """
                  Vérification de l’échelle X/Y de certains types d’objets.

                  Certains types d'objets doivent comporter une échelle X/Y fixe (`xscale=yscale`),
                  D’autres doivent avoir une échelle spécifiée par l’opérateur
                  (`xscale` et `yscale` peuvent être différents).

                  L'échelle en Z doit être égale à 1 pour tous les objets.
                  """
                  )
    def check_r10_scales(self, check_name):

        for layer_name in self.layer_names:
            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue

            # check fixed dimension objects
            if layer_name in self.CONSTANTS_CONFIG.CALQUES_OBJETS_DIMENSION_FIXE:
                fixed_dimension_objects = self.CONSTANTS_CONFIG.CALQUES_OBJETS_DIMENSION_FIXE[
                    layer_name]
                sql_query = '''
                            SELECT
                                geom,
                                object_name,
                                block_name,
                                handle,
                                xscale,
                                yscale
                                FROM "%(layer)s"
                                WHERE
                                    entity_type_id IN (%(fixed_dimension_objects)s)
                                    AND
                                    xscale != yscale
                                    ''' % {
                            'layer': layer_name,
                            'fixed_dimension_objects': ','.join(["'%s'" % entity_type_id for entity_type_id in fixed_dimension_objects])
                }

                # run sql query
                layer = self.gpkg_ds.ExecuteSQL(sql_query)

                # if SQL query returned results
                if layer is not None:
                    feature = layer.GetNextFeature()
                    while feature is not None:
                        self.feature_error(
                            check_name,
                            '''L'objet a une échelle X/Y non conforme avec la charte (xscale != yscale)''',
                            layer_name,
                            feature)

                        feature = layer.GetNextFeature()
                # release result set
                self.gpkg_ds.ReleaseResultSet(layer)

            # check zscale
            sql_query = '''
                        SELECT
                            geom,
                            object_name,
                            block_name,
                            handle,
                            zscale
                            FROM "%(layer)s"
                            WHERE
                                type = 'INSERT'
                                AND
                                zscale != 1
                                ''' % {
                        'layer': layer_name
            }

            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(sql_query)

            # if SQL query returned results
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    self.feature_error(
                        check_name,
                        '''L'objet a une échelle en Z non conforme avec la charte (zscale != 1)''',
                        layer_name,
                        feature)

                    feature = layer.GetNextFeature()
            # release result set
            self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('MATRICULE_PREFIXE',
                  """
                  Vérification de cohérence entre le nom de bloc et le matricule (`MAT`)
                  de certains types d’objets.

                  La 1ère lettre du matricule devra alors être égale à la 1ère lettre du nom de bloc.
                  """
                  )
    def check_r11_matricule_prefix(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and isinstance(entity, ezdxf.entities.Insert):
                layer_name = entity.dxf.layer

                # check if layer expects some objects with a matricule first letter equal to first letter of block name
                if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_MATRICULE:
                    continue

                # check if objet must have a matricule first letter equal to first letter of block name
                mat_objects = self.CONSTANTS_CONFIG.CALQUES_OBJETS_MATRICULE[layer_name]
                entity_type_id = get_entity_type_id(entity)
                if entity_type_id not in mat_objects:
                    continue

                first_block_letter = entity_type_id.lower()[0]
                matricule = entity.get_attrib_text('MAT', None)
                if matricule is None or matricule[0].lower().strip() != first_block_letter:
                    object_name = get_entity_verbose_name(entity)
                    if matricule is None:
                        matricule_label = 'non renseigné'
                    else:
                        matricule_label = matricule
                    self.entity_error(check_name,
                                      '''L'objet doit avoir un attribut MAT ayant pour première lettre la première lettre du nom de bloc''',
                                      layer_name,
                                      entity)

    @check_method('MATRICULE_UNIQUE',
                  """Vérification de l’unicité du matricule (MAT) des objets."""
                  )
    def check_r12_unique_matricule(self, check_name):

        matricules = {}
        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and isinstance(entity, ezdxf.entities.Insert):
                layer_name = entity.dxf.layer

                # check if layer expects some objects with a matricule first letter equal to first letter of block name
                if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_MATRICULE:
                    continue

                # check if objet must have a matricule first letter equal to first letter of block name
                mat_objects = self.CONSTANTS_CONFIG.CALQUES_OBJETS_MATRICULE[layer_name]
                entity_type_id = get_entity_type_id(entity)
                if entity_type_id not in mat_objects:
                    continue

                matricule = entity.get_attrib_text('MAT', None)
                if matricule is None or matricule.strip() == '':
                    self.entity_error(check_name,
                                      '''L'objet doit avoir un attribut MAT renseigné''',
                                      layer_name,
                                      entity
                                      )
                else:
                    m = matricule.strip()
                    object_name = get_entity_verbose_name(entity)
                    # check for duplicates
                    if m in matricules:
                        self.entity_error(check_name,
                                          '''L'objet a un matricule qui n'est pas unique (déjà utilisé par un autre objet)''',
                                          layer_name,
                                          entity
                                          )
                    else:
                        matricules[m] = object_name
