import os.path
import deliverycheck.checks.ref_topo_geobzh.commun as socle_commun
from deliverycheck.checks.ref_topo_geobzh.commun.constants_load import ReferentielGeobretagneDXFConfig

socle_commun_dir = os.path.dirname(os.path.abspath(socle_commun.__file__))
current_dir = os.path.dirname(os.path.abspath(__file__))


class RennesMetropoleDXFConfig(ReferentielGeobretagneDXFConfig):

    def __init__(self, calques_objets_csv_path):
        super().__init__(calques_objets_csv_path)

        # liste des calques / objets de la famille Habillage
        self.COULEURS = {}
        for row in self.CALQUES_OBJETS:
            string_color = row['couleur']
            string_color_tuple = string_color.split(',')
            assert len(string_color_tuple) == 3, string_color
            r, g, b = tuple([int(v) for v in string_color_tuple])
            self.COULEURS[row['id']] = r, g, b

        # liste des calques / objets en fonction de l'echelle attendue
        self.CALQUES_OBJETS_DIMENSION_FIXE = {}
        self.CALQUES_OBJETS_DIMENSION_REELLE = {}
        for row in self.CALQUES_OBJETS:
            calque = row['calque']
            id = row['id']
            dimension = row['dimension'].lower().strip()
            if dimension == 'fixe':
                if calque in self.CALQUES_OBJETS_DIMENSION_FIXE:
                    self.CALQUES_OBJETS_DIMENSION_FIXE[calque].append(id)
                else:
                    self.CALQUES_OBJETS_DIMENSION_FIXE[calque] = [id]
            elif dimension == 'reelle':
                if calque in self.CALQUES_OBJETS_DIMENSION_REELLE:
                    self.CALQUES_OBJETS_DIMENSION_REELLE[calque].append(id)
                else:
                    self.CALQUES_OBJETS_DIMENSION_REELLE[calque] = [id]
            elif dimension != '':
                assert dimension == '', dimension

        # liste des calques / objets qui attendent un matricule conforme à la première lettre du nom de bloc
        self.CALQUES_OBJETS_MATRICULE = {}
        for row in self.CALQUES_OBJETS:
            calque = row['calque']
            id = row['id']
            matricule = row['mat'].lower().strip()
            if matricule != '':
                # verifie que le matricule correspond bien à la première lettre du nom de bloc
                # if matricule.lower() != id[0].lower():
                #     print(id)
                #     raise DataError()

                # ajoute le calque/objet à la liste des objets concernés
                if calque in self.CALQUES_OBJETS_MATRICULE:
                    self.CALQUES_OBJETS_MATRICULE[calque].append(id)
                else:
                    self.CALQUES_OBJETS_MATRICULE[calque] = [id]

        # liste des polylignes attendant une hachure a l'intérieur
        self.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES = {}
        for row in self.CALQUES_OBJETS:
            entity_type = row['type'].strip().lower()
            if entity_type != 'polyligne':
                continue
            calque = row['calque']
            id = row['id']
            double_entite = row['double entite'].lower().strip()
            if double_entite == 'oui':
                if calque not in self.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES:
                    self.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES[calque] = {}
                # on liste les hachures
                hachures = []
                for row2 in self.CALQUES_OBJETS:
                    # si l'entite n'est pas de type hachures, on ne la considere pas
                    entity_type2 = row2['type'].strip().lower()
                    if entity_type2 != 'hachures':
                        continue
                    calque2 = row2['calque']
                    id2 = row2['id']
                    if calque2 == calque and id2 != id and id2.startswith(id):
                        hachures.append(id2)
                if hachures:
                    self.CALQUES_OBJETS_POLYLIGNES_AVEC_HACHURES[calque][id] = hachures
            elif double_entite != '':
                assert double_entite == '', double_entite

        # liste des polylignes attendant une double, voire triple entite
        self.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE = {}
        for row in self.CALQUES_OBJETS:
            entity_type = row['type'].strip().lower()
            if entity_type != 'polyligne':
                continue
            calque = row['calque']
            id = row['id']
            double_entite = row['double entite'].lower().strip()
            if double_entite == 'oui':
                if calque not in self.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE:
                    self.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE[calque] = {}
                # on liste les double entites
                double_entites = []
                for row2 in self.CALQUES_OBJETS:
                    # si l'entite n'est pas une polyligne, on ne la considere pas
                    entity_type2 = row2['type'].strip().lower()
                    if entity_type2 != 'polyligne':
                        continue
                    calque2 = row2['calque']
                    id2 = row2['id']
                    if calque2 == calque and id2 != id and id2.startswith(id):
                        double_entites.append(id2)
                if double_entites:
                    self.CALQUES_OBJETS_POLYLIGNE_DOUBLE_ENTITE[calque][id] = double_entites
            elif double_entite != '':
                assert double_entite == '', double_entite

        # dictionnaire des objets de type Affleurant (attendant un Z non nul)
        self.CALQUES_OBJETS_AFFLEURANTS = {}
        for row in self.CALQUES_OBJETS:
            pcrs_type = row['pcrs'].lower()
            if pcrs_type != 'affleurantpcrs':
                continue
            entity_type = row['type'].strip().lower()
            if entity_type not in ('point', 'bloc', ):
                continue
            calque = row['calque']
            if calque not in self.CALQUES_OBJETS_AFFLEURANTS:
                self.CALQUES_OBJETS_AFFLEURANTS[calque] = {}
            id = row['id']
            self.CALQUES_OBJETS_AFFLEURANTS[calque][id] = row

        # liste des calques qui attendent un ou des calques d'altitude
        # cette variable n'est pas cette definie pour le socle commun
        # car elle est utilisee differemment
        self.CALQUES_ALT = {}
        for row in self.CALQUES_OBJETS:
            calques_alt = row['calque_alt'].strip()
            if calques_alt:
                calque = row['calque']
                calques_alt_list = calques_alt.split(',')
                for calque_alt in calques_alt_list:
                    self.CALQUES_ALT[calque_alt] = calque

        # distance maximum autorisee dans les CALQUES_ALT entre un INSERT et son TEXT associé
        self.CALQUES_ALT_MAXIMUM_DISTANCE = 1.0

        # attributs attendus sur les INSERTS et BLOCS
        # (la liste des calques qui attendent des attributs est calculee dans la conf socle commun)
        self.REQUIRED_INSERT_ATTRIBUTES = ('MAT', 'TABLE', 'VUE', )
        self.REQUIRED_BLOCK_ATTRIBUTES = ()


rennes_metropole_dxf_config = RennesMetropoleDXFConfig(
    os.path.join(current_dir, 'definitions/OBJETS_DXF_RM.csv'))
