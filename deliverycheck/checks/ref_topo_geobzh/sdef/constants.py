import os.path
from .constants_load import ReferentielGeobretagneShapefileConfig

current_dir = os.path.dirname(os.path.abspath(__file__))

socle_commun_shapefile_config = ReferentielGeobretagneShapefileConfig(os.path.join(current_dir, 'definitions/OBJETS_SHP_COMMUN.csv'))
