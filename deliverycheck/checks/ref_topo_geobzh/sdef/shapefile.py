import logging
import os.path

from osgeo import ogr, osr

from deliverycheck.checks import check_method
from deliverycheck.checks.ref_topo_geobzh import SocleCommunBaseCheck
from deliverycheck.util.formats.ogr import (OGRWriter,
                                            feature_has_field,
                                            get_representative_point_from_geometry)
from deliverycheck.checks.ref_topo_geobzh.sdef.shapefile_model import RTS_LAYER_SCHEMAS, RTGE_LAYER_SCHEMAS
from deliverycheck.util.misc import remove_accents, remove_file_if_exists
from .constants import socle_commun_shapefile_config
import cerberus
from decimal import Decimal, ROUND_HALF_UP
from .shp2gml import convert_shapefile_to_gml

# Map of Spatialite geometry type names supported in model definition
# and their expected compatible GML geometry types
SPATIALITE_TO_COMPATIBLE_GML_GEOMETRY_MAP = {
    'POINT': ('GM_Point',),
    'LINESTRING': ('GM_Curve', 'GM_MultiCurve',),
    'POLYGON': ('GM_Polygon',),
    'MULTILINESTRING': ('GM_MultiCurve',),
    'POINT Z': ('GM_Point',),
    'LINESTRING Z': ('GM_Curve', 'GM_MultiCurve',),
    'POLYGON Z': ('GM_Polygon',),
    'MULTILINESTRING Z': ('GM_MultiCurve',)
}


def get_geobretagne_shapefile_feature_verbose_name(layer_name, feature):
    """Returns a unique feature verbose name

    Args:
        layer_name (str): layer name
        feature (ogr.Feature): OGR feature

    Returns:
        str: verbose name
    """
    idobjet = feature.GetField('idobjet')
    if not idobjet:
        idobjet = 'IDOBJET_NON_REMPLI'
    return '%s:%s:%s' % (layer_name, idobjet, feature.GetFID())


class CustomCerberusErrorHandler(cerberus.errors.BasicErrorHandler):
    def _format_message(self, field, error):
        # logging.info("Field- " + str(field) + " | " + "Code- " + str(error.code) + " | " + "Error- " + str(
        #     super(CustomErrorHandler, self)._format_message(field, error)))

        if error.code == 2:
            return "cet attribut est requis"
        elif error.code == 35:
            return "la valeur nulle n'est pas autorisée"
        elif error.code == 68:
            return "valeur '%s' non autorisée" % error.value
        # else:
        #     print(error.code, super(CustomCerberusErrorHandler, self)._format_message(field, error))
        #     raise

        return super(CustomCerberusErrorHandler, self)._format_message(field, error)


class VectorDataType:
    RTS = 'RTS'
    RTGE = 'RTGE'


class SDEFShapefileCheck(SocleCommunBaseCheck):
    user_doc = '''Standard topographique régional / Profil SDEF / Format Shapefile'''

    CONSTANTS_CONFIG = socle_commun_shapefile_config

    def __init__(self, path, report_dir, srs, data_type: VectorDataType,
                 bounds_file_path=None, bounds_srs=None,
                 control_points_path=None, control_points_ogr_driver_name='GPKG', control_points_srs=None, control_points_accuracy_config_path=None,
                 dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None,
                 remove_small_geometries=False
                 ) -> None:
        super().__init__(path, report_dir, srs, bounds_file_path, bounds_srs,
                         control_points_path, control_points_ogr_driver_name, control_points_srs, control_points_accuracy_config_path,
                         dtm_path, dtm_srs, dtm_accuracy_config_path)

        # set data type and the column where object IDs are listed for each data type
        self.data_type = data_type
        if self.data_type == VectorDataType.RTS:
            self.rep_dwg_geobzh_column = 'RTS / valeur rep_dwg_geobzh'
            self.layer_schemas = RTS_LAYER_SCHEMAS
        elif self.data_type == VectorDataType.RTGE:
            self.rep_dwg_geobzh_column = 'RTGE / valeur rep_dwg_geobzh'
            self.layer_schemas = RTGE_LAYER_SCHEMAS

        # set small geometries param
        self.remove_small_geometries = remove_small_geometries

    def feature_error(self, check_name, check_message, layer_name, feature):
        """Log an error on a OGR feature
        """
        object_name = feature.GetField('object_name')
        geometry = feature.GetGeometryRef()
        if geometry is not None:
            geometry = geometry.Clone()
        self.error(check_name,
                   check_message,
                   layer_name=layer_name,
                   object_name=object_name,
                   geometry=geometry)

    def feature_warn(self, check_name, check_message, layer_name, feature):
        """Log a warning on a OGR feature
        """
        object_name = feature.GetField('object_name')
        geometry = feature.GetGeometryRef()
        if geometry is not None:
            geometry = geometry.Clone()
        self.warn(check_name,
                  check_message,
                  layer_name=layer_name,
                  object_name=object_name,
                  geometry=geometry)

    def __create_geopackage(self):
        """Converts input DXF dataset as a Geopackage to perform somes checks.
            This geopackage file is also a check artefact, that can be used to visualize
            input dataset in GIS software.
            Beware only DXF entities in modelspace can be converted as geopackage

        Returns:
            tupe (ogr.DataSource, list): geopackage OGR datasource and layer names list

        """
        self.info(None, 'Conversion des données au format GEOPACKAGE...')
        gpkg_driver = ogr.GetDriverByName("GPKG")
        if os.path.exists(self.gpkg_filename):
            gpkg_driver.DeleteDataSource(self.gpkg_filename)
        ds = gpkg_driver.CreateDataSource(self.gpkg_filename)
        writers = {}
        num_features_written = 0
        for layer_name, feature in self.input_features:
            # access existing writer or create layer/writer
            # for layer name
            if layer_name in writers:
                writer = writers[layer_name]
            else:
                geom_type = feature.GetGeomFieldDefnRef(0).GetType()
                layer = ds.CreateLayer(layer_name,
                                       self.data_srs,
                                       geom_type)
                # add special fields
                layer.CreateField(ogr.FieldDefn('entity_type_id',
                                                ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('object_name',
                                                ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('famille',
                                                ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('color',
                                                ogr.OFTString))
                layer.CreateField(ogr.FieldDefn('text',
                                                ogr.OFTString))

                # add fields from input layer
                feature_defn = feature.GetDefnRef()
                for field_index in range(feature_defn.GetFieldCount()):
                    field_defn = feature_defn.GetFieldDefn(field_index)
                    layer.CreateField(field_defn)

                # create and register OGR writer
                writer = OGRWriter(layer)
                writers[layer_name] = writer

            # create ogr.Feature
            gpkg_feature = ogr.Feature(writer.GetLayerDefn())
            gpkg_feature.SetFrom(feature)

            # ref_cnig is the expected object type (PCRS)
            ref_cnig = feature.GetField('ref_cnig')
            if not ref_cnig:
                ref_cnig = 'REF_CNIG_NON_REMPLI'
            gpkg_feature.SetField('entity_type_id', ref_cnig)

            # rep dwg geobzh is another object type (mapping to GeoBzh objects)
            rep_dwg_ge = feature.GetField('rep_dwg_ge')
            if not rep_dwg_ge:
                rep_dwg_ge = 'REP_DWG_GE_NON_REMPLI'
            gpkg_feature.SetField('rep_dwg_ge', rep_dwg_ge)

            gpkg_feature.SetField('object_name',
                                  get_geobretagne_shapefile_feature_verbose_name(layer_name, feature))

            # feature.SetField('color', famille_hex_color)
            # feature.SetField('text', get_entity_text(entity))

            # add feature to writer
            writer.add_feature(gpkg_feature)
            num_features_written += 1

        # flush / close writers
        for writer in writers.values():
            writer.close()

        # create indexes on fields
        for layer_name in writers:
            for field_name in ('object_name', 'entity_type_id', 'famille'):
                ds.ExecuteSQL('''
                    CREATE INDEX "idx_%(layer_name)s_%(field_name)s" ON "%(layer_name)s"("%(field_name)s");
                ''' %
                              {
                                  'layer_name': layer_name,
                                  'field_name': field_name
                              })
        return ds, num_features_written, writers.keys()

    @property
    def input_features(self):
        num_layers = self.datasource.GetLayerCount()
        for idx in range(num_layers):
            layer = self.datasource.GetLayer(idx)
            layer_name = layer.GetName()
            layer.ResetReading()
            feature = layer.GetNextFeature()
            while feature is not None:
                yield layer_name, feature
                feature = layer.GetNextFeature()

    @property
    def gpkg_features(self):
        num_layers = self.gpkg_ds.GetLayerCount()
        for idx in range(num_layers):
            layer = self.gpkg_ds.GetLayer(idx)
            layer_name = layer.GetName()
            layer.ResetReading()
            feature = layer.GetNextFeature()
            while feature is not None:
                yield layer_name, feature
                feature = layer.GetNextFeature()

    def generate_checks(self):
        self.info(None, 'Format des données à vérifier : %s' % self.data_type)
        # try reading file
        self.info(None, 'Ouverture du fichier/dossier Shapefile %s' %
                  self.path)
        shapefile_driver = ogr.GetDriverByName("ESRI Shapefile")
        self.datasource = shapefile_driver.Open(self.path)
        if self.datasource is None:
            self.error(
                None, '''Impossible d'ouvrir le chemin '%s' avec le driver OGR/ESRI Shapefile''' % self.path)
            yield from ()
        # run checks
        self.info(None, 'Exécution des contrôles...')

        # first check the input dataset base structure is valid
        field_definition_valid = self.check_input_layer_definitions()
        yield field_definition_valid

        if field_definition_valid:
            # create geopackage file from Shapefiles
            self.gpkg_ds, num_features_written, self.layer_names = self.__create_geopackage()

            if num_features_written == 0:
                self.error(None,
                           '''Les fichiers Shapefile ouverts ne contiennent aucun objet. ''')
                yield from ()
            else:
                # run checks that depend on geopackage file
                yield self.check_3_geometries_in_bounds()
                yield self.check_9_allowed_geometries()
                yield self.check_13_allowed_objects_per_layer()
                yield self.check_21_altitude()
                yield self.check_27_control_points()
                yield self.check_27_dtm()
                yield self.check_29_doublons()
                yield self.check_30_allowed_objects_per_layer()
                yield self.check_valid_geometries()
                yield self.check_pile_pont_geometries()
                yield self.check_gml_export()

    @check_method('FORMAT_SHAPEFILE',
                  """
                  Vérifie que les fichiers SHAPEFILE ont a minima les colonnes
                  `idobjet`, `ref_cnig`, `rep_dwg_ge` attendues.
                  """
                  )
    def check_input_layer_definitions(self, check_name):

        input_valid = True
        layer_definitions = {}
        expected_field_names = ('idobjet', 'ref_cnig', 'rep_dwg_ge', )
        for layer_name, feature in self.input_features:
            if layer_name in layer_definitions:
                layer_defn = layer_definitions[layer_name]
            else:
                layer_defn = feature.GetDefnRef()
                layer_definitions[layer_name] = layer_defn
                for field_name in expected_field_names:
                    if layer_defn.GetFieldIndex(field_name) == -1:
                        input_valid = False
                        self.error(check_name,
                                   '''Le champ '%s' n'est pas présent au sein de la couche '%s' ''' % (
                                       field_name, layer_name),
                                   layer_name=layer_name
                                   )
        return input_valid

    @check_method('HORS_ZONE_PROJET',
                  """
                  Vérifie que les données (hors famille `HABILLAGE`) sont conformes au
                  système de coordonnées RGF93-CC48 et incluses dans la zone du projet.
                  """
                  )
    def check_3_geometries_in_bounds(self, check_name):

        if self.bounds_file_path is None or self.bounds_srs is None:
            self.info(check_name, '''Contrôle non exécuté car aucun fichier contenant
                             la zone projet n'a été spécifié, ou les projections
                             des données n'ont pas été spécificées''')
            return

        # read bounds file
        bounds_ds = ogr.Open(self.bounds_file_path)
        bounds_layer = bounds_ds.GetLayer()
        bounds_feature = bounds_layer.GetNextFeature()
        bounds_polygon = bounds_feature.GetGeometryRef()

        # reproject bounds
        bounds_srs = osr.SpatialReference()
        bounds_srs.SetFromUserInput(self.bounds_srs)
        bounds_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        srs_transform = osr.CoordinateTransformation(bounds_srs,
                                                     self.data_srs)
        bounds_polygon.Transform(srs_transform)

        # query features that are out of bounds in each layer
        for layer_name in self.layer_names:
            sql_query = '''
                            SELECT geom, object_name
                            FROM %s
                            WHERE
                                NOT famille = 'habillage'
                                AND
                                NOT ST_Within(geom, GeomFromText('%s'))
                        ''' % (layer_name,
                               bounds_polygon.ExportToWkt())
            layer = self.gpkg_ds.ExecuteSQL(sql_query)
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    self.feature_error(check_name,
                                       '''L'objet est en dehors de la zone projet''',
                                       layer_name,
                                       feature)
                    feature = layer.GetNextFeature()
            self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('GEOMETRIES_NON_AUTORISEES',
                  """
                  Vérifie que seuls les types de géométries attendus dans chaque calque
                  sont présents.
                  """
                  )
    def check_9_allowed_geometries(self, check_name):

        for layer_name in self.layer_names:

            # check if layer name is supported in model definition
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_GEOMETRIES:
                self.error(check_name,
                           '''Le nom de calque '%s' n'est pas supporté dans le modèle de données''' % (
                               layer_name),
                           layer_name=layer_name)
                continue

            # get geometry types in layer
            layer = self.gpkg_ds.ExecuteSQL('''
                            SELECT                                
                                object_name,
                                rep_dwg_ge,
                                geom,
                                GeometryType(geom) as geometry_type                             
                                FROM "%(layer)s"                                    
                        ''' % {'layer': layer_name}
            )
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    spatialite_geometry_type = feature.GetField(
                        'geometry_type')
                    if spatialite_geometry_type not in SPATIALITE_TO_COMPATIBLE_GML_GEOMETRY_MAP:
                        self.error(check_name,
                                   '''Le type geométrique %s (calque %s) n'est pas supporté dans le modèle de données''' % (spatialite_geometry_type,
                                                                                                                            layer_name),
                                   layer_name=layer_name)
                    else:
                        rep_dwg_ge = feature.GetField('rep_dwg_ge')

                        if rep_dwg_ge not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_GEOMETRIES[layer_name]:
                            self.error(check_name,
                                       '''L'ID "%s" n'est pas supporté dans le modèle de données pour le calque "%s"''' % (
                                           rep_dwg_ge,
                                           layer_name),
                                       layer_name=layer_name)
                            feature = layer.GetNextFeature()
                            continue

                        # get expected geometry type for object
                        expected_gml_geometry_type = self.CONSTANTS_CONFIG.CALQUES_OBJETS_GEOMETRIES[
                            layer_name][rep_dwg_ge]

                        # get compatible GML types
                        compatible_gml_geometry_types = SPATIALITE_TO_COMPATIBLE_GML_GEOMETRY_MAP[
                            spatialite_geometry_type]

                        # if current geometry type is not supported => error
                        if expected_gml_geometry_type not in compatible_gml_geometry_types:
                            self.error(check_name,
                                       '''Le type geométrique %s n'est pas celui attendu pour l'objet''' % (
                                           spatialite_geometry_type),
                                       layer_name=layer_name)

                    # move on to next feature
                    feature = layer.GetNextFeature()
            self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('OBJETS_NON_AUTORISES',
                  """Vérifie que chaque calque contient bien les objets attendus et pas d'autres."""
                  )
    def check_13_allowed_objects_per_layer(self, check_name):

        for layer_name, feature in self.gpkg_features:

            # get object entity type ID
            rep_dwg_ge = feature.GetField('rep_dwg_ge')

            # look for layer / entity type
            rep_dwg_geobzh_defined_for_object_layer = False
            for oc in self.CONSTANTS_CONFIG.CALQUES_OBJETS:
                # check if current object layer has a non-empty rep_dwg_geobzh_column
                if oc['calque'] == layer_name and oc[self.rep_dwg_geobzh_column]:
                    rep_dwg_geobzh_defined_for_object_layer = True
                # check if current object layer is found, with a non-empty rep_dwg_geobzh_column value equal to current rep_dwg_ge value
                if oc['calque'] == layer_name and oc[self.rep_dwg_geobzh_column] == rep_dwg_ge:
                    break
            else:
                # layer was expecting some rep_dwg_ge values (some of them dont expect any)
                if rep_dwg_geobzh_defined_for_object_layer:
                    # object was not found with proper layer name and entity type id
                    self.feature_error(check_name,
                                       '''L'objet n'est pas autorisé dans le calque pour les relevés de type %s''' % (
                                           self.data_type),
                                       layer_name,
                                       feature)

    @check_method('ALT',
                  """
                  Vérifie que chaque calque devant contenir un champ `altitude_z` est
                  bien renseigné, et que ce champ a la même valeur que le Z du point associé.
                  """
                  )
    def check_21_altitude(self, check_name):

        # list layers that must have an altitude_z attribute
        altitude_z_layers = []
        for layer_name, schema in self.layer_schemas.items():
            if 'altitude_z' in schema:
                altitude_z_layers.append(layer_name)

        # check layers/features
        for layer_name, feature in self.gpkg_features:
            if layer_name not in altitude_z_layers:
                continue

            # check if feature has field altitude_z
            if not feature_has_field(feature, 'altitude_z'):
                object_name = feature.GetField("object_name")
                self.feature_error(
                    check_name,
                    '''L'objet n'a pas de champ altitude_z.''',
                    layer_name,
                    feature)
                continue

            # get object altitude_z
            altitude_z = feature.GetField('altitude_z')

            # check altitude_z
            try:
                altitude_z = Decimal(altitude_z)
            except:
                object_name = feature.GetField("object_name")
                self.feature_error(
                    check_name,
                    '''l'objet a une altitude qui n'est pas une valeur flottante''',
                    layer_name,
                    feature)
                continue

            if not altitude_z > 0.0:
                object_name = feature.GetField("object_name")
                self.feature_warn(check_name,
                                  '''L'objet a une altitude qui n'est pas > 0.0''',
                                  layer_name,
                                  feature)

            # check if geometry has same altitude
            geom = feature.GetGeometryRef()
            point = geom.GetPoint()

            if len(point) != 3:  # if no Z coordinate
                object_name = feature.GetField("object_name")
                geom = feature.GetGeometryRef()
                self.warn(check_name,
                          '''L'objet a bien un champ altitude_z mais son point est 2D''',
                          layer_name=layer_name,
                          object_name=object_name,
                          geometry=geom.Clone())
            else:
                z = Decimal(point[2])

                rounded_altitude_z = altitude_z.quantize(Decimal('0.01'),
                                                         ROUND_HALF_UP)
                rounded_z = z.quantize(Decimal('0.01'),
                                       ROUND_HALF_UP)

                if rounded_z != rounded_altitude_z:
                    object_name = feature.GetField("object_name")
                    geom = feature.GetGeometryRef()
                    self.warn(check_name,
                              '''L'objet a un champ altitude_z dont la valeur est différente de la coordonnée Z de son point at 0.01 près''',
                              layer_name=layer_name,
                              object_name=object_name,
                              geometry=geom.Clone())

    @check_method('DOUBLON',
                  """
                  Vérification des doublons par type d'objet dans chaque couche.
                
                  Le contrôle cherche des objets de même type (`ref cnig`) dans chaque couche, dont les géométries sont
                  - soit identiques en 3D,
                  - soit, dans le cas de points, très proches (à 0.01 m près)

                  Des warnings sont créés pour chaque doublon potentiel détecté.
                  """)
    def check_29_doublons(self, check_name):

        # query features that are out of bounds in each layer
        for layer_name in self.layer_names:
            sql_query = '''
                            SELECT
                                l1.geom,
                                l1.object_name,
                                l2.object_name AS other_object_name
                                FROM "%(layer)s" l1
                                JOIN "%(layer)s" l2
                                ON 
                                    -- look for close objects using rtree
                                    l2.ROWID IN (
                                        SELECT
                                            id
                                        FROM rtree_%(layer)s_geom r                                        
                                        WHERE
                                            r.maxx >= ST_MinX(l1.geom) - %(max_distance)f
                                            AND
                                            r.minx <= ST_MaxX(l1.geom) + %(max_distance)f
                                            AND
                                            r.maxy >= ST_MinY(l1.geom) - %(max_distance)f
                                            AND
                                            r.miny <= ST_MaxY(l1.geom) + %(max_distance)f
                                    )
                                    
                                    AND

                                    -- that have a different name
                                    l1.object_name != l2.object_name

                                    AND
                                    
                                    -- but same entity_type_id / ref cnig
                                    l1.entity_type_id = l2.entity_type_id                                
                                    
                                    AND

                                    (
                                        -- with either same geometry
                                        ST_Equals(l1.geom, l2.geom)

                                        OR

                                        -- or very close geometry for points
                                        (
                                            ST_GeometryType(l1.geom) = 'POINT'
                                            AND
                                            ST_GeometryType(l2.geom) = 'POINT'
                                            AND
                                            ST_Distance(l1.geom, l2.geom) < %(max_distance)f
                                        )
                                    ) 
                        ''' % {
                'layer': layer_name,
                'max_distance': self.CONSTANTS_CONFIG.DUPLICATE_MAX_DISTANCE
            }
            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(sql_query)
            # set up structures to keep track of unique duplicates and their geometries
            unique_duplicates = set()
            geoms = {}
            # if SQL query returned results
            if layer is not None:
                # populate unique duplicates and keep track of their geometries
                feature = layer.GetNextFeature()
                while feature is not None:
                    object_name = feature.GetField("object_name")
                    other_object_name = feature.GetField("other_object_name")
                    if ((object_name, other_object_name) not in unique_duplicates
                            and
                            (other_object_name, object_name) not in unique_duplicates
                        ):
                        unique_duplicates.add((object_name, other_object_name))
                        geom = feature.GetGeometryRef()
                        geoms[object_name] = get_representative_point_from_geometry(
                            geom)
                    feature = layer.GetNextFeature()
            # release result set
            self.gpkg_ds.ReleaseResultSet(layer)
            # warn of each unique duplicate
            for object_name, other_object_name in unique_duplicates:
                geom = geoms[object_name]
                self.warn(check_name,
                          '''l'objet est un doublon d'un autre objet (distance <= %f)''' % (
                              self.CONSTANTS_CONFIG.DUPLICATE_MAX_DISTANCE),
                          layer_name=layer_name,
                          object_name=object_name,
                          geometry=geom.Clone())

    @check_method('ATTRIBUTS',
                  """Vérifie les attributs présents sur chaque couche."""
                  )
    def check_30_allowed_objects_per_layer(self, check_name):

        v = cerberus.Validator(error_handler=CustomCerberusErrorHandler())
        v.allow_unknown = True  # additional fields are allowed

        for layer_name, feature in self.gpkg_features:

            # check if layer has an expected schema
            if layer_name not in self.layer_schemas:
                continue

            # get object attributes as a dict
            feature_attrs = feature.ExportToJson(as_object=True)['properties']

            # convert all attribute names to lower case
            lc_feature_attrs = {k.lower(): v for k, v in feature_attrs.items()}

            # check validity to layer schema
            v.validate(lc_feature_attrs, self.layer_schemas[layer_name])

            # if some errors were found
            if v.errors:
                # print(feature_attrs)
                for field_name, field_errors in v.errors.items():
                    for field_error in field_errors:
                        self.feature_error(
                            check_name,
                            '''Erreur sur l'attribut '%s' : %s''' % (field_name,
                                                                     field_error),
                            layer_name,
                            feature)

    @check_method('VALIDITE_GEOM',
                  """
                  Vérifie que les géométries sont valides au regard de leur type géométrique
                  et que leurs longueurs / surfaces minimales sont conformes.
                  """)
    def check_valid_geometries(self, check_name):

        # check geometries of expected layers
        for layer_name, feature in self.gpkg_features:
            # check if layer name is supported in model definition
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_GEOMETRIES:
                continue

            # get geometry
            geom = feature.GetGeometryRef()

            # check if geometry is not null
            if geom is None:
                self.feature_error(
                    check_name,
                    '''La géométrie n'est pas valide car non renseignée (NULL)''',
                    layer_name,
                    feature)
                continue

            # get geometry type
            geom_type = geom.GetGeometryType()

            # check if geometry is valid
            if not geom.IsValid():
                self.feature_error(
                    check_name,
                    '''La géométrie n'est pas valide''',
                    layer_name,
                    feature)

            # check length
            if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D,):
                length = geom.Length()
                if length < self.CONSTANTS_CONFIG.MIN_LENGTH:
                    # just warn if small geometries have to be removed
                    if self.remove_small_geometries:
                        self.feature_warn(
                            check_name,
                            '''La géométrie a une longueur trop faible (< %.3f m). Elle sera supprimée lors de l'export GML''' % self.CONSTANTS_CONFIG.MIN_LENGTH,
                            layer_name,
                            feature)
                    else:
                        self.feature_error(
                            check_name,
                            '''La géométrie a une longueur trop faible (< %.3f m). L'objet doit être supprimé ou modifié''' % self.CONSTANTS_CONFIG.MIN_LENGTH,
                            layer_name,
                            feature)

            # check area
            elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D,):
                area = geom.Area()
                if area < self.CONSTANTS_CONFIG.MIN_AREA:
                    # just warn if small geometries have to be removed
                    if self.remove_small_geometries:
                        self.feature_error(
                            check_name,
                            '''La géométrie a une surface trop faible (< %.3f m²). Elle sera supprimée lors de l'export GML''' % self.CONSTANTS_CONFIG.MIN_LENGTH,
                            layer_name,
                            feature)

                    else:
                        self.feature_error(
                            check_name,
                            '''La géométrie a une surface trop faible (< %.3f m²). L'objet doit être supprimé ou modifié''' % self.CONSTANTS_CONFIG.MIN_LENGTH,
                            layer_name,
                            feature)

    @check_method('PILEPONT',
                  """
                  Vérifie que les géométries des objets exportés en tant que PilePont dans le GML ont a minimum 2 points
                  """)
    def check_pile_pont_geometries(self, check_name):
        # check geometries of expected PilePont objects
        for layer_name, feature in self.gpkg_features:
            # check if layer name is supported in model definition
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_PILEPONT:
                continue

            ref_cnig = feature.GetField("ref_cnig")
            if ref_cnig not in self.CONSTANTS_CONFIG.CALQUES_OBJETS_PILEPONT[layer_name]:
                continue

            # get geometry and its type
            geom = feature.GetGeometryRef()
            geom_type = geom.GetGeometryType()

            # check if geometry is valid
            if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D) and geom.GetPointCount() < 3:
                self.feature_error(
                    check_name,
                    '''La géométrie n'est pas valide : elle doit contenir a minima 3 points.''',
                    layer_name,
                    feature)

    @check_method('EXPORT_GML',
                  """
                  Exporte les données au format PCRS GML 2.0
                  """)
    def check_gml_export(self, check_name):

        # remove existing GML file if it exists
        remove_file_if_exists(self.gml_filename)

        # check if previous errors were found
        if self.error_found:
            self.warning(check_name,
                         '''Des erreurs ont été trouvées. L'export GML se sera pas réalisé.''')
            return

        # set min length / area for exported geometries
        if self.remove_small_geometries:
            min_length = self.CONSTANTS_CONFIG.MIN_LENGTH
            min_area = self.CONSTANTS_CONFIG.MIN_AREA
        else:
            min_length = min_area = None

        try:
            convert_shapefile_to_gml(self.gpkg_filename,
                                     self.srs,
                                     self.CONSTANTS_CONFIG.calques_objets_csv_path,
                                     self.gml_xsd_path,
                                     self.gml_filename,
                                     logger=self,
                                     check_name=check_name,
                                     min_length=min_length,
                                     min_area=min_area)
        except Exception as e:
            self.info(check_name, 'Erreur lors de la conversion GML')
            self.error(check_name, str(e))
            remove_file_if_exists(self.gml_filename)
