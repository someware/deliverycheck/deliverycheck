import csv


class ReferentielGeobretagneShapefileConfig:
    """Charge la configuration pour les vérifications du referentiel topographique GeoBretagne (format SHAPEFILE)"""

    def __init__(self, calques_objets_csv_path):

        self.calques_objets_csv_path = calques_objets_csv_path

        # charge les types d'objets et calques attendus
        self.CALQUES_OBJETS = []
        with open(calques_objets_csv_path) as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=',')
            for row in csvreader:
                self.CALQUES_OBJETS.append(row)

        # liste des calques attendus
        self.CALQUES = list(set([row['calque']
                                 for row in self.CALQUES_OBJETS]))

        # dictionnaire du type géométrique attendu par calque
        self.CALQUES_OBJETS_GEOMETRIES = {}
        for row in self.CALQUES_OBJETS:
            calque = row['calque']
            if calque not in self.CALQUES_OBJETS_GEOMETRIES:
                self.CALQUES_OBJETS_GEOMETRIES[calque] = {}
            geometry_type = row['type']
            rep_dwg_ge = row['id']
            self.CALQUES_OBJETS_GEOMETRIES[calque][rep_dwg_ge] = geometry_type            

        # liste des noms de famille (en minuscules)
        self.FAMILLES = list(set([row['famille'].strip().lower()
                                  for row in self.CALQUES_OBJETS]))

        # liste des calques
        self.CALQUES_OBJETS_PILEPONT = {}
        for row in self.CALQUES_OBJETS:
            pcrs = row['pcrs']
            if pcrs == 'PilePontPCRS':
                calque = row['calque']
                entity_type_id = row['ref cnig']
                if calque not in self.CALQUES_OBJETS_PILEPONT:
                    self.CALQUES_OBJETS_PILEPONT[calque] = []
                self.CALQUES_OBJETS_PILEPONT[calque].append(entity_type_id)

        # distance maximum utilisée pour rechercher des points de controle proches d'un point
        self.MAX_SEARCH_DISTANCE = 0.2

        # distance maximum autorisée entre objets de type point (pour recherche de doublons)
        self.DUPLICATE_MAX_DISTANCE = 0.01

        # longueur minimum pour les polylignes
        self.MIN_LENGTH = 1E-3

        # surface minimum pour les polygones
        self.MIN_AREA = 1E-3
