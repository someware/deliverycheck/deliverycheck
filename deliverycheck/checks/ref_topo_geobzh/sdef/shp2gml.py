from osgeo import ogr
from deliverycheck.util.formats.ogr import exterior_ring, as_multilinestring, as_polygon, as_unmeasured_geometry
import csv
import xmlschema
import uuid


PCRS_NAMESPACES = {
    'pcrs': 'http://cnig.gouv.fr/pcrs',
    'gml': 'http://www.opengis.net/gml/3.2',
    'xlink': 'http://www.w3.org/1999/xlink',
    'gss': 'http://www.isotc211.org/2005/gss',
    'gco': 'http://www.isotc211.org/2005/gco',
    'gsr': 'http://www.isotc211.org/2005/gsr',
    'pcrs-i': 'http://cnig.gouv.fr/pcrs-information',
    'gmd': 'http://www.isotc211.org/2005/gmd',
    'gts': 'http://www.isotc211.org/2005/gts',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance'
}


def get_random_unique_id():
    return 'id_%s' % uuid.uuid4()


class PCRSObjectDefinition:

    def __init__(self, schema, row, srs_name) -> None:
        self.schema = schema
        self.row = row
        self.srs_name = srs_name

        # Get XSD element for current Class
        for e in self.schema.findall('pcrs:%s' % self.PCRS_CLASS):
            if e.local_name == self.PCRS_CLASS:
                self.xsd_element = e
                break
        else:
            raise xmlschema.XMLResourceError(
                'PCRS class not found: %s' % self.PCRS_CLASS)

        self.xml_converter = schema.get_converter(None,
                                                  namespaces=PCRS_NAMESPACES)

    def as_xml_string(self, feature):
        gml_dict = self.gml_dict(feature)

        encoded_feature_member = next(self.xsd_element.iter_encode(gml_dict,
                                                                   validation='strict',
                                                                   use_defaults=True,
                                                                   converter=self.xml_converter))

        string_encoded_feature_member = xmlschema.etree_tostring(encoded_feature_member,
                                                                 PCRS_NAMESPACES)

        # write feature
        feature_member = '''<pcrs:featureMember>%s</pcrs:featureMember>\n''' % string_encoded_feature_member
        return feature_member

    def gml_dict(self, feature):
        raise NotImplementedError

    def gml_id(self, feature):
        return self.idobjet(feature)

    def idobjet(self, feature):
        return feature.GetField('gmlid')

    def get_gml_point(self, point):
        assert isinstance(point, tuple)
        assert len(point) in (2, 3)
        return ','.join('%.9f' % c for c in point)

    def get_ogr_geometry(self, feature):
        return feature.GetGeometryRef()

    def get_gml_geom_from_ogr_geom(self, geom):
        # remove M coordinate if needed
        geom = as_unmeasured_geometry(geom)

        # check geometry type is compatible with PCRS class
        geom_type = geom.GetGeometryType()
        assert geom_type in self.EXPECTED_GEOM_TYPES, '''Le type géométrique "%s" n'est pas supporté pour %s''' % (geom.GetGeometryName(),
                                                                                                                   self.PCRS_CLASS)

        # Point 2D / 3D
        if geom_type in (ogr.wkbPoint, ogr.wkbPoint25D, ):
            return {
                'gml:Point': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:coordinates': self.get_gml_point(geom.GetPoint(0))
                }
            }

        # LineString 2D / 3D
        elif geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            gml_points = [self.get_gml_point(p) for p in geom.GetPoints()]
            return {
                'gml:Curve': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:segments': {
                        'gml:LineStringSegment': {
                            'gml:coordinates': ' '.join(gml_points)
                        }
                    }
                }
            }

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            # TODO: not sure about this conversion using only the exterior ring
            ring = exterior_ring(geom)
            gml_points = [self.get_gml_point(p)
                          for p in ring.GetPoints()]
            return {
                'gml:Polygon': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:exterior': {
                        'gml:LinearRing': {
                            'gml:coordinates': ' '.join(gml_points)
                        }
                    }
                }
            }

        # Multipoint 2D / 3D
        elif geom_type in (ogr.wkbMultiPoint, ogr.wkbMultiPoint25D, ):
            gml_points = []
            for i in range(geom.GetGeometryCount()):
                p = geom.GetGeometryRef(i)
                gml_point = self.get_gml_geom_from_ogr_geom(p)
                gml_points.append(gml_point)
            return {
                'gml:MultiPoint': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    'gml:pointMember': gml_points
                }
            }

        # MultilineString 2D / 3D
        elif geom_type in (ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ):
            gml_linestrings = []
            for i in range(geom.GetGeometryCount()):
                l = geom.GetGeometryRef(i)
                pos_list = []
                for point in l.GetPoints():
                    pos_list.extend(point)
                gml_linestring = {
                    'gml:LineString': {
                        '@gml:id': get_random_unique_id(),
                        '@srsDimension': l.CoordinateDimension(),
                        'gml:posList': pos_list
                    }
                }
                gml_linestrings.append(gml_linestring)

            return {
                'gml:MultiCurve': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    'gml:curveMember': gml_linestrings
                }
            }

        else:
            raise NotImplementedError(geom.GetGeometryName())

    def gml_geom(self, feature):
        geom = self.get_ogr_geometry(feature)
        if not geom.IsValid():
            idobjet = self.idobjet(feature)
            raise Exception(
                'Objet %s: La géométrie en entrée crée une géométrie GML non valide : %s' % (idobjet,
                                                                                             geom))
        return self.get_gml_geom_from_ogr_geom(geom)


class ObjetVecteurPCRS(PCRSObjectDefinition):
    def __init__(self, schema, row, srs_name) -> None:
        super().__init__(schema, row, srs_name)

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:idObjet': self.idobjet(feature),
            'pcrs:thematique': feature.GetField('thematique'),
            'pcrs:qualiteCategorisation': feature.GetField('qualitecat'),
            'pcrs:precisionPlanimetrique': feature.GetField('precisionp'),
            'pcrs:precisionAltimetrique': feature.GetField('precisiona'),
            'pcrs:producteur': feature.GetField('producteur'),
            'pcrs:geometrie': self.gml_geom(feature)
        }
        return feature_dict


class HabillagePCRS(PCRSObjectDefinition):
    def __init__(self, schema, row, srs_name) -> None:
        super().__init__(schema, row, srs_name)

    def idhabillage(self, feature):
        return feature.GetField('gmlid')

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:idHabillage': self.idhabillage(feature),
            'pcrs:thematique': feature.GetField('thematique'),
            'pcrs:geometrie': self.gml_geom(feature)
        }
        return feature_dict


class HabillageEnveloppePCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageEnveloppePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPolygon, ogr.wkbPolygon25D, )


class HabillageLignesPCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageLignesPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbMultiLineString, ogr.wkbMultiLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()

        # remove M coordinate if needed
        geom = as_unmeasured_geometry(geom)

        # get geometry type
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return as_multilinestring(geom)

        # MultilineString 2D / 3D
        elif geom_type in (ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return as_multilinestring(exterior_ring(geom))

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas supporté pour HabillageLignesPCRS''' % geom.GetGeometryName())


class HabillagePointsPCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillagePointsPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )


class AffleurantPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'AffleurantPCRS'

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:idObjet': self.idobjet(feature),
            'pcrs:thematique': feature.GetField('thematique'),
            'pcrs:qualiteCategorisation': feature.GetField('qualitecat'),
            'pcrs:precisionPlanimetrique': feature.GetField('precisionp'),
            'pcrs:precisionAltimetrique': feature.GetField('precisiona'),
            'pcrs:producteur': feature.GetField('producteur'),
            'pcrs:gestionnaire': feature.GetField('gestionnai'),
        }

        nature = feature.GetField('nature')
        if nature != '00':
            feature_dict['pcrs:nature'] = nature
        # else:
        # TODO: log a warning ?

        feature_dict['pcrs:reseau'] = feature.GetField('reseau')
        feature_dict['pcrs:representation'] = self.representation(feature)

        return feature_dict


class AffleurantPointPCRS(AffleurantPCRS):
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def representation(self, feature):
        return {
            'pcrs:AffleurantGeometriquePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:point': [{
                    'pcrs:AffleurantPointPCRS': {
                        '@gml:id': get_random_unique_id(),
                        'pcrs:geometrie': self.gml_geom(feature)
                    }
                }
                ]
            }
        }


class AffleurantEnveloppePCRS(AffleurantPCRS):
    EXPECTED_GEOM_TYPES = (ogr.wkbPolygon, ogr.wkbPolygon25D, )

    def representation(self, feature):
        return {
            'pcrs:AffleurantGeometriquePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:enveloppe': [{
                    'pcrs:AffleurantEnveloppePCRS': {
                        '@gml:id': get_random_unique_id(),
                        'pcrs:geometrie': self.gml_geom(feature)
                    }
                }
                ]
            }
        }


class PointLevePCRS(PCRSObjectDefinition):
    PCRS_CLASS = 'PointLevePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name) -> None:
        super().__init__(schema, row, srs_name)

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:geometrie': self.gml_geom(feature),
            'pcrs:numeroPoint': feature.GetField('gmlid'),
            'pcrs:precisionXY': feature.GetFieldAsInteger('precisionp'),
            'pcrs:precisionZ': feature.GetFieldAsInteger('precisiona'),
            'pcrs:producteur': feature.GetField('producteur')
        }
        return feature_dict


class FacadePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'FacadePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class ProeminenceBatiPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'ProeminenceBatiPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class ArbrePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'ArbrePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:reference'] = feature.GetField('reference')
        return feature_dict


class QuaiFluvioMaritimePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'QuaiFluvioMaritimePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class QuaiRailPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'QuaiRailPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class RailPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'RailPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class MurPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'MurPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:typeMur'] = feature.GetField('TypeMur')
        return feature_dict


class MarcheEscalierPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'MarcheEscalierPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class SeuilPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'SeuilPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class LimiteVoiriePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'LimiteVoiriePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D, )

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()

        # remove M coordinate if needed
        geom = as_unmeasured_geometry(geom)

        # get geometry type
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return exterior_ring(geom)

        # Other geom type
        else:
            raise NotImplementedError(geom.GetGeometryName())


class PilierRegulierPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PilierRegulierPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:section'] = feature.GetField('section')
        feature_dict['pcrs:angleRotation'] = 0
        feature_dict['pcrs:longueur'] = 0
        feature_dict['pcrs:largeur'] = 0
        return feature_dict


class PilierParticulierPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PilierParticulierPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class PilePontPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PilePontPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPolygon, ogr.wkbPolygon25D,
                           ogr.wkbLineString, ogr.wkbLineString25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()

        # remove M coordinate if needed
        geom = as_unmeasured_geometry(geom)

        # get geometry type
        geom_type = geom.GetGeometryType()

        # Polygon 2D / 3D
        if geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return geom

        # LineString 2D / 3D
        elif geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return as_polygon(geom)

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour PilePontPCRS''' % geom.GetGeometryName())


PCRS_CONVERSION_CLASSES = {
    'AffleurantEnveloppePCRS': AffleurantEnveloppePCRS,
    'AffleurantPointPCRS': AffleurantPointPCRS,
    'ArbrePCRS': ArbrePCRS,
    'FacadePCRS': FacadePCRS,
    'HabillageEnveloppePCRS': HabillageEnveloppePCRS,
    'HabillageLignesPCRS': HabillageLignesPCRS,
    'HabillagePointsPCRS': HabillagePointsPCRS,
    'LimiteVoiriePCRS': LimiteVoiriePCRS,
    'MarcheEscalierPCRS': MarcheEscalierPCRS,
    'MurPCRS': MurPCRS,
    'PilePontPCRS': PilePontPCRS,
    'PilierRegulierPCRS': PilierRegulierPCRS,
    'PilierParticulierPCRS': PilierParticulierPCRS,
    'PointLevePCRS': PointLevePCRS,
    'ProeminenceBatiPCRS': ProeminenceBatiPCRS,
    'QuaiFluvioMaritimePCRS': QuaiFluvioMaritimePCRS,
    'QuaiRailPCRS': QuaiRailPCRS,
    'RailPCRS': RailPCRS,
    'SeuilPCRS': SeuilPCRS
}


def convert_shapefile_to_gml(input_gpkg_path, srs_name,
                             csv_data_model_path,
                             gml_xsd_path,
                             output_gml_path,
                             logger,
                             check_name,
                             min_length=None,
                             min_area=None):
    # Open GPKG
    logger.info(check_name, 'Ouverture du fichier GPKG à convertir')
    gpkg_ds = ogr.Open(input_gpkg_path)
    num_gpkg_layers = gpkg_ds.GetLayerCount()

    logger.info(check_name, 'Chargement du schéma PCRS au format XSD')
    pcrs_v20_schema = xmlschema.XMLSchema(gml_xsd_path)

    # Load objects that can be exported to PCRS
    logger.info(check_name, 'Import du modèle de données CSV')
    pcrs_calques_objets = {}
    with open(csv_data_model_path) as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=',')
        for row in csvreader:
            calque = row.get('calque', '')
            if not calque:
                continue

            pcrs_value = row.get('pcrs', '')
            if not pcrs_value:
                continue

            if pcrs_value in PCRS_CONVERSION_CLASSES:
                if not calque in pcrs_calques_objets:
                    pcrs_calques_objets[calque] = {}

                # get entity type id
                entity_type_id = row['ref cnig']

                # load conversion class
                conversion_class = PCRS_CONVERSION_CLASSES[pcrs_value]

                # instanciate conversion class
                try:
                    converter = conversion_class(pcrs_v20_schema,
                                                 row,
                                                 srs_name)
                except Exception as e:
                    logger.error(check_name,
                                 '''Le convertisseur PCRS n'a pas été créé pour la couche %s / objet %s''' % (calque, entity_type_id))
                    logger.error(check_name, e)  # , exc_info=True)
                else:
                    # add converter
                    pcrs_calques_objets[calque][entity_type_id] = converter
            else:
                logger.error(
                    check_name, 'Valeur PCRS non reconnue "%s"' % pcrs_value)

    # Create output file
    with open(output_gml_path, 'w') as gml_file:
        logger.info(check_name,
                    'Lecture du fichier Geopackage et création du GML')

        gml_file.write('''<?xml version="1.0" encoding="UTF-8"?>\n''')
        gml_file.write('''<PlanCorpsRueSimplifie xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns="http://cnig.gouv.fr/pcrs" gml:id="PlanCorpsRueSimplifie.1" version="2.0" xsi:schemaLocation="http://cnig.gouv.fr/pcrs CNIG_PCRS_v2.0.xsd">\n''')

        total_num_features_written = 0
        # to keep track of layers and entity type IDs that were read
        layer_entity_type_ids = set()
        for idx in range(num_gpkg_layers):
            layer = gpkg_ds.GetLayer(idx)

            # check if layer has objects that can be exported
            layer_name = layer.GetName()
            if layer_name not in pcrs_calques_objets:
                logger.warning(check_name,
                               'Couche %s non traitée car aucune conversion PCRS est définie' % layer_name)
                continue

            logger.info(check_name, 'Export de la couche %s' % layer_name)

            # get layer objects that can be exported to PCRS
            def_pcrs_objets = pcrs_calques_objets[layer_name]

            # initialize default converter for layers that have no expected entity_type_id
            default_converter = None
            if len(def_pcrs_objets) == 1 and next(iter(def_pcrs_objets.keys())) == '':
                default_converter = next(iter(def_pcrs_objets.values()))

            # iterate over layer features
            layer.ResetReading()
            feature = layer.GetNextFeature()
            num_features_read = 0
            num_small_features_skipped = 0
            num_features_written = 0
            discarded_entity_types = set()
            while feature is not None:
                num_features_read += 1

                # check geometry length / area (and skip feature if too small)
                geom = feature.GetGeometryRef()
                geom_type = geom.GetGeometryType()
                if (
                    (
                        min_length is not None
                        and
                        geom_type in (ogr.wkbLineString, ogr.wkbLineString25D,)
                        and
                        geom.Length() < min_length
                    )
                    or
                    (
                        min_area is not None
                        and
                        geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D,)
                        and
                        geom.Area() < min_area
                    )
                ):
                    # skip feature and move on to next one
                    num_small_features_skipped += 1
                    feature = layer.GetNextFeature()
                    continue

                # get entity type id
                entity_type_id = feature.GetField('entity_type_id')

                # get converter for object
                if default_converter is not None:
                    converter = default_converter
                else:
                    if entity_type_id not in def_pcrs_objets:
                        discarded_entity_types.add(entity_type_id)
                        feature = layer.GetNextFeature()
                        continue
                    converter = def_pcrs_objets[entity_type_id]

                layer_entity_type_ids.add((layer_name, entity_type_id))

                # get feature xml string
                gml_feature_string = converter.as_xml_string(feature)

                # remove pcrs namespace before writing (as XML header allows to do so)
                gml_feature_string = gml_feature_string.replace('pcrs:', '')

                # write feature
                gml_file.write(gml_feature_string)
                num_features_written += 1

                feature = layer.GetNextFeature()

            logger.info(check_name, 'Nombre objets lus pour la couche %s : %d' % (layer_name,
                                                                                  num_features_read))
            logger.info(check_name, 'Nombre petits objets non exportés pour la couche %s : %d' % (layer_name,
                                                                                                  num_small_features_skipped))
            logger.info(check_name, 'Nombre objets exportés pour la couche %s : %d' % (layer_name,
                                                                                       num_features_written))
            if discarded_entity_types:
                logger.error(check_name, '''Types d'objets sans correspondance pour la couche %s : %s''' %
                             (layer_name, ', '.join(discarded_entity_types)))
            total_num_features_written += num_features_written

        gml_file.write('''</PlanCorpsRueSimplifie>''')
    # Close GML file

    logger.info(check_name, 'Couches/objets convertis:')
    for layer_name, entity_type_id in layer_entity_type_ids:
        logger.info(check_name, '%s : %s' % (layer_name, entity_type_id))

    logger.info(check_name, '''Nombre total d'objets exportés : %d''' %
                total_num_features_written)

    # Check GML conformity
    logger.info(check_name,
                'Vérification de la conformité du fichier XML par rapport au XSD')
    try:
        pcrs_v20_schema.validate(output_gml_path)
        logger.info(check_name, 'Le GML est conforme !')
    except xmlschema.XMLSchemaValidationError as e:
        logger.error(check_name, str(e))
        raise e
