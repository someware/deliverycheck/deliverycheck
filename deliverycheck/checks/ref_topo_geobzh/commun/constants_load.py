import csv


class ReferentielGeobretagneDXFConfig:
    """Charge la configuration pour les vérifications du referentiel topographique GeoBretagne (format DXF)"""

    def __init__(self, calques_objets_csv_path, couleurs_famille_csv_path=None):

        self.calques_objets_csv_path = calques_objets_csv_path

        # utilisé en interne pour fermer des polygones, comparer des points..;
        self.EPSILON = 1E-3

        # charge les types d'objets et calques attendus
        self.CALQUES_OBJETS = []
        with open(calques_objets_csv_path) as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=',')
            for row in csvreader:
                self.CALQUES_OBJETS.append(row)

        self.HABILLAGE_FAMILLE = 'habillage'

        # liste des calques / objets de la famille Habillage
        self.CALQUES_OBJETS_HABILLAGE = {row['id']: row['calque']
                                         for row in self.CALQUES_OBJETS if row['famille'].lower() == self.HABILLAGE_FAMILLE}

        # liste des calques attendus
        self.CALQUES = list(set([row['calque']
                                 for row in self.CALQUES_OBJETS]))

        # liste des calques dédiés aux textes, dans lesquels les objets en double sont notamment possibles
        self.CALQUES_TEXTE = list(set([row['calque']
                                       for row in self.CALQUES_OBJETS if row['calque'].lower().endswith('_sci')]))

        # dictionnaire des calques qui ont un calque_alt associé, avec son nom
        self.CALQUES_CALQUES_ALT = {row['calque']: (row['id'], row['calque_alt'])
                                    for row in self.CALQUES_OBJETS if row['calque_alt'].lower() != 'nc'}

        # dictionnaire des calques qui ont un calque_mat associé, avec son nom
        self.CALQUES_CALQUES_MAT = {row['calque']: (row['id'], row['calque_mat'])
                                    for row in self.CALQUES_OBJETS if row.get('calque_mat', 'nc').lower() != 'nc'}

        # liste des noms de famille (en minuscules)
        self.FAMILLES = list(set([row['famille'].strip().lower()
                                  for row in self.CALQUES_OBJETS]))

        # liste des calques / objets qui doivent avoir un ou plusieurs attributs d'altitude
        self.CALQUES_OBJETS_ALT = {}
        for row in self.CALQUES_OBJETS:
            altitude_attributes = row.get('attributs altitude', '')
            if altitude_attributes != '':
                row_id = row['id']
                self.CALQUES_OBJETS_ALT[row_id] = altitude_attributes.split(
                    ',')

        # chargement des couleurs par famille
        if couleurs_famille_csv_path is not None:
            # dictionnaire des couleurs attendues pour chaque famille
            # au passage, une verification de la cohérence des noms de famille est réalisée
            self.COULEURS = {}
            with open(couleurs_famille_csv_path) as csvfile:
                csvreader = csv.DictReader(csvfile, delimiter=',')
                for row in csvreader:
                    famille = row['Famille'].strip().lower()
                    # TODO: à remettre quand la config CSV de Rennes intégrera les noms de famille
                    # if not famille:
                    #     raise DataError()
                    # if famille not in self.FAMILLES:
                    #     raise LookupError(famille)
                    self.COULEURS[famille] = (int(row['R']),
                                              int(row['G']),
                                              int(row['B']))

            # TODO: à remettre quand la config CSV de Rennes intégrera les noms de famille
            # for f in self.FAMILLES:
            #     if f not in self.COULEURS:
            #         raise LookupError(f)

        # distance maximum utilisée pour rechercher des points de controle proches d'un point
        self.MAX_SEARCH_DISTANCE = 0.2

        # distance maximum autorisée entre objets de type point (pour recherche de doublons)
        self.DUPLICATE_MAX_DISTANCE = 0.01

        # distance maximum autorisée entre un objet de type point et un autre objet texte (avec point d'insertion) associé
        self.TEXT_MAX_DISTANCE = 0.01

        # libelle correspondant au type pour les objets de type texte
        self.TEXT_TYPE_LABELS = ('texte libre', 'texte information', )

        # styles d'entité spéciaux (en minuscules), utilisables sur toutes les couches
        # pour dessiner des choses diverses et variees
        self.SPECIAL_ENTITY_TYPE_IDs = (
            'socle_continu',
            'socle_discontinu',
            # 'axes', # pas sûr
            # 'topo_continu' # pas sûr
        )

        # suffixes des layers autorisés à contenir des hachures
        self.HATCH_LAYERS_SUFFIXES = (
            'H_SCS',  # Socle Commun Surface
            'H_SCT',  # Socle Commun souTerrain
            'H_SCI',  # Socle Commun Info
        )

        # liste des calques dont les INSERTS/BLOCS attendent des attributs (et les attributs attendus)
        self.REQUIRED_INSERT_ATTRIBUTES = (
            'ID', 'NATURE', 'FAMILLE', 'CALQUE', )
        self.REQUIRED_BLOCK_ATTRIBUTES = self.REQUIRED_INSERT_ATTRIBUTES
        self.CALQUES_INSERT_BLOCKS_ATTRIBUTES = list(set([row['calque'] for row in self.CALQUES_OBJETS
                                                          if row.get('type', '').lower() == 'bloc' and row.get('attributs insert', '').lower() != 'non']))
