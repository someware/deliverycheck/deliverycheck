import os.path
from .constants_load import ReferentielGeobretagneDXFConfig

current_dir = os.path.dirname(os.path.abspath(__file__))

socle_commun_dxf_config = ReferentielGeobretagneDXFConfig(os.path.join(current_dir, 'definitions/OBJETS_DXF_COMMUN.csv'),
                                                          os.path.join(current_dir, 'definitions/COULEURS_DXF_COMMUN.csv'))

