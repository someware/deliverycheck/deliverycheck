from osgeo import ogr
from deliverycheck.util.formats.ogr import exterior_ring, as_multipoint, as_multilinestring, as_polygon, as_linestring
import csv
import xmlschema
import uuid

PCRS_NAMESPACES = {
    'pcrs': 'http://cnig.gouv.fr/pcrs',
    'gml': 'http://www.opengis.net/gml/3.2',
    'xlink': 'http://www.w3.org/1999/xlink',
    'gss': 'http://www.isotc211.org/2005/gss',
    'gco': 'http://www.isotc211.org/2005/gco',
    'gsr': 'http://www.isotc211.org/2005/gsr',
    'pcrs-i': 'http://cnig.gouv.fr/pcrs-information',
    'gmd': 'http://www.isotc211.org/2005/gmd',
    'gts': 'http://www.isotc211.org/2005/gts',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance'
}

UNKNOWN_VALUE = 'nc'


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()


def get_random_unique_id():
    return 'id_%s' % uuid.uuid4()


class PCRSObjectDefinition:
    # abstract class

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        self.schema = schema
        self.row = row
        self.srs_name = srs_name
        self.producteur = producteur
        self.gestionnaire_map = gestionnaire_map
        self.default_gestionnaire = default_gestionnaire
        self.qualite_map = qualite_map
        self.default_qualite = default_qualite
        self.precisionxy_map = precisionxy_map
        self.default_precisionxy = default_precisionxy
        self.precisionz_map = precisionz_map
        self.default_precisionz = default_precisionz
        self.prefixe_id = prefixe_id
        self.date_leve = date_leve
        self.horodatage = horodatage

        # Get XSD element for current Class
        for e in self.schema.findall('pcrs:%s' % self.PCRS_CLASS):
            if e.local_name == self.PCRS_CLASS:
                self.xsd_element = e
                break
        else:
            raise xmlschema.XMLResourceError(
                'PCRS class not found: %s' % self.PCRS_CLASS)

        self.xml_converter = self.schema.get_converter(None,
                                                       namespaces=PCRS_NAMESPACES)

    def as_xml_string(self, feature):
        gml_dict = self.gml_dict(feature)

        encoded_feature_member = next(self.xsd_element.iter_encode(gml_dict,
                                                                   validation='strict',
                                                                   use_defaults=True,
                                                                   converter=self.xml_converter))

        string_encoded_feature_member = xmlschema.etree_tostring(encoded_feature_member,
                                                                 PCRS_NAMESPACES)

        # write feature
        feature_member = '''<pcrs:featureMember>\n%s\n</pcrs:featureMember>''' % string_encoded_feature_member
        return feature_member

    def gml_dict(self, feature):
        raise NotImplementedError

    def gml_id(self, feature):
        return 'id-%s' % self.idobjet(feature)

    def idobjet(self, feature):
        handle = feature.GetField('handle')
        return '%s%s' % (self.prefixe_id, handle)

    def get_gml_point(self, point):
        assert isinstance(point, tuple)
        assert len(point) in (2, 3)
        return ','.join('%.9f' % c for c in point)

    def get_ogr_geometry(self, feature):
        return feature.GetGeometryRef()

    def get_gml_geom_from_ogr_geom(self, geom):
        geom_type = geom.GetGeometryType()
        assert geom_type in self.EXPECTED_GEOM_TYPES, '''Le type géométrique "%s" n'est pas pris en charge pour %s''' % (geom.GetGeometryName(),
                                                                                                                         self.PCRS_CLASS)

        # Point 2D / 3D
        if geom_type in (ogr.wkbPoint, ogr.wkbPoint25D, ):
            return {
                'gml:Point': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:coordinates': self.get_gml_point(geom.GetPoint(0))
                }
            }

        # LineString 2D / 3D
        elif geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            gml_points = [self.get_gml_point(p) for p in geom.GetPoints()]
            return {
                'gml:Curve': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:segments': {
                        'gml:LineStringSegment': {
                            'gml:coordinates': ' '.join(gml_points)
                        }
                    }
                }
            }

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            # TODO: not sure about this conversion using only the exterior ring
            exterior_ring = geom.GetGeometryRef(0)
            gml_points = [self.get_gml_point(p)
                          for p in exterior_ring.GetPoints()]
            return {
                'gml:Polygon': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    '@srsDimension': geom.CoordinateDimension(),
                    'gml:exterior': {
                        'gml:LinearRing': {
                            'gml:coordinates': ' '.join(gml_points)
                        }
                    }
                }
            }

        # Multipoint 2D / 3D
        elif geom_type in (ogr.wkbMultiPoint, ogr.wkbMultiPoint25D, ):
            gml_points = []
            for i in range(geom.GetGeometryCount()):
                p = geom.GetGeometryRef(i)
                gml_point = self.get_gml_geom_from_ogr_geom(p)
                gml_points.append(gml_point)
            return {
                'gml:MultiPoint': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    'gml:pointMember': gml_points
                }
            }

        # MultilineString 2D / 3D
        elif geom_type in (ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ):
            gml_linestrings = []
            for i in range(geom.GetGeometryCount()):
                l = geom.GetGeometryRef(i)
                pos_list = []
                for point in l.GetPoints():
                    pos_list.extend(point)
                gml_linestring = {
                    'gml:LineString': {
                        '@gml:id': get_random_unique_id(),
                        '@srsDimension': l.CoordinateDimension(),
                        'gml:posList': pos_list
                    }
                }
                gml_linestrings.append(gml_linestring)

            return {
                'gml:MultiCurve': {
                    '@gml:id': get_random_unique_id(),
                    '@srsName': self.srs_name,
                    'gml:curveMember': gml_linestrings
                }
            }

        else:
            raise NotImplementedError(geom.GetGeometryName())

    def gml_geom(self, feature):
        geom = self.get_ogr_geometry(feature)
        return self.get_gml_geom_from_ogr_geom(geom)

    def gestionnaire(self, reseau):
        return self.gestionnaire_map.get(reseau, self.default_gestionnaire)

    def qualite(self, thematique):
        return '%02d' % int(self.qualite_map.get(thematique, self.default_qualite))

    def precisionplanimetrique(self, thematique):
        return '%03d' % int(self.precisionxy_map.get(thematique, self.default_precisionxy))

    def precisionaltimetrique(self, thematique):
        return '%03d' % int(self.precisionz_map.get(thematique, self.default_precisionz))

    def precisionxy(self, thematique):
        return int(self.precisionxy_map.get(thematique, self.default_precisionxy))

    def precisionz(self, thematique):
        return int(self.precisionz_map.get(thematique, self.default_precisionz))


class ObjetVecteurPCRS(PCRSObjectDefinition):
    # abstract class

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        if not is_integer(row['CategorieThematiquePCRSType']):
            raise Exception('''La valeur "%s" n'est pas prise en charge pour la colonne CategorieThematiquePCRSType''' %
                            row['CategorieThematiquePCRSType'])
        self.thematique = '%02d' % int(row['CategorieThematiquePCRSType'])

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature)
        }

        if self.date_leve:
            feature_dict['pcrs:dateLeve'] = self.date_leve

        feature_dict.update({
            'pcrs:idObjet': self.idobjet(feature),
            'pcrs:thematique': self.thematique,
            'pcrs:qualiteCategorisation': self.qualite(self.thematique),
            'pcrs:precisionPlanimetrique': self.precisionplanimetrique(self.thematique),
            'pcrs:precisionAltimetrique': self.precisionaltimetrique(self.thematique),
            'pcrs:producteur': self.producteur,
            'pcrs:geometrie': self.gml_geom(feature)
        })

        return feature_dict


class HabillagePCRS(PCRSObjectDefinition):
    # abstract class

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        if not is_integer(row['CategorieThematiquePCRSType']):
            raise Exception('''La valeur "%s" n'est pas prise en charge pour la colonne CategorieThematiquePCRSType''' %
                            row['CategorieThematiquePCRSType'])
        self.thematique = '%02d' % int(row['CategorieThematiquePCRSType'])

    def idhabillage(self, feature):
        handle = feature.GetField('handle')
        return '%s%s' % (self.prefixe_id, handle)

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:idHabillage': self.idhabillage(feature),
            'pcrs:thematique': self.thematique,
            'pcrs:geometrie': self.gml_geom(feature)
        }
        return feature_dict


class AffleurantPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'AffleurantPCRS'

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        nature_affleurant = row['NatureAffleurantPCRSType']
        if nature_affleurant == UNKNOWN_VALUE:
            self.nature = None
        elif not is_integer(nature_affleurant):
            raise Exception('''La valeur "%s" n'est pas prise en charge pour la colonne NatureAffleurantPCRSType''' %
                            nature_affleurant)
        else:
            self.nature = '%02d' % int(nature_affleurant)

        self.reseau = row['NatureReseauPCRSType']

        self.source = row["type d'objet"]

    def symbolisation(self, feature):
        return None

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
        }

        if self.date_leve:
            feature_dict['pcrs:dateLeve'] = self.date_leve

        feature_dict.update(
            {
                'pcrs:idObjet': self.idobjet(feature),
                'pcrs:thematique': self.thematique,
                'pcrs:qualiteCategorisation': self.qualite(self.thematique),
                'pcrs:precisionPlanimetrique': self.precisionplanimetrique(self.thematique),
                'pcrs:precisionAltimetrique': self.precisionaltimetrique(self.thematique),
                'pcrs:producteur': self.producteur
            })

        gestionnaire = self.gestionnaire(self.reseau)
        if gestionnaire:
            feature_dict['pcrs:gestionnaire'] = gestionnaire

        if self.nature is not None:
            feature_dict['pcrs:nature'] = self.nature

        feature_dict['pcrs:reseau'] = self.reseau

        feature_dict['pcrs:source'] = self.source

        # add symbol if there is one
        symbolisation = self.symbolisation(feature)
        if symbolisation is not None:
            feature_dict['pcrs:symbolisation'] = symbolisation

        # add geometric representation
        feature_dict['pcrs:representation'] = self.representation(feature)

        return feature_dict


class AffleurantPointPCRS(AffleurantPCRS):
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def representation(self, feature):
        return {
            'pcrs:AffleurantGeometriquePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:point': [{
                    'pcrs:AffleurantPointPCRS': {
                        '@gml:id': get_random_unique_id(),
                        'pcrs:geometrie': self.gml_geom(feature)
                    }
                }
                ]
            }
        }


class AffleurantLignesPCRS(AffleurantPCRS):
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbMultiLineString, ogr.wkbMultiLineString25D,
                           )

    def representation(self, feature):
        return {
            'pcrs:AffleurantGeometriquePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:ligne': [{
                    'pcrs:AffleurantLignesPCRS': {
                        '@gml:id': get_random_unique_id(),
                        'pcrs:geometrie': self.gml_geom(feature)
                    }
                }
                ]
            }
        }

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return as_multilinestring(geom)

        # MultilineString 2D / 3D
        elif geom_type in (ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ):
            return geom

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour AffleurantLignesPCRS''' % geom.GetGeometryName())


class AffleurantSymbolePCRS(AffleurantPCRS):
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D,)

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        self.reference = row['reference']

    def representation(self, feature):
        return {
            'pcrs:AffleurantGeometriquePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:point': [{
                    'pcrs:AffleurantPointPCRS': {
                        '@gml:id': get_random_unique_id(),
                        'pcrs:geometrie': self.gml_geom(feature)
                    }
                }
                ]
            }
        }

    def symbolisation(self, feature):
        return {
            'pcrs:AffleurantSymbolePCRS': {
                '@gml:id': get_random_unique_id(),
                'pcrs:geometrie': self.gml_geom(feature),
                'pcrs:reference': self.reference,
                'pcrs:angleRotation': feature.GetField('rotation'),
                'pcrs:longueur_mm': feature.GetField('dimx') * 1000.0,
                'pcrs:largeur_mm': feature.GetField('dimy') * 1000.0
            }
        }


class PointLevePCRS(PCRSObjectDefinition):
    PCRS_CLASS = 'PointLevePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        self.thematique = '01'  # TOPO

    def gml_dict(self, feature):
        feature_dict = {
            '@gml:id': self.gml_id(feature),
            'pcrs:geometrie': self.gml_geom(feature),
            'pcrs:numeroPoint': self.idobjet(feature),
            'pcrs:precisionXY': self.precisionxy(self.thematique),
            'pcrs:precisionZ': self.precisionz(self.thematique),
        }

        if self.horodatage:
            feature_dict['pcrs:horodatage'] = self.horodatage

        feature_dict.update(
            {
                'pcrs:producteur': self.producteur
            })

        return feature_dict


class FacadePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'FacadePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return exterior_ring(geom)

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour MurPCRS''' % geom.GetGeometryName())


class ProeminenceBatiPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'ProeminenceBatiPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class ArbrePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'ArbrePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        self.reference = row['reference']

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:reference'] = self.reference
        return feature_dict


class QuaiFluvioMaritimePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'QuaiFluvioMaritimePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class RailPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'RailPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D, )


class MurPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'MurPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D,)

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        categorie_mur = row['CategorieMurPCRSType']
        if categorie_mur == UNKNOWN_VALUE:
            self.typemur = None
        elif not is_integer(categorie_mur):
            raise Exception(
                '''La valeur "%s" n'est pas prise en charge pour la colonne CategorieMurPCRSType''' % categorie_mur)
        else:
            self.typemur = '%02d' % int(categorie_mur)

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)

        if self.typemur:
            feature_dict['pcrs:typeMur'] = self.typemur

        return feature_dict

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return exterior_ring(geom)

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour MurPCRS''' % geom.GetGeometryName())


class MarcheEscalierPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'MarcheEscalierPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPoint, ogr.wkbPoint25D,)

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        str_longueur_block = row['longueur_block']
        str_longueur_block = str_longueur_block.replace(',', '.')
        self.longueur_block = float(str_longueur_block)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Point 2D / 3D
        elif geom_type in (ogr.wkbPoint, ogr.wkbPoint25D, ):
            return as_linestring(geom,
                                 self.longueur_block,
                                 feature.GetField('xscale'),
                                 feature.GetField('rotation'),
                                 )

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour LimiteVoiriePCRS''' % geom.GetGeometryName())


class SeuilPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'SeuilPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPoint, ogr.wkbPoint25D,)

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        str_longueur_block = row['longueur_block']
        str_longueur_block = str_longueur_block.replace(',', '.')
        self.longueur_block = float(str_longueur_block)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Point 2D / 3D
        elif geom_type in (ogr.wkbPoint, ogr.wkbPoint25D, ):
            return as_linestring(geom,
                                 self.longueur_block,
                                 feature.GetField('xscale'),
                                 feature.GetField('rotation'),
                                 )

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour LimiteVoiriePCRS''' % geom.GetGeometryName())


class LimiteVoiriePCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'LimiteVoiriePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return exterior_ring(geom)

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour LimiteVoiriePCRS''' % geom.GetGeometryName())


class PilePontPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PilePontPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPolygon, ogr.wkbPolygon25D, )


class PointCanevasPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PointCanevasPCRS'

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        # TODO: add optional attributes ?
        # self.canevas =
        # self.precisionxy = int(row['precisionplanimetrique'])
        # self.precisionz = int(row['precisionaltimetrique'])
        # self.immatriculation =
        # self.dateCreation =
        # self.fiche =

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        # TODO: add optional attributes ?
        return feature_dict


class HabillageEnveloppePCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageEnveloppePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPolygon, ogr.wkbPolygon25D,
                           ogr.wkbLineString, ogr.wkbLineString25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # Polygon 2D / 3D
        if geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return geom

        # LineString 2D / 3D
        elif geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return as_polygon(geom)

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour HabillageLignesPCRS''' % geom.GetGeometryName())


class HabillageLignesPCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageLignesPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbLineString, ogr.wkbLineString25D,
                           ogr.wkbMultiLineString, ogr.wkbMultiLineString25D,
                           ogr.wkbPolygon, ogr.wkbPolygon25D,)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # LineString 2D / 3D
        if geom_type in (ogr.wkbLineString, ogr.wkbLineString25D, ):
            return as_multilinestring(geom)

        # MultilineString 2D / 3D
        elif geom_type in (ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ):
            return geom

        # Polygon 2D / 3D
        elif geom_type in (ogr.wkbPolygon, ogr.wkbPolygon25D, ):
            return as_multilinestring(exterior_ring(geom))

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour HabillageLignesPCRS''' % geom.GetGeometryName())


class HabillagePointsPCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillagePointsPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D,
                           ogr.wkbMultiPoint, ogr.wkbMultiPoint25D)

    def get_ogr_geometry(self, feature):
        geom = feature.GetGeometryRef()
        geom_type = geom.GetGeometryType()

        # Point 2D / 3D
        if geom_type in (ogr.wkbPoint, ogr.wkbPoint25D, ):
            return as_multipoint(geom)

        # MultiPoint 2D / 3D
        elif geom_type in (ogr.wkbMultiPoint, ogr.wkbMultiPoint25D, ):
            return geom

        # Other geom type
        else:
            raise NotImplementedError(
                '''Le type géométrique %s n'est pas pris en charge pour HabillagePointsPCRS''' % geom.GetGeometryName())


class HabillageSymbolePCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageSymbolePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        self.reference = row['reference']

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:reference'] = self.reference
        feature_dict['pcrs:angleRotation'] = feature.GetField('rotation')

        dimx = feature.GetField('dimx')
        if dimx:
            feature_dict['pcrs:longueur_mm'] = dimx * 1000.0

        dimy = feature.GetField('dimx')
        if dimy:
            feature_dict['pcrs:largeur_mm'] = dimy * 1000.0

        return feature_dict


class HabillageTextePCRS(HabillagePCRS):
    PCRS_CLASS = 'HabillageTextePCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        self.reference = row['reference']
        self.justification = 'C'  # TODO: check this mapping
        self.taille = 10.0  # TODO: check this mapping

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:libelle'] = feature.GetField('text')
        feature_dict['pcrs:angleRotation'] = feature.GetField('rotation')
        feature_dict['pcrs:justification'] = self.justification
        feature_dict['pcrs:taille'] = self.taille
        return feature_dict


class PilierRegulierPCRS(ObjetVecteurPCRS):
    PCRS_CLASS = 'PilierRegulierPCRS'
    EXPECTED_GEOM_TYPES = (ogr.wkbPoint, ogr.wkbPoint25D, )

    def __init__(self, schema, row, srs_name, producteur,
                 gestionnaire_map, default_gestionnaire,
                 qualite_map, default_qualite,
                 precisionxy_map, default_precisionxy,
                 precisionz_map, default_precisionz,
                 prefixe_id, date_leve, horodatage) -> None:
        super().__init__(schema, row, srs_name, producteur,
                         gestionnaire_map, default_gestionnaire,
                         qualite_map, default_qualite,
                         precisionxy_map, default_precisionxy,
                         precisionz_map, default_precisionz,
                         prefixe_id, date_leve, horodatage)

        if not is_integer(row['SectionPilierPCRSType']):
            raise Exception(
                '''La valeur "%s" n'est pas prise en charge pour la colonne SectionPilierPCRSType''' % row['SectionPilierPCRSType'])
        self.section = '%02d' % int(row['SectionPilierPCRSType'])

    def gml_dict(self, feature):
        feature_dict = super().gml_dict(feature)
        feature_dict['pcrs:section'] = self.section
        feature_dict['pcrs:angleRotation'] = feature.GetField('rotation')
        feature_dict['pcrs:longueur'] = feature.GetField('dimx') * 1000.0
        feature_dict['pcrs:largeur'] = feature.GetField('dimy') * 1000.0
        return feature_dict


PCRS_CONVERSION_CLASSES = {
    'AffleurantLignesPCRS': AffleurantLignesPCRS,
    'AffleurantSymbolePCRS': AffleurantSymbolePCRS,  # TODO: check if it fine
    'ArbrePCRS': ArbrePCRS,
    'FacadePCRS': FacadePCRS,
    'HabillageEnveloppePCRS': HabillageEnveloppePCRS,
    'HabillageLignesPCRS': HabillageLignesPCRS,
    'HabillagePointsPCRS': HabillagePointsPCRS,
    'HabillageSymbolePCRS': HabillageSymbolePCRS,
    'HabillageTextePCRS': HabillageTextePCRS,
    'LimiteVoiriePCRS': LimiteVoiriePCRS,
    'MarcheEscalierPCRS': MarcheEscalierPCRS,
    'MurPCRS': MurPCRS,
    'PilePontPCRS': PilePontPCRS,
    'PilierRegulierPCRS': PilierRegulierPCRS,
    'PointCanevasPCRS': PointCanevasPCRS,
    'PointLevePCRS': PointLevePCRS,
    'ProeminenceBatiPCRS': ProeminenceBatiPCRS,
    'QuaiFluvioMaritimePCRS': QuaiFluvioMaritimePCRS,
    'RailPCRS': RailPCRS,
    'SeuilPCRS': SeuilPCRS,
}


def convert_dxf_to_gml(input_gpkg_path, srs_name,
                       csv_data_model_path,
                       gml_xsd_path,
                       output_gml_path,
                       logger,
                       check_name,
                       producteur,
                       default_gestionnaire,
                       gestionnaire_map,
                       default_qualite,
                       qualite_map,
                       default_precisionxy,
                       precisionxy_map,
                       default_precisionz,
                       precisionz_map,
                       prefixe_id,
                       date_leve,
                       horodatage):
    # Open GPKG
    logger.info(check_name, 'Ouverture du fichier GPKG à convertir')
    gpkg_ds = ogr.Open(input_gpkg_path)
    num_gpkg_layers = gpkg_ds.GetLayerCount()

    logger.info(check_name, 'Chargement du schéma PCRS au format XSD')
    pcrs_v20_schema = xmlschema.XMLSchema(gml_xsd_path)

    # Load objects that can be exported to PCRS
    logger.info(check_name, 'Import du modèle de données CSV')
    pcrs_calques_objets = {}
    with open(csv_data_model_path) as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=',')
        for row in csvreader:
            calque = row.get('calque', '')
            if not calque:
                continue

            pcrs_value = row.get('pcrs', '')
            if pcrs_value in (None, '', UNKNOWN_VALUE):
                continue

            if pcrs_value in PCRS_CONVERSION_CLASSES:
                if not calque in pcrs_calques_objets:
                    pcrs_calques_objets[calque] = {}

                # get entity type id
                entity_type_id = row['id']

                # load conversion class
                conversion_class = PCRS_CONVERSION_CLASSES[pcrs_value]

                # instanciate conversion class
                try:
                    converter = conversion_class(pcrs_v20_schema,
                                                 row,
                                                 srs_name,
                                                 producteur,
                                                 gestionnaire_map,
                                                 default_gestionnaire,
                                                 qualite_map,
                                                 default_qualite,
                                                 precisionxy_map,
                                                 default_precisionxy,
                                                 precisionz_map,
                                                 default_precisionz,
                                                 prefixe_id,
                                                 date_leve,
                                                 horodatage
                                                 )
                except Exception as e:
                    logger.error(check_name,
                                 '''Le convertisseur PCRS n'a pas été créé pour la couche %s / objet %s''' % (calque, entity_type_id))
                    logger.error(check_name, e)  # , exc_info=True)

                else:
                    # add converter
                    pcrs_calques_objets[calque][entity_type_id] = converter
            else:
                logger.error(
                    check_name, 'Valeur PCRS non reconnue "%s"' % pcrs_value)

    # Create output file
    with open(output_gml_path, 'w') as gml_file:
        gml_file.write('''<?xml version="1.0" encoding="UTF-8"?>\n''')
        gml_file.write('''<PlanCorpsRueSimplifie xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns="http://cnig.gouv.fr/pcrs" gml:id="PlanCorpsRueSimplifie.1" version="2.0" xsi:schemaLocation="http://cnig.gouv.fr/pcrs CNIG_PCRS_v2.0.xsd">\n''')

        logger.info(
            check_name, 'Lecture du fichier Geopackage et création du GML')
        total_num_features_written = 0
        for idx in range(num_gpkg_layers):
            layer = gpkg_ds.GetLayer(idx)

            # check if layer has objects that can be exported
            layer_name = layer.GetName()
            if layer_name not in pcrs_calques_objets:
                logger.warning(check_name,
                               'Couche %s non traitée car aucune conversion PCRS est définie' % layer_name)
                continue

            logger.info(check_name, 'Export de la couche %s' % layer_name)

            # get layer objects that can be exported to PCRS
            def_pcrs_objets = pcrs_calques_objets[layer_name]

            # iterate over layer features
            layer.ResetReading()
            feature = layer.GetNextFeature()
            num_features_read = 0
            num_features_written = 0
            entity_types_not_written = set()
            while feature is not None:
                num_features_read += 1

                # get entity type id
                entity_type_id = feature.GetField('entity_type_id')
                if entity_type_id not in def_pcrs_objets:
                    entity_types_not_written.add(entity_type_id)
                    feature = layer.GetNextFeature()
                    continue

                # get converter for object
                converter = def_pcrs_objets[entity_type_id]

                # get feature xml string
                gml_feature_string = converter.as_xml_string(feature)

                # remove pcrs namespace before writing (as XML header allows to do so)
                gml_feature_string = gml_feature_string.replace('pcrs:', '')

                # write feature
                gml_file.write(gml_feature_string)
                gml_file.write('\n')
                num_features_written += 1

                feature = layer.GetNextFeature()

            logger.info(check_name, 'Nombre objets lus pour la couche %s : %d' % (layer_name,
                                                                                  num_features_read))
            logger.info(check_name, 'Nombre objets exportés pour la couche %s : %d' % (layer_name,
                                                                                       num_features_written))
            if entity_types_not_written:
                logger.warning(check_name, '''Types d'objets non exportés pour la couche %s : %s''' % (layer_name,
                                                                                                       ', '.join(entity_types_not_written)))
            total_num_features_written += num_features_written

        gml_file.write('''</PlanCorpsRueSimplifie>''')
    # Close GML file

    logger.info(check_name, '''Nombre total d'objets exportés : %d''' %
                total_num_features_written)

    # Check GML conformity
    logger.info(check_name,
                'Vérification de la conformité du fichier XML par rapport au XSD')
    try:
        pcrs_v20_schema.validate(output_gml_path)
        logger.info(check_name, 'Le GML est conforme !')
    except xmlschema.XMLSchemaValidationError as e:
        logger.error(check_name, str(e))
