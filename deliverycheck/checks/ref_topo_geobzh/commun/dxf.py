import os.path
import ezdxf
from ezdxf.blkrefs import BlockDefinitionIndex
from osgeo import ogr, osr
import json
import math
from deliverycheck.checks import check_method
from deliverycheck.checks.ref_topo_geobzh import SocleCommunBaseCheck
from deliverycheck.util.formats.ogr import OGRWriter, get_representative_point_from_geometry
from deliverycheck.util.formats.dxf import (get_entity_type_id,
                                            get_insert_block_name,
                                            get_ogr_geometry,
                                            get_entity_verbose_name,
                                            get_entity_rgb_color,
                                            get_entity_aci_rgb_color,
                                            get_entity_text,
                                            get_insert_entity_measurements)
from deliverycheck.util.misc import remove_accents, remove_file_if_exists

from .constants import socle_commun_dxf_config
from .dxf2gml import convert_dxf_to_gml

# ezdxf entity types allowed for each object type (see CALQUES_OBJETS)
OBJECT_TYPE_TO_ALLOWED_EZDXF_ENTITY_TYPES = {
    'Point': ezdxf.entities.Point,
    'Bloc': ezdxf.entities.Insert,
    'Polyligne': (ezdxf.entities.Polyline, ezdxf.entities.LWPolyline, ezdxf.entities.Line,),
    'Hachures': (ezdxf.entities.Hatch, ezdxf.entities.Polyline, ezdxf.entities.LWPolyline,),
    'Texte': ezdxf.entities.MText
}

# conversion from input geometry types to geometry types to write in GIS export
GIS_GEOMETRY_TYPE_EXPORT_MAP = {
    ogr.wkbPoint: ogr.wkbPoint25D,
    ogr.wkbPoint25D: ogr.wkbPoint25D,
    ogr.wkbLineString: ogr.wkbLineString25D,
    ogr.wkbLineString25D: ogr.wkbLineString25D,
    ogr.wkbPolygon: ogr.wkbPolygon25D,
    ogr.wkbPolygon25D: ogr.wkbPolygon25D,
    ogr.wkbMultiLineString: ogr.wkbMultiLineString25D,
    ogr.wkbMultiLineString25D: ogr.wkbMultiLineString25D,
    ogr.wkbGeometryCollection: ogr.wkbGeometryCollection25D,
    ogr.wkbGeometryCollection25D: ogr.wkbGeometryCollection25D,
}

# conversion from GIS export geometry types to layer prefix
GIS_GEOMETRY_TYPE_TO_PREFIX_MAP = {
    ogr.wkbPoint25D: 'POINT',
    ogr.wkbLineString25D: 'LINE',
    ogr.wkbPolygon25D: 'POLYGON',
    ogr.wkbMultiLineString25D: 'MULTILINE',
    ogr.wkbGeometryCollection25D: 'MULTI'
}


def selection_attr(dxf_handle):
    """Returns an AutoCAD command string that can be used to zoom on an entity using its handle

    Args:
        dxf_handle (str): DXF entity handle

    Returns:
        str: AutoCAD command string
    """
    return 'SELECT (HANDENT "%s")' % dxf_handle


class SocleCommunDXFCheck(SocleCommunBaseCheck):
    user_doc = '''Standard topographique régional / Socle commun / Format DXF'''

    CONSTANTS_CONFIG = socle_commun_dxf_config

    def __init__(self, path, report_dir, srs,
                 bounds_file_path=None, bounds_srs=None,
                 control_points_path=None, control_points_ogr_driver_name='GPKG', control_points_srs=None, control_points_accuracy_config_path=None,
                 dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None,
                 gml_export_options={}
                 ) -> None:
        super().__init__(path, report_dir, srs, bounds_file_path, bounds_srs,
                         control_points_path, control_points_ogr_driver_name, control_points_srs, control_points_accuracy_config_path,
                         dtm_path, dtm_srs, dtm_accuracy_config_path, gml_export_options)

        # add a custom log attribute
        self.additional_log_attrs.append('selection')

    @check_method('CONVERSION_GEOPACKAGE',
                  """Vérifie que le fichier DXF peut être converti en Geopackage""")
    def __create_geopackage(self, check_name):
        """
        Converts input DXF dataset as a Geopackage to perform somes checks.
        This geopackage file is also a check artefact, that can be used to visualize
        input dataset in GIS software.
        Beware only DXF entities in modelspace can be converted as geopackage

        Returns:
            tupe (ogr.DataSource, list): geopackage OGR datasource and layer names list

        """
        self.info(None, 'Conversion des données au format GEOPACKAGE...')

        # create GPKG file
        gpkg_driver = ogr.GetDriverByName("GPKG")
        if os.path.exists(self.gpkg_filename):
            gpkg_driver.DeleteDataSource(self.gpkg_filename)
        ds = gpkg_driver.CreateDataSource(self.gpkg_filename)
        writers = {}

        # create GIS GPKG file
        if os.path.exists(self.gis_gpkg_filename):
            gpkg_driver.DeleteDataSource(self.gis_gpkg_filename)
        gis_gpkg_ds = gpkg_driver.CreateDataSource(self.gis_gpkg_filename)
        gis_writers = {}
        gis_additionnal_layer_fields = {}

        point_fields = [
            ogr.FieldDefn('block_name', ogr.OFTString),
            ogr.FieldDefn('xscale', ogr.OFTReal),
            ogr.FieldDefn('yscale', ogr.OFTReal),
            ogr.FieldDefn('dimx', ogr.OFTReal),
            ogr.FieldDefn('dimy', ogr.OFTReal),
            ogr.FieldDefn('zscale', ogr.OFTReal),
            ogr.FieldDefn('rotation', ogr.OFTReal),
            ogr.FieldDefn('text', ogr.OFTString),
        ]

        default_fields = [
            ogr.FieldDefn('type', ogr.OFTString),
            ogr.FieldDefn('handle', ogr.OFTString),
            ogr.FieldDefn('entity_type_id', ogr.OFTString),
            ogr.FieldDefn('object_name', ogr.OFTString),
            ogr.FieldDefn('famille', ogr.OFTString),
            ogr.FieldDefn('libelle_type', ogr.OFTString),
            ogr.FieldDefn('color', ogr.OFTString),
        ] + point_fields

        default_gis_fields = [
            ogr.FieldDefn('handle', ogr.OFTString),
            ogr.FieldDefn('color', ogr.OFTString),
        ]

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):

                # list all DXF layers for current entity
                # it is of course the entity layer
                # but, if entity is an INSERT, it also can be the entity attributes
                # layers that can be different from the entity layer (OMG !)
                # in this case, entity will be written to multiple layers
                # for each layer, only attribs belonging to the layer will be added
                # to the OGR feature
                entity_dxf_layer_name = entity.dxf.layer
                dxf_layer_names = set([entity_dxf_layer_name])
                if isinstance(entity, ezdxf.entities.Insert):
                    for a in entity.attribs:
                        dxf_layer_names.add(a.dxf.layer)

                for dxf_layer_name in dxf_layer_names:

                    ###############################################
                    # Internal GPKG Export
                    ###############################################

                    # access existing writer or create layer/writer
                    # for DXF layer name
                    if dxf_layer_name in writers:
                        writer = writers[dxf_layer_name]
                    else:
                        # create geopackage layer
                        layer = ds.CreateLayer(dxf_layer_name,
                                               self.data_srs,
                                               ogr.wkbUnknown)
                        # create default fields
                        for field in default_fields:
                            layer.CreateField(field)

                        # add attribs field
                        layer.CreateField(ogr.FieldDefn(
                            'attribs', ogr.OFTString))

                        # create and register writer for layer
                        writer = OGRWriter(layer)
                        writers[dxf_layer_name] = writer

                    # create ogr.Feature
                    feature = ogr.Feature(writer.GetLayerDefn())

                    # compute entity type id
                    entity_type_id = get_entity_type_id(entity)

                    # convert entity geometry as ogr.Geometry
                    try:
                        geom = get_ogr_geometry(entity,
                                                self.CONSTANTS_CONFIG.EPSILON)
                    except NotImplementedError as e:
                        self.error(check_name,
                                   '''Erreur sur l'entité DXF "%s" : %s''' % (entity_type_id, str(e)))
                        # move on to next entity
                        continue

                    # set geometry
                    feature.SetGeometry(geom)

                    # add DXF type
                    feature.SetField('type', entity.DXFTYPE)

                    # add DXF handle
                    feature.SetField('handle', entity.dxf.handle)

                    # set entity type id
                    feature.SetField('entity_type_id', entity_type_id)

                    # compute entity famille
                    feature.SetField('famille',
                                     self.get_famille(dxf_layer_name))

                    # compute entity type label
                    libelle_type_objet = self.get_libelle_type_objet(dxf_layer_name,
                                                                     entity_type_id)
                    feature.SetField('libelle_type', libelle_type_objet)

                    # compute object verbose name by overriding the entity layer
                    # (useful for INSERT entities having some attributes belonging to other layers)
                    object_name = get_entity_verbose_name(entity,
                                                          dxf_layer_name)
                    feature.SetField('object_name', object_name)

                    # set information for INSERTs
                    if isinstance(entity, ezdxf.entities.Insert):
                        block_name = get_insert_block_name(entity)
                        feature.SetField('block_name', block_name)
                        feature.SetField('xscale', entity.dxf.xscale)
                        feature.SetField('yscale', entity.dxf.yscale)
                        feature.SetField('zscale', entity.dxf.zscale)
                        feature.SetField('rotation', entity.dxf.rotation)
                        dimx, dimy = get_insert_entity_measurements(entity)
                        feature.SetField('dimx', dimx)
                        feature.SetField('dimy', dimy)

                    # set information for Texts
                    if isinstance(entity, (ezdxf.entities.Text,
                                           ezdxf.entities.MText)):
                        feature.SetField('rotation', entity.dxf.rotation)

                    # compute color
                    entity_rgb_color = get_entity_rgb_color(self.dxf_doc,
                                                            entity)
                    entity_hex_color = '#%02x%02x%02x' % entity_rgb_color
                    feature.SetField('color', entity_hex_color)

                    # set entity text
                    feature.SetField('text', get_entity_text(entity))

                    # add additional attributes if entity is an INSERT
                    attribs = {}
                    if isinstance(entity, ezdxf.entities.Insert):
                        for a in entity.attribs:
                            # only write attributes that belong to current layer
                            if a.dxf.layer == dxf_layer_name:
                                attribs[a.dxf.tag] = a.dxf.text
                        feature.SetField('attribs', json.dumps(attribs))

                    # add feature to writer
                    writer.add_feature(feature)

                    ###############################################
                    # Update GIS GPKG Export definition
                    ###############################################

                    # create GIS layer if required and add feature
                    if dxf_layer_name in gis_writers:
                        layer_gis_writers = gis_writers[dxf_layer_name]
                    else:
                        layer_gis_writers = {}

                    input_geom_type = geom.GetGeometryType()
                    dest_geom_type = GIS_GEOMETRY_TYPE_EXPORT_MAP[input_geom_type]
                    if dest_geom_type not in layer_gis_writers:
                        # compute GIS layer name
                        gis_layer_name = '%s_%s' % (dxf_layer_name,
                                                    GIS_GEOMETRY_TYPE_TO_PREFIX_MAP[dest_geom_type])

                        # create geopackage layer
                        gis_layer = gis_gpkg_ds.CreateLayer(gis_layer_name,
                                                            self.data_srs,
                                                            dest_geom_type)
                        # create default fields
                        for field in default_gis_fields:
                            gis_layer.CreateField(field)

                        # add point fields if required
                        if dest_geom_type == ogr.wkbPoint25D:
                            for field in point_fields:
                                gis_layer.CreateField(field)

                        # create and register writer for layer
                        gis_writer = OGRWriter(gis_layer)
                        layer_gis_writers[dest_geom_type] = gis_writer
                        gis_writers[dxf_layer_name] = layer_gis_writers
                    else:
                        gis_writer = layer_gis_writers[dest_geom_type]

                    # update GIS layer definition if required
                    if dxf_layer_name not in gis_additionnal_layer_fields:
                        gis_additionnal_layer_fields[dxf_layer_name] = {}
                    additionnal_layer_fields = gis_additionnal_layer_fields[dxf_layer_name].get(
                        dest_geom_type, [])
                    for attname in attribs.keys():
                        # add field to layer if required
                        if not gis_writer.has_field_defn(attname, case_sensitive=False):
                            # add new field to layer
                            gis_writer.CreateField(
                                ogr.FieldDefn(str(attname), ogr.OFTString))
                            # update list of additional fields for layer
                            if attname not in additionnal_layer_fields:
                                additionnal_layer_fields.append(attname)
                    gis_additionnal_layer_fields[dxf_layer_name][dest_geom_type] = additionnal_layer_fields

        # flush / close writers
        for writer in writers.values():
            writer.close()

        # create indexes on fields
        for layer_name in writers:
            for field_name in ('object_name', 'entity_type_id', 'famille'):
                ds.ExecuteSQL('''
                    CREATE INDEX "idx_%(layer_name)s_%(field_name)s" ON "%(layer_name)s"("%(field_name)s");
                ''' %
                              {
                                  'layer_name': layer_name,
                                  'field_name': field_name
                              })

        # Fill GIS layers now we know their definition
        num_layers = ds.GetLayerCount()
        for idx in range(num_layers):
            layer = ds.GetLayer(idx)
            layer_name = layer.GetName()
            layer.ResetReading()
            feature = layer.GetNextFeature()
            while feature is not None:
                # get gis writer
                geom = feature.GetGeometryRef()
                input_geom_type = geom.GetGeometryType()
                dest_geom_type = GIS_GEOMETRY_TYPE_EXPORT_MAP[input_geom_type]
                gis_writer = gis_writers[layer_name][dest_geom_type]

                # create GIS feature
                gis_feature = ogr.Feature(gis_writer.GetLayerDefn())
                gis_feature.SetFrom(feature)

                additionnal_layer_fields = gis_additionnal_layer_fields[layer_name][dest_geom_type]
                if additionnal_layer_fields:
                    attribs_str = feature.GetField('attribs')
                    if attribs_str:
                        attribs = json.loads(attribs_str)
                        for attname, attvalue in attribs.items():
                            # add field to feature
                            gis_feature.SetField(attname, attvalue)

                # # add GIS feature to GIS writer
                gis_writer.add_feature(gis_feature)

                feature = layer.GetNextFeature()

        # flush / close GIS writers
        for gis_layer_writer in gis_writers.values():
            for gis_writer in gis_layer_writer.values():
                gis_writer.close()

        # close GIS datasource
        gis_gpkg_ds = None

        # set global variable
        self.gpkg_ds = ds
        self.layer_names = writers.keys()

    def _add_xlsx_check_reports_tabs(self, xls_ds):
        super()._add_xlsx_check_reports_tabs(xls_ds)

        # create object report layer
        object_layer = xls_ds.CreateLayer('objets')

        # add base fields
        object_layer.CreateField(ogr.FieldDefn('calque', ogr.OFTString))
        object_layer.CreateField(ogr.FieldDefn('id', ogr.OFTString))
        object_layer.CreateField(ogr.FieldDefn('handle', ogr.OFTString))
        object_layer.CreateField(ogr.FieldDefn('controle', ogr.OFTString))
        object_layer.CreateField(ogr.FieldDefn('criticite', ogr.OFTString))
        object_layer.CreateField(ogr.FieldDefn('description', ogr.OFTString))

        # create writer
        object_writer = OGRWriter(object_layer)

        for log in self.check_logs:
            if not hasattr(log, 'layer_name'):
                continue

            object_feature = ogr.Feature(object_writer.GetLayerDefn())

            object_feature.SetField('calque', log.layer_name)

            if hasattr(log, 'block_name'):
                object_feature.SetField('id', log.block_name)

            if hasattr(log, 'handle'):
                object_feature.SetField('handle', log.handle)

            object_feature.SetField('controle', log.check_name)

            object_feature.SetField('criticite', log.criticity)

            if hasattr(log, 'check_message'):
                object_feature.SetField('description', str(log.check_message))

            # add feature to writer
            object_writer.add_feature(object_feature)

        # flush writer
        object_writer.close()

    def _generate_initial_checks(self):
        # try reading file
        self.info(None, 'Ouverture du fichier DXF %s' % self.path)
        self.dxf_doc = ezdxf.readfile(self.path)
        self.dxf_modelspace = self.dxf_doc.layouts.modelspace()

        # first check : create geopackage file from DXF
        yield self.__create_geopackage()

        if self.error_found:
            self.info(None, '''Une ou plusieurs erreurs sont apparues lors de la conversion en Geopackage. Les contrôles ne peuvent pas s'exécuter tant que celles-ci ne sont pas corrigées''')
        else:
            # run checks
            self.info(None, 'Exécution des contrôles...')
            yield self.check_2_unused_blocks_and_plans()
            yield self.check_3_geometries_in_bounds()
            yield self.check_5_cartouche()
            yield self.check_8_objects_in_layer_0()
            yield self.check_9_allowed_geometries()
            yield self.check_11_hatch_fill()
            yield self.check_12_blocks_attributes()
            yield self.check_13_allowed_objects_per_layer()
            yield self.check_14_colors()
            yield self.check_16_text_placement()
            yield self.check_18_uppercase_text()
            yield self.check_19_leve_topo()
            yield self.check_21_leve_topo_alt_mat()
            yield self.check_27_control_points()
            yield self.check_27_dtm()
            yield self.check_29_doublons()

    def generate_checks(self):
        # first, execute conformity checks
        yield from self._generate_initial_checks()
        # then try to generate GML
        yield self.check_gml_export()

    def entity_error(self, check_name, check_message, layer_name, entity):
        """Log an error on a DXF entity
        """

        object_name = get_entity_verbose_name(entity)

        geometry = get_ogr_geometry(entity,
                                    self.CONSTANTS_CONFIG.EPSILON)

        handle = entity.dxf.handle

        if isinstance(entity, ezdxf.entities.Insert):
            block_name = get_insert_block_name(entity)
        else:
            block_name = ''

        self.error(check_name,
                   check_message,
                   layer_name=layer_name,
                   object_name=object_name,
                   geometry=geometry.Clone(),
                   handle=handle,
                   block_name=block_name,
                   selection=selection_attr(handle))

    def entity_warn(self, check_name, check_message, layer_name, entity):
        """Log a warning on a DXF entity
        """

        object_name = get_entity_verbose_name(entity)

        geometry = get_ogr_geometry(entity,
                                    self.CONSTANTS_CONFIG.EPSILON)

        handle = entity.dxf.handle

        if isinstance(entity, ezdxf.entities.Insert):
            block_name = get_insert_block_name(entity)
        else:
            block_name = ''

        self.warn(check_name,
                  check_message,
                  layer_name=layer_name,
                  object_name=object_name,
                  geometry=geometry.Clone(),
                  handle=handle,
                  block_name=block_name,
                  selection=selection_attr(handle))

    def feature_error(self, check_name, check_message, layer_name, feature):
        """Log an error on a OGR feature (converted from a DXF entity)
        """
        object_name = feature.GetField('object_name')
        geometry = feature.GetGeometryRef().Clone()
        handle = feature.GetField('handle')
        block_name = feature.GetField('block_name')
        self.error(check_name,
                   check_message,
                   layer_name=layer_name,
                   object_name=object_name,
                   geometry=geometry.Clone(),
                   handle=handle,
                   block_name=block_name,
                   selection=selection_attr(handle))

    def feature_warn(self, check_name, check_message, layer_name, feature):
        """Log a warning on a OGR feature (converted from a DXF entity)
        """
        object_name = feature.GetField('object_name')
        geometry = feature.GetGeometryRef().Clone()
        handle = feature.GetField('handle')
        block_name = feature.GetField('block_name')
        self.warn(check_name,
                  check_message,
                  layer_name=layer_name,
                  object_name=object_name,
                  geometry=geometry.Clone(),
                  handle=handle,
                  block_name=block_name,
                  selection=selection_attr(handle))

    def get_famille(self, layer_name):
        """Returns expected famille for DXF layer name"""
        for row in self.CONSTANTS_CONFIG.CALQUES_OBJETS:
            if row['calque'] == layer_name:
                return row['famille']
        return None

    def get_libelle_type_objet(self, layer_name, entity_type_id):
        """Returns object type label for layer_name and entity type id"""
        # look for layer / entity type
        for oc in self.CONSTANTS_CONFIG.CALQUES_OBJETS:

            # check if object layer is found
            if oc['calque'] != layer_name:
                continue

            if oc['id'] == entity_type_id:
                return oc["type d'objet"]
        else:
            # no match was found with layer_name and entity_type_id
            # means this entity does not belong to model (see check 13)
            return None

    def is_habillage(self, entity):
        """returns if entity belongs to HABILLAGE family"""
        layer = entity.dxf.layer
        entity_type_id = get_entity_type_id(entity)
        expected_layer = self.CONSTANTS_CONFIG.CALQUES_OBJETS_HABILLAGE.get(
            entity_type_id, None)
        if expected_layer is None:
            return False
        return expected_layer == layer

    def get_alt_attributes(self, entity):
        """returns if entity must have an altitude attribute"""
        entity_type_id = get_entity_type_id(entity)
        if entity_type_id in self.CONSTANTS_CONFIG.CALQUES_OBJETS_ALT:
            return self.CONSTANTS_CONFIG.CALQUES_OBJETS_ALT[entity_type_id]
        else:
            return None

    @check_method('BLOC_NON_UTILISE',
                  """Vérifie que les fichiers sont purgés de tous les blocs et plans inutilisés""")
    def check_2_unused_blocks_and_plans(self, check_name):

        bdi = BlockDefinitionIndex(self.dxf_doc)
        for block_layout in self.dxf_doc.blocks:
            b = block_layout.block
            if not b.is_layout_block:
                if not bdi.has_name(b.dxf.name):
                    self.error(check_name,
                               'Le bloc "%s" semble non utilisé' % b.dxf.name)

    @check_method('HORS_ZONE_PROJET',
                  """
                  Vérifie que les données (hors famille HABILLAGE) sont conformes au système
                  de coordonnées RGF93-CC48 et incluses dans la zone du projet
                  """
                  )
    def check_3_geometries_in_bounds(self, check_name):

        if self.bounds_file_path is None or self.bounds_srs is None:
            self.info(check_name, '''Contrôle non exécuté car aucun fichier contenant
                             la zone projet n'a été spécifié, ou les projections
                             des données n'ont pas été spécificées''')
            return

        # read bounds file
        bounds_ds = ogr.Open(self.bounds_file_path)
        bounds_layer = bounds_ds.GetLayer()
        bounds_feature = bounds_layer.GetNextFeature()
        bounds_polygon = bounds_feature.GetGeometryRef()

        # reproject bounds
        bounds_srs = osr.SpatialReference()
        bounds_srs.SetFromUserInput(self.bounds_srs)
        bounds_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        srs_transform = osr.CoordinateTransformation(bounds_srs,
                                                     self.data_srs)
        bounds_polygon.Transform(srs_transform)

        # query features that are out of bounds in each layer
        for layer_name in self.layer_names:
            # TODO: use spatial index !
            sql_query = '''
                            SELECT
                                geom,
                                object_name,
                                block_name,
                                handle
                            FROM %s
                            WHERE
                                NOT famille = 'habillage'
                                AND
                                NOT ST_Within(geom, GeomFromText('%s'))
                        ''' % (layer_name,
                               bounds_polygon.ExportToWkt())
            layer = self.gpkg_ds.ExecuteSQL(sql_query)
            if layer is not None:
                feature = layer.GetNextFeature()
                while feature is not None:
                    self.feature_error(check_name,
                                       '''L'objet est en dehors de la zone projet''',
                                       layer_name,
                                       feature)
                    feature = layer.GetNextFeature()
            self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('CARTOUCHE',
                  """Vérifie que le cartouche est dans l’espace papier."""
                  )
    def check_5_cartouche(self, check_name):
        # check there is no HABILLAGE entity in wrong layers of modelspace
        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                layer_name = entity.dxf.layer
                entity_type_id = get_entity_type_id(entity)
                expected_layer = self.CONSTANTS_CONFIG.CALQUES_OBJETS_HABILLAGE.get(entity_type_id,
                                                                                    None)
                if expected_layer is None:
                    continue
                if layer_name != expected_layer:
                    self.entity_error(check_name,
                                      '''L'objet se trouve dans un calque non autorisé dans la charte pour les objets de la famille HABILLAGE''',
                                      layer_name,
                                      entity
                                      )

    @check_method('CALQUE_0',
                  """Vérifie qu'il n'existe aucun objet dans le calque 0.""")
    def check_8_objects_in_layer_0(self, check_name):

        if '0' in self.dxf_doc.layers:
            for entity in self.dxf_doc.entities:
                if (entity in self.dxf_modelspace) and hasattr(entity.dxf, 'layer') and entity.dxf.layer == '0' and isinstance(entity, ezdxf.entities.DXFGraphic):
                    self.entity_error(
                        check_name,
                        '''L'objet ne doit pas se trouver dans le calque 0. Ce calque doit être vide''',
                        '0',
                        entity)

    @check_method('GEOMETRIES_NON_AUTORISEES',
                  """
                  Vérifie que seuls les objets suivants sont présents :

                  - `POINT`
                  - `INSERT`
                  - `ATTRIB`
                  - `POLYLINE`
                  - `LINE`
                  - `LWPOLYLIGNE`
                  - `HATCH`
                  - `CIRCLE`
                  - `ARC`
                  - `DIMENSION`

                  Pour les objets comportant des arcs, remonte un warning pour indiquer que ces objets ont été convertis en SIG et doivent être vérifiés.
                  """
                  )
    def check_9_allowed_geometries(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                if isinstance(entity, ezdxf.entities.MText):
                    pass
                if not isinstance(entity, (ezdxf.entities.Point,
                                           ezdxf.entities.Insert,
                                           ezdxf.entities.Attrib,
                                           ezdxf.entities.Polyline,
                                           ezdxf.entities.Line,
                                           ezdxf.entities.LWPolyline,
                                           ezdxf.entities.Hatch,
                                           ezdxf.entities.Circle,
                                           ezdxf.entities.Arc,
                                           ezdxf.entities.Dimension,
                                           )):
                    entity_dxf_type = entity.dxftype()
                    self.entity_error(
                        check_name,
                        '''Les objets de type %s ne sont pas autorisés dans la charte''' % (
                            entity_dxf_type),
                        entity.dxf.layer,
                        entity)
                # warn of geometry conversions
                if isinstance(entity, ezdxf.entities.LWPolyline) and entity.has_arc:
                    entity_dxf_type = entity.dxftype()
                    self.entity_warn(
                        check_name,
                        '''L'objet de type %s contient des arcs. Bien vérifier sa conversion en géométrie SIG''' % (
                            entity_dxf_type),
                        entity.dxf.layer,
                        entity)
                elif isinstance(entity, (ezdxf.entities.Spline,
                                         ezdxf.entities.Ellipse
                                         )):
                    entity_dxf_type = entity.dxftype()
                    self.entity_warn(
                        check_name,
                        '''L'objet de type %s contient des arcs. Bien vérifier sa conversion en géométrie SIG''' % (
                            entity_dxf_type),
                        entity.dxf.layer,
                        entity)

    @ check_method('HACHURES',
                   """
                  Les hachures doivent se trouver seulement dans les calques
                  de remplissage (suffixe H).
                  """
                   )
    def check_11_hatch_fill(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                if isinstance(entity, ezdxf.entities.Hatch):
                    for layer_suffix in self.CONSTANTS_CONFIG.HATCH_LAYERS_SUFFIXES:
                        if not entity.dxf.layer.endswith(layer_suffix):
                            break
                    else:
                        self.entity_error(
                            check_name,
                            '''Le calque n'est pas autorisé à contenir des hachures.''',
                            entity.dxf.layer,
                            entity)

    @check_method('ATTRIBUTS_BLOCS',
                  """Vérification des attributs attendu sur les `INSERTS` et `BLOCS`.""")
    def check_12_blocks_attributes(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                layer_name = entity.dxf.layer

                # skip layers that are not expected to have attributes on INSERTs or BLOCKs
                if layer_name not in self.CONSTANTS_CONFIG.CALQUES_INSERT_BLOCKS_ATTRIBUTES:
                    continue

                # skip objects that are not INSERTs
                if not isinstance(entity, ezdxf.entities.Insert):
                    continue

                # insert attributes dict
                insert_attribs = {
                    a.dxf.tag: a.dxf.text for a in entity.attribs}

                # block attributes dict (some kind of template for the insert)
                block_attribs = {
                    a.dxf.tag: a.dxf.text for a in entity.block().attdefs()}

                # check if some INSERT attributes are missing or not set
                missing_insert_attribs = []
                for attribute in self.CONSTANTS_CONFIG.REQUIRED_INSERT_ATTRIBUTES:
                    if attribute not in insert_attribs or not insert_attribs[attribute]:
                        missing_insert_attribs.append(attribute)

                # check if some BLOCK attributes are missing or not set
                missing_block_attribs = []
                for attribute in self.CONSTANTS_CONFIG.REQUIRED_BLOCK_ATTRIBUTES:
                    if attribute not in block_attribs or not block_attribs[attribute]:
                        missing_block_attribs.append(attribute)

                # if some INSERT attributes are missing or not set
                if missing_insert_attribs:
                    self.entity_error(
                        check_name,
                        '''Les attributs suivants sont manquants : %s.''' % (
                            ','.join(missing_insert_attribs)),
                        layer_name,
                        entity)

                # if some BLOCK attributes are missing or not set
                if missing_block_attribs:
                    object_name = get_entity_verbose_name(entity)
                    self.entity_error(
                        check_name,
                        '''Les attributs suivants sont manquants pour le bloc associé à l'objet: %s. A-t-il été modifié ?''' % (
                            ','.join(missing_block_attribs)),
                        layer_name,
                        entity)

    @check_method('OBJETS_NON_AUTORISES',
                  """
                  Vérifie qu'à l’intérieur du dessin les différents éléments sont séparés
                  par familles (Réseaux, voirie, assainissement...), selon les règles établies
                  dans le cahier des charges.

                  Une organisation de calques se référant au graphisme exclusivement est
                  interdite (calque `BLOC` ou `POINT` par exemple).
                  """
                  )
    def check_13_allowed_objects_per_layer(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer') and not self.is_habillage(entity):
                layer_name = entity.dxf.layer

                # get entity type id
                entity_type_id = get_entity_type_id(entity)

                # if entity type id is special, it is allowed except for some geometry types
                if entity_type_id.lower() in self.CONSTANTS_CONFIG.SPECIAL_ENTITY_TYPE_IDs:

                    # check allowed geometry types
                    if not isinstance(entity, (
                            ezdxf.entities.Insert,
                            ezdxf.entities.Hatch,
                    )):
                        # if not allowed
                        self.entity_error(
                            check_name,
                            '''Le type %s n'est pas autorisé dans la charte pour les objets Ligne, Polyligne ou Cercle.''' % (
                                entity_type_id
                            ),
                            entity.dxf.layer,
                            entity)

                    # move on to next entity
                    continue

                # look for layer / entity type
                layer_found = False
                entity_type_id_layer = None
                for oc in self.CONSTANTS_CONFIG.CALQUES_OBJETS:

                    # check if object layer is found
                    if layer_found is False and oc['calque'] == layer_name:
                        layer_found = True

                    # check if object entity type is found
                    if entity_type_id_layer is None and oc['id'] == entity_type_id:
                        entity_type_id_layer = oc['calque']

                    # check if object layer and entity type id match
                    if oc['calque'] == layer_name and oc['id'] == entity_type_id:
                        # object was found with proper layer name and entity type id
                        # but we have to check its DXF object type
                        object_type = oc['type']
                        expected_dxf_types = OBJECT_TYPE_TO_ALLOWED_EZDXF_ENTITY_TYPES[object_type]
                        if not isinstance(entity, expected_dxf_types):
                            self.entity_error(
                                check_name,
                                '''Le type %s n'est pas compatible avec l'ID de la charte (attendu: %s).''' % (
                                    entity.dxftype(),
                                    object_type,
                                ),
                                entity.dxf.layer,
                                entity)
                        # object found so break
                        break
                else:
                    # object was not found with proper layer name and entity type id
                    if entity_type_id_layer is not None:
                        msg = '''L'objet devrait se trouver dans le calque %s''' % entity_type_id_layer

                    elif layer_found:
                        msg = '''L'objet a un type qui n'est autorisé dans la charte pour le calque %s ''' % layer_name
                    else:
                        msg = '''Ni l'objet, ni le calque n'appartiennent à la charte.'''

                    self.entity_error(
                        check_name,
                        msg,
                        layer_name,
                        entity)

    @check_method('COULEURS',
                  """
                  Vérifie que la table de couleurs utilisée est impérativement celle prescrite
                  dans le cahier des charges.

                  La couleur ACI 7 (table de couleur par défaut AutoCAD) est considérée comme
                  valant `0,0,0` ou `255,255,255`.
                  """
                  )
    def check_14_colors(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):

                aci, (r, g, b) = get_entity_aci_rgb_color(self.dxf_doc, entity)

                famille = self.get_famille(entity)
                if famille is None:
                    # object is very likely to be unauthorized, so another test will raise this issue
                    continue

                # get expected color
                fr, fg, fb = self.CONSTANTS_CONFIG.COULEURS[famille]

                if r == fr and g == fg and b == fb:
                    # if colors match, move on to next entity
                    continue

                if aci == 7:  # special AutoCAD color that means object is white on black background, and black on white background
                    # so we allow both colors
                    if (fr, fg, fb) in ((0, 0, 0), (255, 255, 255)):
                        # if colors match, move on to next entity
                        continue

                # an error was detected
                object_name = get_entity_verbose_name(entity)
                self.entity_error(
                    check_name,
                    '''L'objet a une couleur non autorisée pour sa famille''',
                    entity.dxf.layer,
                    entity)

    @check_method('PLACEMENT_TEXTE',
                  """
                  Vérifie que les objets qui ont le type d'objet *Texte libre* et *Texte information*
                  sont bien sur un autre objet ou à l'intérieur d'un objet de la même famille
                  que l'objet texte.
                  """
                  )
    def check_16_text_placement(self, check_name):

        for layer_name in self.layer_names:
            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue

            famille = self.get_famille(layer_name)

            # skip habillage famille
            if famille is self.CONSTANTS_CONFIG.HABILLAGE_FAMILLE:
                continue

            # check layers of same famille
            for other_layer_name in self.layer_names:

                # skip layers that are not expected in dataset
                if other_layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                    continue

                # if different famille, move on to next feature
                if not self.get_famille(other_layer_name) == famille:
                    continue

                sql_query = '''
                            SELECT
                                l1.geom,
                                l1.object_name,
                                l1.block_name,
                                l1.handle,
                                l2.object_name AS other_object_name,
                                ST_GeometryType(l2.geom) as other_geom_type
                                FROM "%(layer)s" l1
                                JOIN "%(other_layer)s" l2
                                ON
                                    lower(l1.libelle_type) IN (%(text_type_labels)s)

                                    AND

                                    lower(l2.libelle_type) NOT IN (%(text_type_labels)s)

                                    AND

                                    l2.ROWID IN (
                                        SELECT
                                            id
                                        FROM rtree_%(other_layer)s_geom r
                                        WHERE
                                            r.maxx >= ST_MinX(l1.geom) - %(max_distance)f
                                            AND
                                            r.minx <= ST_MaxX(l1.geom) + %(max_distance)f
                                            AND
                                            r.maxy >= ST_MinY(l1.geom) - %(max_distance)f
                                            AND
                                            r.miny <= ST_MaxY(l1.geom) + %(max_distance)f
                                    )
                                    AND

                                    (
                                        (
                                            ST_GeometryType(l1.geom) = 'POINT'
                                            AND
                                            ST_GeometryType(l2.geom) = 'POINT'
                                            AND
                                            ST_Distance(l1.geom, l2.geom) < %(max_distance)f
                                            AND
                                            ST_Distance(l1.geom, l2.geom) > %(epsilon)f
                                        )

                                        OR

                                        (
                                            ST_GeometryType(l1.geom) = 'POINT'
                                            AND
                                            ST_GeometryType(l2.geom) = 'POLYGON'
                                            AND
                                            ST_Distance(l1.geom, l2.geom) < %(max_distance)f
                                            AND
                                            NOT ST_Intersects(l1.geom, l2.geom)
                                        )

                                    )
                                    ''' % {
                            'layer': layer_name,
                            'other_layer': other_layer_name,
                            'text_type_labels': ','.join(["'%s'" % label for label in self.CONSTANTS_CONFIG.TEXT_TYPE_LABELS]),
                            'max_distance': self.CONSTANTS_CONFIG.TEXT_MAX_DISTANCE,
                            'epsilon': self.CONSTANTS_CONFIG.EPSILON
                }

                # run sql query
                layer = self.gpkg_ds.ExecuteSQL(sql_query)

                # if SQL query returned results
                if layer is not None:
                    feature = layer.GetNextFeature()
                    while feature is not None:
                        object_name = feature.GetField("object_name")
                        other_object_name = feature.GetField(
                            "other_object_name")
                        other_geom_type = feature.GetField("other_geom_type")

                        if other_geom_type == 'POLYGON':
                            self.feature_error(
                                check_name,
                                '''Les objets de type TEXTE doivent être situés à l'intérieur du polygone qu'ils référencent.''',
                                layer_name,
                                feature)
                        elif other_geom_type == 'POINT':
                            self.feature_error(
                                check_name,
                                '''Les objets de type TEXTE doivent être situés sur le point référencé (distance < %f et > à %f)''' % (
                                    self.CONSTANTS_CONFIG.TEXT_MAX_DISTANCE,
                                    self.CONSTANTS_CONFIG.EPSILON),
                                layer_name,
                                feature)

                        feature = layer.GetNextFeature()
                # release result set
                self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('TEXTES',
                  """Les caractères ne doivent pas être accentués, ni en minuscule.""")
    def check_18_uppercase_text(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                text = get_entity_text(entity)
                if text is None:
                    continue
                if ((text != text.upper())
                    or
                    (text != remove_accents(text))
                    ):
                    self.entity_warn(
                        check_name,
                        '''Les textes doivent être en MAJUSCULES, sans accent.''',
                        entity.dxf.layer,
                        entity)

    @check_method('ALTITUDE',
                  """
                  Certains objets attendent des attributs `ALT`, `GST`, `TN`, `TAMPON`, `RADIER`
                  qui doivent correspondre à une altitude.
                  Ces objets doivent aussi avoir un point 3D (valeur en Z).
                  """
                  )
    def check_19_leve_topo(self, check_name):

        for entity in self.dxf_doc.entities:
            if entity in self.dxf_modelspace and hasattr(entity.dxf, 'layer'):
                # object must be an Insert
                if not isinstance(entity, ezdxf.entities.Insert):
                    continue

                # it must have expected altitude attributes
                expected_altitude_attributes = self.get_alt_attributes(entity)
                if expected_altitude_attributes is None:
                    continue

                # check insert point is 3D
                insert_point = entity.dxf.insert
                if len(insert_point) != 3:  # must have a Z value
                    self.entity_error(
                        check_name,
                        '''L'objet doit avoir une composante Z''',
                        entity.dxf.layer,
                        entity)

                # iterate over expected altitude attributes for entity
                for expected_altitude_attribute in expected_altitude_attributes:

                    altitude = entity.get_attrib_text(
                        expected_altitude_attribute, None, search_const=True)

                    # if altitude attribute does not exist => error and move on to next expected attribute
                    if altitude is None:
                        self.entity_error(
                            check_name,
                            '''L'objet doit avoir un attribut %s indiquant son altitude''' % (
                                expected_altitude_attribute),
                            entity.dxf.layer,
                            entity)
                        continue

                    # if altitude attribute exists => check its value
                    else:
                        try:
                            altitude_value = float(altitude)
                        except:
                            object_name = get_entity_verbose_name(entity)
                            self.entity_error(
                                check_name,
                                '''L'objet a un attribut %s qui n'est pas une valeur numérique''' % (
                                    expected_altitude_attribute),
                                entity.dxf.layer,
                                entity)
                            continue
                        if not altitude_value > 0.0:
                            self.entity_warn(
                                check_name,
                                '''L'objet doit avoir un attribut %s avec une valeur numérique généralement positive non nulle. Pensez à contrôler la valeur''' % (
                                    expected_altitude_attribute),
                                entity.dxf.layer,
                                entity)

    @check_method('ALT_MAT',
                  """
                  Les points `YP_1521` (Point topo) ont un point homologue dans les calques
                  `ALT` et `MAT` correspondant, avec un Z et Matricule identique au point topo.
                  """
                  )
    def check_21_leve_topo_alt_mat(self, check_name):

        for layer_name in self.layer_names:
            layers_to_check = []

            # Check if an ALT layer must be checked
            alt_layer_info = self.CONSTANTS_CONFIG.CALQUES_CALQUES_ALT.get(layer_name,
                                                                           None)
            if alt_layer_info is not None:
                entity_type_id, alt_layer_name = alt_layer_info

                # check if ALT layer exist in dataset
                if alt_layer_name in self.layer_names:
                    layers_to_check.append(
                        (entity_type_id, alt_layer_name, 'ALT'))
                else:
                    self.error(
                        check_name,
                        '''Le calque %s est absent. Il doit contenir les altitudes du calque %s.''' % (
                            alt_layer_name, layer_name),
                        layer_name=alt_layer_name,
                    )

            # Check if a MAT layer must be checked
            mat_layer_info = self.CONSTANTS_CONFIG.CALQUES_CALQUES_MAT.get(layer_name,
                                                                           None)
            if mat_layer_info is not None:
                entity_type_id, mat_layer_name = mat_layer_info

                # check if MAT layer exist in dataset
                if mat_layer_name in self.layer_names:
                    layers_to_check.append(
                        (entity_type_id, mat_layer_name, 'MAT'))
                else:
                    self.error(
                        check_name,
                        '''Le calque %s est absent. Il doit contenir les matricules du calque %s.''' % (
                            mat_layer_name, layer_name),
                        layer_name=mat_layer_name,
                    )

            for entity_type_id, other_layer_name, expected_attribute_name in layers_to_check:
                sql_query = '''
                            SELECT
                                l1.geom,
                                l1.object_name,
                                l1.block_name,
                                l1.handle,
                                l2.geom as other_geom,
                                l2.object_name AS other_object_name,
                                json_extract(l2.attribs, '$.%(expected_attribute_name)s') as "%(expected_attribute_name)s"
                                FROM "%(layer)s" l1
                                LEFT JOIN "%(other_layer)s" l2
                                ON

                                    l2.ROWID IN (
                                        SELECT
                                            id
                                        FROM rtree_%(other_layer)s_geom r
                                        WHERE
                                            r.maxx >= ST_MinX(l1.geom) - %(max_distance)f
                                            AND
                                            r.minx <= ST_MaxX(l1.geom) + %(max_distance)f
                                            AND
                                            r.maxy >= ST_MinY(l1.geom) - %(max_distance)f
                                            AND
                                            r.miny <= ST_MaxY(l1.geom) + %(max_distance)f
                                    )

                                    AND

                                    ST_Distance(l1.geom, l2.geom) < %(max_distance)f
                                
                                WHERE l1.entity_type_id = '%(entity_type_id)s'
                            ''' % {
                            'layer': layer_name,
                            'entity_type_id': entity_type_id,
                            'other_layer': other_layer_name,
                            'expected_attribute_name': expected_attribute_name,
                            'max_distance': self.CONSTANTS_CONFIG.TEXT_MAX_DISTANCE
                }

                # run sql query
                layer = self.gpkg_ds.ExecuteSQL(sql_query)

                # if SQL query returned results
                if layer is not None:
                    processed = []
                    feature = layer.GetNextFeature()
                    while feature is not None:
                        geom = feature.GetGeometryRef()
                        object_name = feature.GetField("object_name")

                        # lazy way to handle distinct results
                        if object_name in processed:
                            feature = layer.GetNextFeature()
                            continue
                        processed.append(object_name)

                        handle = feature.GetField("handle")
                        other_object_name = feature.GetField(
                            "other_object_name")
                        expected_attribute_value = feature.GetField(
                            expected_attribute_name)
                        if other_object_name is None:
                            self.error(
                                check_name,
                                '''L'objet doit avoir un point homologue dans le calque %s (à une distance < %f)''' % (
                                    other_layer_name,
                                    self.CONSTANTS_CONFIG.TEXT_MAX_DISTANCE,
                                ),
                                layer_name=layer_name,
                                object_name=object_name,
                                handle=handle,
                                geometry=geom.Clone()
                            )
                        else:
                            # check attribute value
                            if expected_attribute_value is None:
                                self.error(
                                    check_name,
                                    '''L'objet doit avoir une valeur pour l'attribut %s''' % (
                                        expected_attribute_name
                                    ),
                                    layer_name=other_layer_name,
                                    object_name=other_object_name,
                                    handle=handle,
                                    geometry=geom.Clone())
                            # check altitude value when expected attribute is ALT
                            elif expected_attribute_name == 'ALT':
                                try:
                                    altitude_value = float(
                                        expected_attribute_value)
                                except:
                                    self.error(
                                        check_name,
                                        '''L'objet a un attribut %s qui n'est pas une valeur numérique''' % (
                                            expected_attribute_name),
                                        layer_name=other_layer_name,
                                        object_name=other_object_name,
                                        handle=handle,
                                        geometry=geom.Clone()
                                    )
                                    continue
                                if not altitude_value > 0.0:
                                    self.warning(
                                        check_name,
                                        '''L'objet doit avoir un attribut %s avec une valeur numérique positive non nulle''' % (
                                            expected_attribute_name),
                                        layer_name=other_layer_name,
                                        object_name=other_object_name,
                                        handle=handle,
                                        geometry=geom.Clone()
                                    )

                        feature = layer.GetNextFeature()
                # release result set
                self.gpkg_ds.ReleaseResultSet(layer)

    @check_method('DOUBLON',
                  """
                  Le contrôle cherche des objets de même type dans chaque couche, dont les géométries sont
                  - soit identiques en 3D,
                  - soit, dans le cas de points, très proches (à 0.01 m près)

                  Deux objets sont considérés comme de même type s'ils ont le même type DXF et,
                  pour certains types DXF (INSERT, HATCH), s'ils ont le même type de BLOC (INSERT)
                  ou PATTERN NAME (HATCH).

                  Des warnings sont créés pour chaque doublon potentiel détecté.

                  """)
    def check_29_doublons(self, check_name):

        # query features that are out of bounds in each layer
        for layer_name in self.layer_names:
            # skip layers that are not expected in dataset
            if layer_name not in self.CONSTANTS_CONFIG.CALQUES:
                continue
            # skip layers that are meant to contain texts
            if layer_name in self.CONSTANTS_CONFIG.CALQUES_TEXTE:
                continue
            sql_query = '''
                            SELECT
                                l1.geom,
                                l1.object_name,
                                l1.block_name,
                                l1.handle,
                                l2.object_name AS other_object_name
                                FROM "%(layer)s" l1
                                JOIN "%(layer)s" l2
                                ON
                                    l2.ROWID IN (
                                        SELECT
                                            id
                                        FROM rtree_%(layer)s_geom r
                                        WHERE
                                            r.maxx >= ST_MinX(l1.geom) - %(max_distance)f
                                            AND
                                            r.minx <= ST_MaxX(l1.geom) + %(max_distance)f
                                            AND
                                            r.maxy >= ST_MinY(l1.geom) - %(max_distance)f
                                            AND
                                            r.miny <= ST_MaxY(l1.geom) + %(max_distance)f
                                    )

                                    AND

                                    l1.object_name != l2.object_name

                                    AND

                                    l1.entity_type_id = l2.entity_type_id

                                    AND

                                    (
                                        ST_Equals(l1.geom, l2.geom)

                                        OR

                                        (
                                            ST_GeometryType(l1.geom) = 'POINT'
                                            AND
                                            ST_GeometryType(l2.geom) = 'POINT'
                                            AND
                                            ST_Distance(l1.geom, l2.geom) < %(max_distance)f
                                        )
                                    )
                        ''' % {
                'layer': layer_name,
                'max_distance': self.CONSTANTS_CONFIG.DUPLICATE_MAX_DISTANCE
            }
            # run sql query
            layer = self.gpkg_ds.ExecuteSQL(sql_query)
            # set up structures to keep track of unique duplicates and their geometries
            unique_duplicates = set()
            geoms = {}
            handles = {}
            # if SQL query returned results
            if layer is not None:
                # populate unique duplicates and keep track of their geometries
                feature = layer.GetNextFeature()
                while feature is not None:
                    object_name = feature.GetField("object_name")
                    other_object_name = feature.GetField("other_object_name")
                    if ((object_name, other_object_name) not in unique_duplicates
                            and
                            (other_object_name, object_name) not in unique_duplicates
                            ):
                        unique_duplicates.add((object_name, other_object_name))
                        geom = feature.GetGeometryRef()
                        geoms[object_name] = get_representative_point_from_geometry(
                            geom)
                        handles[object_name] = feature.GetField('handle')
                    feature = layer.GetNextFeature()
            # release result set
            self.gpkg_ds.ReleaseResultSet(layer)
            # warn of each unique duplicate
            for object_name, other_object_name in unique_duplicates:
                geom = geoms[object_name]
                handle = handles[object_name]
                self.warn(check_name,
                          '''L'objet %s est un doublon de l'objet %s (distance <= %f)''' % (
                              object_name,
                              other_object_name,
                              self.CONSTANTS_CONFIG.DUPLICATE_MAX_DISTANCE),
                          layer_name=layer_name,
                          object_name=object_name,
                          geometry=geom.Clone(),
                          handle=handle,
                          selection=selection_attr(handle))

    @check_method('EXPORT_GML',
                  """
                  Exporte les données au format PCRS GML 2.0.

                  Nécessite l'option gml_export_options avec la forme suivante :
                  ```
                  {
                    
                    # Nom du producteur
                    'producteur' : 'Nom du producteur',
                    
                    # Gestionnaire par défaut
                    'default_gestionnaire' : 'Gestionnaire par défaut',

                    # Gestionnaire par nature de réseau
                    'gestionnaire_map' : {
                    # Nature reseau : Libellé gestionnaire
                    'ELEC': 'Gestionnaire ELEC',
                    'INCE': 'Gestionnaire Incendie'
                    },
                    
                    # Qualité par défaut 
                    'default_qualite' : '01',

                    # Qualité par thématique (si besoin)
                    'qualite_map' : {
                        # Code Thematique : Code Qualite
                        '02': '02'
                    },

                    # Précision 2D par défaut
                    'default_precisionxy' : 5,

                    # Précision par thématique
                    'precisionxy_map' : {
                        # Code Thematique : Precision XY
                        '02': 5
                    },
                    
                    # Précision en Z par défaut
                    'default_precisionz' : 5,
                    
                    # Précision en Z par thématique (si besoin)
                    'precisionz_map' : {
                        # Code Thematique : Precision Z
                        '02': 10
                    },
                    
                    # Préfixe des identifiants d'objets (chaine vide sinon)
                    'prefixe_id' : '',
                    
                    # Date de levé au format YYYY-MM-DD (chaine vide sinon)
                    'date_leve' : 'YYYY-MM-DD',
                    
                    # Horodatage au format YYYY-MM-DD (chaine vide sinon)
                    'horodatage' : 'YYYY-MM-DD'
                  }
                  ```

                  """)
    def check_gml_export(self, check_name):

        # remove existing GML file if it exists
        remove_file_if_exists(self.gml_filename)

        # check if previous errors were found
        if self.error_found:
            self.warning(check_name,
                         '''Des erreurs ont été trouvées. L'export GML se sera pas réalisé.''')
            return

        # check if GML options were set
        gml_options = self.gml_export_options
        if not gml_options:
            self.warning(check_name,
                         '''Aucun paramètre d'export GML spécifié. L'export se sera pas réalisé.''')
            return

        # check gml parameters
        required_parameters = [
            'producteur',
            'default_gestionnaire',
            'gestionnaire_map',
            'default_qualite',
            'qualite_map',
            'default_precisionxy',
            'precisionxy_map',
            'default_precisionz',
            'precisionz_map',
            'prefixe_id',
            'date_leve',
            'horodatage'
        ]
        parameters_ok = True
        for param in required_parameters:
            if param not in gml_options:
                self.error(check_name,
                           '''Le paramètre d'export GML "%s" n'a pas été spécifié. L'export se sera pas réalisé.''' % param)
                parameters_ok = False
        for param in gml_options:
            if param not in required_parameters:
                self.error(check_name,
                           '''Un paramètre d'export GML inconnu a été spécifié ("%s"). L'export se sera pas réalisé.''' % param)
                parameters_ok = False
        if not parameters_ok:
            return

        try:
            convert_dxf_to_gml(self.gpkg_filename,
                               self.srs,
                               self.CONSTANTS_CONFIG.calques_objets_csv_path,
                               self.gml_xsd_path,
                               self.gml_filename,
                               logger=self,
                               check_name=check_name,
                               **gml_options)
        except Exception as e:
            self.error(check_name,
                       'Erreur lors de la conversion GML : %s' % str(e))
            remove_file_if_exists(self.gml_filename)
