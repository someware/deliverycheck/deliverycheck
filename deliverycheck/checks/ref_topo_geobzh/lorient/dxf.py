from fileinput import filename
from deliverycheck.checks.ref_topo_geobzh.commun.dxf import SocleCommunDXFCheck
from .constants import lorient_agglomeration_config
from deliverycheck.checks import check_method
import os.path
import re
import zipfile
import fnmatch

FILENAME_REGEX = r'^(?P<commune>([\w ])[^_]+)_(?P<affaire>([\w ])[^_]+)[ _]+(?P<entreprise>([\w ])[^_]+)[ _]+(?P<version>([\w ])[^_]+)[ _]*\((?P<date>[0-9]{8})\)[ _]*\.dxf$'
COMPILED_FILENAME_REGEX = re.compile(FILENAME_REGEX, re.IGNORECASE)


class LorientAgglomerationDXFCheck(SocleCommunDXFCheck):
    user_doc = '''Standard topographique régional / Profil Lorient Agglomération / Format DXF'''

    CONSTANTS_CONFIG = lorient_agglomeration_config

    def __init__(self, path, report_dir, srs, bounds_file_path=None, bounds_srs=None,
                 control_points_path=None, control_points_ogr_driver_name='GPKG', control_points_srs=None, control_points_accuracy_config_path=None,
                 dtm_path=None, dtm_srs=None, dtm_accuracy_config_path=None, gml_export_options={},
                 archive_path=None) -> None:
        super().__init__(path, report_dir, srs,
                         bounds_file_path, bounds_srs,
                         control_points_path, control_points_ogr_driver_name, control_points_srs, control_points_accuracy_config_path,
                         dtm_path, dtm_srs, dtm_accuracy_config_path, gml_export_options)
        # set archive path
        self.archive_path = archive_path

    def _generate_initial_checks(self):
        yield from super()._generate_initial_checks()
        yield self.check_26_unused_blocks_and_plans()
        if self.archive_path is not None:
            yield self.check_25_archive_format()

    @check_method('NOMENCLATURE_NOM_DE_FICHIER',
                  """
                  Les noms de fichiers informatiques devront suivre la dénomination suivante :
                  `Commune_Affaire_Nom Entreprise_Version de mise à jour du fichier`
                  (date au format AAAAMMJJ).
                  """
                  )
    def check_26_unused_blocks_and_plans(self, check_name):

        dxf_filename = os.path.basename(self.path)
        regex_match = COMPILED_FILENAME_REGEX.match(dxf_filename)
        if regex_match is not None:
            commune = regex_match.group('commune')
            affaire = regex_match.group('affaire')
            entreprise = regex_match.group('entreprise')
            version = regex_match.group('version')
            date = regex_match.group('date')
            self.info(check_name, 'Le nom de fichier semble correctement formatté. Commune: %s, Affaire: %s, Entreprise: %s, Version: %s, Date: %s' % (
                commune,
                affaire,
                entreprise,
                version,
                date))
        else:
            self.warn(check_name,
                       '''Le nom du fichier '%s' ne respecte pas la nomenclature du cahier des charges (mauvaise gestion possible des séparateurs _)''' % dxf_filename)

    @check_method('FORMAT_ARCHIVE_ZIP',
                  """
                  Fourniture pour tous les types de prestations des pièces énoncées ci-après avec
                  une détermination arrondie au mm :

                  - Le schéma des polygonales
                  - Le carnet de terrain, soit notamment :
                      - L’acquisition sur le terrain comprenant les données sources, le fichier
                        brut natif (notamment formats gsi, landxml, raw, job, ou jobxml…)
                        accompagné du fichier épuré (mêmes formats ou rtf, doc, docx, geo…)
                      - Les fichiers d’observations satellitaires au format RINEX pour les
                        observations statiques.
                      - Les rapports complets d’observation pour le temps réel et notamment
                        le fichier export des données et métadonnées au format html ou équivalent
                        comprenant entre autres les précisions horizontales et verticales, durées
                        et dates d’observation, nombre de satellites…
                      - Le carnet des observations, les calculs des Vo et des cheminements polygonaux
                      - Le carnet des observations et les calculs des cheminements de nivellement direct
                  - Les fichiers de calculs (calage, rattachement compris) (formats rtf, doc, docx)
                  - Les listings des résultats (stations et point levés) au format ASCII
                    (N°, X, Y, Z), les coordonnées XYZ étant arrondies au mm.
                  - Le nuage de points traité et géoréférencé notamment au format .las pour les
                    observations scanner
                  - Les copies des croquis de terrain avec les photographies
                  - Le rapport d’exécution complet de la prestation intégrant notamment
                    la méthodologie spécifique détaillée permettant de connaître la précision
                    du levé (appareils utilisés avec leurs précisions, méthode de travail,
                    les cheminements exécutés et les rattachements planimétrique et altimétrique
                    du levé…)
                  - Les fiches stations au format pdf sans restriction ni protection (3 cotes
                    minimum, coordonnées arrondies au mm et dénomination correspondant exactement
                    aux calculs et plans)
                  - Les documents graphiques réalisés au 1/200e, accompagnés d’un cartouche,
                    au format pdf

                  Le support doit être marqué avec les indications suivantes :

                  - La commune concernée
                  - Le nom de dossier de travail
                  - L’identité de l’émetteur
                  - La date de diffusion
                  """
                  )
    def check_25_archive_format(self, check_name):

        if not zipfile.is_zipfile(self.archive_path):
            self.error(check_name,
                       '''Le fichier '%s' n'est pas un fichier ZIP conforme''' % self.archive_path)
            return

        # get all ZIP file inner path as lower-case strings
        zip = zipfile.ZipFile(self.archive_path)
        zippaths = [p.lower() for p in zip.namelist()]

        # check if paths have a common prefix
        # which would probably mean there is an upper directory
        zippaths_prefix = os.path.commonprefix(zippaths)
        # TODO: check if zipfile also returns Unix-paths on Windows
        if zippaths_prefix.endswith('/'):
            zip_top_dir = zippaths_prefix
        else:
            zip_top_dir = None

        # preprocess zip paths to remove potential readme files
        # that were provided by Lorient Agglomeration
        for expected_directory in self.CONSTANTS_CONFIG.EXPECTED_ARCHIVE_DIRECTORIES:
            readme_filename = '%s/%s.txt' % (expected_directory,
                                             expected_directory)
            if readme_filename in zippaths:
                zippaths.remove(readme_filename)

        # for each expected directory
        # check if it has at least one file matching expected file extensions
        for expected_directory, directory_information in self.CONSTANTS_CONFIG.EXPECTED_ARCHIVE_DIRECTORIES.items():

            # get expected extensions
            expected_file_extensions = directory_information['extensions']

            # check if at leat one file matches an extension
            for file_ext in expected_file_extensions:
                # if zip has a top directory
                if zip_top_dir:
                    # TODO: check if zipfile also returns Unix-paths on Windows
                    file_pattern = '%s%s/*.%s' % (zip_top_dir,
                                                  expected_directory,
                                                  file_ext.lower())
                else:
                    # TODO: check if zipfile also returns Unix-paths on Windows
                    file_pattern = '%s/*.%s' % (expected_directory,
                                                file_ext.lower())
                if len(fnmatch.filter(zippaths, file_pattern)) > 0:
                    break

            # if no file was found matching expected extensions
            else:
                extensions_string = ', '.join(expected_file_extensions)
                required = directory_information['required']
                # if at least on file matching extensions is required, log an error
                if required:
                    self.error(check_name,
                               '''Le fichier ZIP doit impérativement contenir un dossier '%s' avec au moins un fichier de type %s''' % (expected_directory, extensions_string))
                # if not, just log a warning
                else:
                    self.warn(check_name,
                              '''Le fichier ZIP doit si possible contenir un dossier '%s' avec au moins un fichier de type %s''' % (expected_directory, extensions_string))
