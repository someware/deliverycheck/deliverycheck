import os.path
import deliverycheck.checks.ref_topo_geobzh.commun as socle_commun
from deliverycheck.checks.ref_topo_geobzh.commun.constants_load import ReferentielGeobretagneDXFConfig

socle_commun_dir = os.path.dirname(os.path.abspath(socle_commun.__file__))
current_dir = os.path.dirname(os.path.abspath(__file__))


class LorientAgglomerationDXFConfig(ReferentielGeobretagneDXFConfig):

    def __init__(self, calques_objets_csv_path, couleurs_famille_csv_path):
        super().__init__(calques_objets_csv_path,
                         couleurs_famille_csv_path=couleurs_famille_csv_path)

        # suffixes des layers autorisés à contenir des hachures
        # (surcharge l'object défini pour la config du socle commun)
        self.HATCH_LAYERS_SUFFIXES = (
            'H_SCS',  # Socle Commun Surface
            'H_SCT',  # Socle Commun souTerrain
            'H_SCI',  # Socle Commun Info
            'H_PSS',  # Pas Socle Surface
            'H_PST',  # Pas Socle souTerrain
            'H_PSI',  # Pas Socle Info
        )

        self.EXPECTED_ARCHIVE_DIRECTORIES = {
            '010_note_methodologique': {
                'required': True,
                'extensions': [
                    'PDF',
                    'DOC',
                    'DOCX',
                    'RTF',
                    'ODT',
                    'TXT',
                    'XLS',
                    'XLSX',
                    'ODS',
                ]
            },
            '020_schema_polygonales': {
                'required': False,
                'extensions': [
                    'PDF',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'JPG',
                    'JPEG',
                    'PNG'
                ]
            },
            '030_gnss/031_fichier_brut_natif': {
                'required': False,
                'extensions': [
                    'GSI',
                    'LANDXML',
                    'RAW',
                    'JOB',
                    'JOBXML',
                    'HTM',
                    'TXT',
                    'CSV',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'MDB'
                ]
            },
            '030_gnss/032_fichier_épuré': {
                'required': False,
                'extensions': [
                    'GSI',
                    'LANDXML',
                    'RAW',
                    'JOB',
                    'JOBXML',
                    'HTM',
                    'TXT',
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'CSV',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'MDB',
                    'GEO'
                ]
            },
            '030_gnss/033_observations_satellitaires': {
                'required': False,
                'extensions': [
                    '18N',
                    '18G',
                    '18O'
                ]
            },
            '030_gnss/034_rapports_observation':  {
                'required': False,
                'extensions': [
                    'HTM',
                    'CSV',
                    'GSI',
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'CSV',
                    'XLS',
                    'XLSX',
                    'ODS'
                ]
            },
            '040_tacheometre/041_fichier_brut_natif': {
                'required': False,
                'extensions': [
                    'GSI',
                    'LANDXML',
                    'RAW',
                    'JOB',
                    'JOBXML',
                    'HTM',
                    'TXT',
                    'CSV',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'MDB'
                ]
            },
            '040_tacheometre/042_fichier_épuré': {
                'required': False,
                'extensions': [
                    'GSI',
                    'LANXML',
                    'RAW',
                    'JOB',
                    'JOBXML',
                    'HTM',
                    'TXT',
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'CSV',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'GEO',
                    'MDB'
                ]
            },
            '050_nivellement/051_fichier_brut_natif': {
                'required': False,
                'extensions': [
                    'GSI',
                    'DAT',
                    'PDF',
                    'XLS',
                    'XLSX',
                    'ODS'
                ]
            },
            '050_nivellement/052_fichier_épuré': {
                'required': False,
                'extensions': [
                    'GSI',
                    'DAT',
                    'PDF',
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'GEO'
                ]
            },
            '050_nivellement/053_calculs_cheminements': {
                'required': False,
                'extensions': [
                    'GSI',
                    'DAT',
                    'PDF',
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'XLS',
                    'XLSX',
                    'ODS',
                    'GEO'
                ]
            },
            '060_scanner/061_nuage_de_points': {
                'required': False,
                'extensions': [
                    'LAS',
                    'CSV',
                    'TXT'
                ]
            },
            '070_calculs_calage_rattachement': {
                'required': False,
                'extensions': [
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT'
                ]
            },
            '080_calculs_des_v0': {
                'required': False,
                'extensions': [
                    'RTF',
                    'DOC',
                    'DOCX',
                    'ODT',
                    'GEO'
                ]
            },
            '090_fiches_stations': {
                'required': False,
                'extensions': [
                    'PDF',
                ]
            },
            '100_listing_des_resultats': {
                'required': True,
                'extensions': [
                    'TXT'
                ]
            },
            '110_document_graphique': {
                'required': True,
                'extensions': [
                    'PDF'
                ]
            },
            '120_croquis_terrain': {
                'required': False,
                'extensions': [
                    'PDF',
                    'JPG',
                    'PNG',
                    'DOC',
                    'DOCX',
                    'ODT'
                ]
            },
            '130_plans/131_dwg_2d_et_demi': {
                'required': True,
                'extensions': [
                    'DWG'
                ]
            },
            '130_plans/132_dwg_3d': {
                'required': True,
                'extensions': [
                    'DWG'
                ]
            },
        }


lorient_agglomeration_config = LorientAgglomerationDXFConfig(os.path.join(current_dir, 'definitions/OBJETS_DXF_LO.csv'),
                                                             os.path.join(socle_commun_dir, 'definitions/COULEURS_DXF_COMMUN.csv'))
