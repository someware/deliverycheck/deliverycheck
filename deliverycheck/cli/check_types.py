from deliverycheck.checks.ref_topo_geobzh.commun import SocleCommunDXFCheck
from deliverycheck.checks.ref_topo_geobzh.sdef import SDEFShapefileCheck
from deliverycheck.checks.ref_topo_geobzh.lorient import LorientAgglomerationDXFCheck
from deliverycheck.checks.ref_topo_geobzh.rennes import RennesMetropoleDXFCheck

AVAILABLE_CHECK_TYPES = {
    'referentiel_topographie_geobretagne.socle_commun.dxf': SocleCommunDXFCheck,
    'referentiel_topographie_geobretagne.sdef.shapefile': SDEFShapefileCheck,
    'referentiel_topographie_geobretagne.lorient_agglomeration.dxf': LorientAgglomerationDXFCheck,
    'referentiel_topographie_geobretagne.rennes_metropole.dxf': RennesMetropoleDXFCheck
}
