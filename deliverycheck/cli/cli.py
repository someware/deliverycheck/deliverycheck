import logging
import sys
import os
import os.path
import json
import click

from .check_types import AVAILABLE_CHECK_TYPES


@click.group()
def cli():
    pass


@cli.command()
@click.option('--check-type', '-t', help='Dataset type', required=True,
              type=click.Choice(AVAILABLE_CHECK_TYPES.keys(), case_sensitive=False))
def doc(check_type):
    """Return documentation for selected check type

    Args:
        check_type (str): type of check to run
    """
    CheckClass = AVAILABLE_CHECK_TYPES[check_type]
    print('Checks for selected check type:')
    print(CheckClass.generate_user_doc())


@cli.command()
@click.option('--check-type', '-t', help='Dataset type', required=True,
              type=click.Choice(AVAILABLE_CHECK_TYPES.keys(), case_sensitive=False))
@click.option('--path', '-p', help='Path of dataset to check', required=True,
              type=click.Path(exists=True, file_okay=True, dir_okay=True, readable=True, resolve_path=True, allow_dash=False))
@click.option('--report-dir', help='Output report path', required=True,
              type=click.Path(exists=True, file_okay=False, dir_okay=True, readable=True, writable=True, resolve_path=True, allow_dash=False))
@click.option('--check-option', '-o', help='Other option', required=False, default=None, type=click.STRING, nargs=2, multiple=True)
def check_dataset(check_type, path, report_dir, check_option):
    """Check an delivery dataset and write check reports to report_dir

    Args:
        check_type (str): type of check to run
        path (str): dataset path
        report_dir (str): output report directory
    """

    # set up logging into work dir
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s:%(message)s',
                        handlers=[
                            logging.StreamHandler(sys.stdout)
                        ])

    # set up log handler for file
    # TODO: log report writing should be handled by the CheckClass
    log_file_handler = logging.FileHandler(
        filename=os.path.join(report_dir, 'report.log'))
    logger = logging.getLogger()
    logger.addHandler(log_file_handler)

    # prepare options
    options = {}
    for option_name, option_value in check_option:
        # print(option_name, option_value)

        # try to load option as a json object
        try:
            option_obj = json.loads(option_value)
        except ValueError:
            option_obj = None

        # if option was loaded as json dict/list
        if isinstance(option_obj, (list, dict)):
            options[option_name] = option_obj
        # if it is not a json dict/list
        else:
            options[option_name] = option_value

    # initialize check class and run tests
    CheckClass = AVAILABLE_CHECK_TYPES[check_type]
    checker = CheckClass(path,
                         report_dir,
                         **options)
    checker.run_checks()
    checker.write_check_reports()

    # remove log handler for file
    logger.removeHandler(log_file_handler)

    logging.info('Delivery check done.')


if __name__ == '__main__':
    cli()
