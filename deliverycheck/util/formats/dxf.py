import ezdxf
import ezdxf.bbox
from ezdxf.colors import aci2rgb
from ezdxf import path
from ezdxf.math import Vec3
from osgeo import ogr


def get_ogr_geometry(entity, epsilon):
    """Convert DXF entity as an OGR geometry

    Args:
        entity (sublass of ezdxf.entities.DXFEntity): DXF entity

    Raises:
        NotImplementedError: raised if an input DXF entity type is not already supported

    Returns:
        ogr.Geometry: OGR geometry
    """
    ocs = entity.ocs()

    # Point
    if isinstance(entity, ezdxf.entities.Point):
        p = ogr.Geometry(ogr.wkbPoint)
        ocs_point = entity.dxf.location
        wcs_point = ocs.to_wcs(ocs_point)
        p.AddPoint(*wcs_point)
        return p

    # Insert
    elif isinstance(entity, ezdxf.entities.Insert):
        p = ogr.Geometry(ogr.wkbPoint)
        ocs_point = entity.dxf.insert
        wcs_point = ocs.to_wcs(ocs_point)
        p.AddPoint(*wcs_point)
        return p

    # Text, MText, Dimension
    elif isinstance(entity, (ezdxf.entities.Text,
                             ezdxf.entities.MText,
                             ezdxf.entities.Dimension)):
        p = ogr.Geometry(ogr.wkbPoint)
        ocs_point = entity.dxf.insert
        wcs_point = ocs.to_wcs(ocs_point)
        p.AddPoint(*wcs_point)
        return p

    # Polyline / LWPolyline
    elif isinstance(entity, (ezdxf.entities.Polyline,
                             ezdxf.entities.LWPolyline)):
        # flatten polyline
        p = path.make_path(entity)
        vertices = p.flattening(epsilon)
        # convert
        if isinstance(entity, ezdxf.entities.LWPolyline) and entity.closed:
            p = ogr.Geometry(ogr.wkbPolygon)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            for x, y, z in vertices:
                wx, wy, wz = ocs.to_wcs(Vec3(x, y, z))
                ring.AddPoint(wx, wy, wz)
            # add ring to polygon
            p.AddGeometry(ring)
            return p
        else:
            l = ogr.Geometry(ogr.wkbLineString)
            for x, y, z in vertices:
                wx, wy, wz = ocs.to_wcs(Vec3(x, y, z))
                l.AddPoint(wx, wy, wz)
            return l

    # Spline / Ellipse
    elif isinstance(entity, (ezdxf.entities.Spline, ezdxf.entities.Ellipse)):
        if isinstance(entity, ezdxf.entities.Ellipse):
            spline = entity.to_spline(False)
        else:
            spline = entity
        # flatten spline
        vertices = spline.flattening(epsilon)
        # convert
        if spline.closed:
            p = ogr.Geometry(ogr.wkbPolygon)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            for x, y, z in vertices:
                wx, wy, wz = ocs.to_wcs(Vec3(x, y, z))
                ring.AddPoint(wx, wy, wz)
            # add first point to the end to close linearring
            ring.AddPoint(*ring.GetPoint(0))
            # add ring to polygon
            p.AddGeometry(ring)
            return p
        else:
            l = ogr.Geometry(ogr.wkbLineString)
            for x, y, z in vertices:
                wx, wy, wz = ocs.to_wcs(Vec3(x, y, z))
                l.AddPoint(wx, wy, wz)
            return l

    # Line
    elif isinstance(entity, ezdxf.entities.Line):
        l = ogr.Geometry(ogr.wkbLineString)
        l.AddPoint(*entity.dxf.start)
        l.AddPoint(*entity.dxf.end)
        return l

    # Circle
    elif isinstance(entity, ezdxf.entities.Circle):
        # No flattening
        p = ogr.Geometry(ogr.wkbPoint)
        p.AddPoint(*entity.dxf.center)
        return p

    # MPolygon, Hatch
    elif isinstance(entity, (ezdxf.entities.MPolygon,
                             ezdxf.entities.Hatch)):
        g = ogr.Geometry(ogr.wkbGeometryCollection)
        for p in entity.paths.paths:
            if isinstance(p, ezdxf.entities.PolylinePath):
                if p.is_closed:
                    poly = ogr.Geometry(ogr.wkbPolygon)
                    ring = ogr.Geometry(ogr.wkbLinearRing)
                    for v in p.vertices:
                        if isinstance(v, tuple):
                            ring.AddPoint(*v)
                        else:
                            ring.AddPoint(*v.dxf.location)
                    # add first point to the end to close linearring
                    ring.AddPoint(*ring.GetPoint(0))
                    # add ring to polygon
                    poly.AddGeometry(ring)
                    # add polygon to geometry collection
                    g.AddGeometry(poly)
                    # if we find a PolylinePath, it is very likely to be the boundary ring
                    # and it can be a mess integrating geometries extracted from EdgePath afterwards
                    # so we just exit loop on entity paths
                    break
                else:
                    l = ogr.Geometry(ogr.wkbLineString)
                    for v in p.vertices:
                        if isinstance(v, tuple):
                            l.AddPoint(*v)
                        else:
                            l.AddPoint(*v.dxf.location)
                    g.AddGeometry(l)
            elif isinstance(p, ezdxf.entities.EdgePath):
                edges = ogr.Geometry(ogr.wkbGeometryCollection)
                for edge in p.edges:
                    if isinstance(edge, ezdxf.entities.LineEdge):
                        l = ogr.Geometry(ogr.wkbLineString)
                        l.AddPoint(*edge.start)
                        l.AddPoint(*edge.end)
                        edges.AddGeometry(l)
                    elif isinstance(edge, ezdxf.entities.ArcEdge):
                        # raise NotImplementedError('Type DXF non supporté : ArcEdge')
                        # print(get_entity_verbose_name(entity))
                        pass
                    elif isinstance(edge, ezdxf.entities.SplineEdge):
                        # raise NotImplementedError('Type DXF non supporté : SplineEdge')
                        # print(get_entity_verbose_name(entity))
                        pass
                # try to merge edges as a polygon
                poly = ogr.BuildPolygonFromEdges(edges, 0, 1, epsilon)
                if poly is not None:
                    g.AddGeometry(poly)
                else:
                    multilinestring = ogr.ForceToMultiLineString(edges)
                    g.AddGeometry(multilinestring)
        # if only one geometry finally
        if g.GetGeometryCount() == 1:
            # return this geometry
            return g.GetGeometryRef(0).Clone()
        # if not, return multigeometry
        else:
            return g
    else:
        # TODO: see how we can handle all ezdxf entity types without crashing
        # return None if not supported ? just do not get geometries from objects that are not supported in the Geobzh specifications ?
        # print(type(entity), entity.dxf.layer)
        raise NotImplementedError(
            'Type DXF non supporté : %s' % entity.dxftype())


def get_insert_block_name(insert_entity):
    """Compute DXF entity block name.
    Used attribute ID whenever possible as some block names are created by AutoCAD and their ID can be found as an attribute

    Args:
        insert_entity (ezdxf.entities.Insert): DXF INSERT entity

    Returns:
        str: block name
    """
    assert isinstance(insert_entity, ezdxf.entities.Insert)
    id_attrib = insert_entity.get_attrib('ID')
    if id_attrib is not None:
        block_name = id_attrib.dxf.text
    else:
        block_name = insert_entity.dxf.name
    return block_name


def get_entity_type_id(entity):
    """Compute a unique DXF entity type ID, for internal use, by merging some parameters specific to entity type

    Args:
        entity (sublass of ezdxf.entities): DXF entity

    Returns:
        str: entity type ID
    """
    if isinstance(entity, ezdxf.entities.Insert):
        return get_insert_block_name(entity)
    elif isinstance(entity, (
            ezdxf.entities.DXFGraphic  # most object types
    )):
        return entity.dxf.linetype
    else:
        # TODO: see how we can handle all ezdxf entity types without crashing
        print(type(entity), entity.dxf.layer)
        raise NotImplementedError(
            'Type DXF non supporté : %s' % entity.dxftype())


def get_entity_verbose_name(entity, layer_name=None):
    """Compute a unique DXF entity verbose name

    Args:
        entity (sublass of ezdxf.entities.DXFEntity): DXF entity
        layer_name : text. Should only be used for INSERT entity when some of their attributes belong to other layers,
                     and therefore the entity can be somewhat 'duplicated' on such layers

    Returns:
        str: entity verbose name
    """
    if layer_name is None:
        layer_name = entity.dxf.layer
    return '%s:%s:%s:%s' % (
        layer_name,
        entity.dxftype(),  # dxf entity type
        get_entity_type_id(entity),  # this is a type id
        entity.dxf.handle  # this is the actual unique ID
    )


def get_entity_aci_rgb_color(doc, entity):
    """Returns ACI and RGB color for entity

    Args:
        doc (ezdxf.document.Drawing): DXF document
        entity (sublass of ezdxf.entities.DXFEntity): DXF entity

    Returns:
        tuple: ACI color (if exists) and RGB tuple
    """
    rgb = entity.rgb
    if rgb is not None:
        return None, rgb
    layer = doc.layers.get(entity.dxf.layer)
    if isinstance(layer.color, tuple):
        return None, layer.color
    rgb = aci2rgb(layer.color)
    return layer.color, rgb


def get_entity_rgb_color(doc, entity):
    """Returns RGB color for entity

    Args:
        doc (ezdxf.document.Drawing): DXF document
        entity (sublass of ezdxf.entities.DXFEntity): DXF entity

    Returns:
        tuple: RGB tuple
    """
    aci, rgb = get_entity_aci_rgb_color(doc, entity)
    return rgb


def get_entity_text(entity):
    """Returns entity text when available for entity type

    Args:
        entity (sublass of ezdxf.entities.DXFEntity): DXF entity

    Returns:
        str or None: entity text or None
    """
    if isinstance(entity, (
        ezdxf.entities.Text,
        ezdxf.entities.Attrib
    )):
        return entity.dxf.text
    elif isinstance(entity, ezdxf.entities.MText):
        return entity.text
    elif isinstance(entity, ezdxf.entities.Insert):
        # TODO: set TXT and TYPE values as a parameter ?
        text = entity.get_attrib_text('TXT', None, True)
        if text is not None:
            return text
        return entity.get_attrib_text('TYPE', None, True)
    else:
        return None


def get_insert_entity_measurements(insert_entity):
    # check entity type
    assert(isinstance(insert_entity, ezdxf.entities.Insert))

    # get block for insert
    block = insert_entity.block()

    # get block entities
    block_entities = [be for be in block if isinstance(be,
                                                       ezdxf.entities.DXFGraphic)
                      and
                      not isinstance(be, ezdxf.entities.attrib.AttDef)
                      ]

    if not block_entities:
        return None, None

    # compute extent of block entities
    block_extent = ezdxf.bbox.extents(block_entities)
    xmin, ymin, z = block_extent.extmin.xyz
    xmax, ymax, z = block_extent.extmax.xyz

    # compute block measurements
    block_dimx = xmax - xmin
    block_dimy = ymax - ymin

    # compute insert entity measurements
    dimx = block_dimx * insert_entity.dxf.xscale
    dimy = block_dimy * insert_entity.dxf.yscale

    return dimx, dimy
