from osgeo import ogr
from shapely import affinity
from shapely.geometry import LineString


def feature_has_field(feature, field_name, case_sensitive=True):
    feature_defn = feature.GetDefnRef()
    for field_index in range(feature_defn.GetFieldCount()):
        field_defn = feature_defn.GetFieldDefn(field_index)
        if case_sensitive:
            if field_defn.GetName() == field_name:
                return True
        else:
            if field_defn.GetName().lower() == field_name.lower():
                return True
    return False


def layer_has_field(layer, field_name, case_sensitive=True):
    feature_defn = layer.GetLayerDefn()
    for field_index in range(feature_defn.GetFieldCount()):
        field_defn = feature_defn.GetFieldDefn(field_index)
        if case_sensitive:
            if field_defn.GetName() == field_name:
                return True
        else:
            if field_defn.GetName().lower() == field_name.lower():
                return True
    return False


def as_unmeasured_geometry(geometry):
    """If geometry is measured, remove the M Dimension

    Args:
        geometry (ogr.Geometry): geometry

    Returns:
        ogr.Geometry: geometry
    """
    # in case geometry is measured (M Coordinate)
    # convert it as a simple geometry
    if geometry.IsMeasured():
        dim = geometry.CoordinateDimension()
        geometry.SetCoordinateDimension(dim - 1)  # remove last dimension (M)
    return geometry


def get_points_from_geometry(geometry, include_inner_geoms=True, process_only_first_geom=False):
    """Return a list of 2D OGR points extracted from supplied geometry

    Args:
        geometry (ogr.Geometry): OGR geometry
        include_inner_geoms (bool, optional): Extract points from inner geometries too (for POLYGONS). Defaults to True.
        process_only_first_geom (bool, optional): Extract points from first geometry only (for MULTI geometries). Defaults to False.

    Raises:
        NotImplementedError: raised if an input OGR geometry type is not already supported

    Returns:
        list: list of 2D OGR points
    """
    ogr_points = []
    geom_type = geometry.GetGeometryType()
    # simple type
    if geom_type in [
        ogr.wkbPoint, ogr.wkbPoint25D, ogr.wkbPointZM,
        ogr.wkbLineString, ogr.wkbLineString25D, ogr.wkbLineStringZM,
        ogr.wkbPolygon, ogr.wkbPolygon25D, ogr.wkbPolygonZM
    ]:
        geom_count = geometry.GetGeometryCount()
        if geom_count == 0:
            for j in range(geometry.GetPointCount()):
                x, y = geometry.GetPoint_2D(j)
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint(x, y)
                ogr_points.append(pt)
        else:
            for i in range(geom_count):
                g = geometry.GetGeometryRef(i)
                for j in range(g.GetPointCount()):
                    x, y = g.GetPoint_2D(j)
                    pt = ogr.Geometry(ogr.wkbPoint)
                    pt.AddPoint(x, y)
                    ogr_points.append(pt)
                    if not include_inner_geoms:
                        break
    # multi type
    elif geom_type in [
        ogr.wkbMultiPoint, ogr.wkbMultiPoint25D, ogr.wkbMultiPointZM,
        ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ogr.wkbMultiLineStringZM,
        ogr.wkbMultiPolygon, ogr.wkbMultiPolygon25D, ogr.wkbMultiPolygonZM,
        ogr.wkbGeometryCollection, ogr.wkbGeometryCollection25D, ogr.wkbGeometryCollectionZM
    ]:
        for i in range(geometry.GetGeometryCount()):
            g = geometry.GetGeometryRef(i)
            ogr_points.extend(get_points_from_geometry(g,
                                                       include_inner_geoms,
                                                       process_only_first_geom))
            if process_only_first_geom:
                break
    else:
        raise NotImplementedError('Unsupported geometry type: %s (%d)' % (
            geometry.GetGeometryName(), geom_type))
    return ogr_points


def get_3d_points_from_geometry(geometry, include_inner_geoms=True, process_only_first_geom=False):
    """Return a list of 3D OGR points extracted from supplied geometry

    Args:
        geometry (ogr.Geometry): OGR geometry
        include_inner_geoms (bool, optional): Extract points from inner geometries too (for POLYGONS). Defaults to True.
        process_only_first_geom (bool, optional): Extract points from first geometry only (for MULTI geometries). Defaults to False.

    Raises:
        NotImplementedError: raised if an input OGR geometry type is not already supported

    Returns:
        list: list of 3D OGR points
    """
    ogr_points = []
    geom_type = geometry.GetGeometryType()
    # simple 3D type
    if geom_type in [
        ogr.wkbPoint25D,
        ogr.wkbLineString25D,
        ogr.wkbPolygon25D,
    ]:
        geom_count = geometry.GetGeometryCount()
        if geom_count == 0:
            for j in range(geometry.GetPointCount()):
                x, y, z = geometry.GetPoint(j)
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint(x, y, z)
                ogr_points.append(pt)
        else:
            for i in range(geom_count):
                g = geometry.GetGeometryRef(i)
                for j in range(g.GetPointCount()):
                    x, y, z = g.GetPoint(j)
                    pt = ogr.Geometry(ogr.wkbPoint)
                    pt.AddPoint(x, y, z)
                    ogr_points.append(pt)
                    if not include_inner_geoms:
                        break
    # simple 2D types
    elif geom_type in [
        ogr.wkbPoint, ogr.wkbPointZM,
        ogr.wkbLineString, ogr.wkbLineStringZM,
        ogr.wkbPolygon, ogr.wkbPolygonZM
    ]:
        geom_count = geometry.GetGeometryCount()
        if geom_count == 0:
            for j in range(geometry.GetPointCount()):
                x, y, z = geometry.GetPoint_2D(j)
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint(x, y, 0)
                ogr_points.append(pt)
        else:
            for i in range(geom_count):
                g = geometry.GetGeometryRef(i)
                for j in range(g.GetPointCount()):
                    x, y = g.GetPoint_2D(j)
                    pt = ogr.Geometry(ogr.wkbPoint)
                    pt.AddPoint(x, y, 0)
                    ogr_points.append(pt)
                    if not include_inner_geoms:
                        break
    # multi type
    elif geom_type in [
        ogr.wkbMultiPoint, ogr.wkbMultiPoint25D, ogr.wkbMultiPointZM,
        ogr.wkbMultiLineString, ogr.wkbMultiLineString25D, ogr.wkbMultiLineStringZM,
        ogr.wkbMultiPolygon, ogr.wkbMultiPolygon25D, ogr.wkbMultiPolygonZM,
        ogr.wkbGeometryCollection, ogr.wkbGeometryCollection25D, ogr.wkbGeometryCollectionZM
    ]:
        for i in range(geometry.GetGeometryCount()):
            g = geometry.GetGeometryRef(i)
            ogr_points.extend(get_3d_points_from_geometry(g,
                                                          include_inner_geoms,
                                                          process_only_first_geom))
            if process_only_first_geom:
                break
    else:
        raise NotImplementedError('Unsupported geometry type: %s (%d)' % (
            geometry.GetGeometryName(), geom_type))
    return ogr_points


def get_representative_point_from_geometry(geom):
    """Returns an OGR point that belongs to supplied OGR geometry

    Args:
        geom (ogr.Geometry): OGR geometry

    Returns:
        ogr.Geometry: OGR point
    """
    # get ogr points of geometry
    ogr_points = get_points_from_geometry(geom,
                                          include_inner_geoms=False,
                                          process_only_first_geom=True)
    # get only first list of points (we do not care about interior rings or multigeometries)
    if isinstance(ogr_points[0], list):
        ogr_points = ogr_points[0]
    # compute representative point
    representative_point = ogr_points[0]
    return representative_point


def are_geometries_2d_equal(geom1, geom2, max_distance):
    """Returns whether supplied geometries can be considered equal, meaning being distant less than max_distance

    Args:
        geom1 (ogr.Geometry): first geometry
        geom2 (ogr.Geometry): second geometry
        max_distance (_type_): maximum distance allowed (<=) between geometries

    Raises:
        NotImplementedError: raised if an input OGR geometry type is not already supported

    Returns:
        boolean: True if geometries are considered equal
    """
    geom1_2d = geom1.Clone()
    geom1_2d.FlattenTo2D()
    geom2_2d = geom2.Clone()
    geom2_2d.FlattenTo2D()
    geom1_2d_type = geom1_2d.GetGeometryType()
    geom2_2d_type = geom2_2d.GetGeometryType()
    if geom1_2d_type != geom2_2d_type:
        return False
    if geom1_2d_type == ogr.wkbPoint:
        return geom1_2d.Distance(geom2_2d) <= max_distance
    elif geom1_2d_type in [ogr.wkbLineString, ogr.wkbPolygon]:
        # check is geom1 has a point that is not close to geom2
        geom1_count = geom1_2d.GetGeometryCount()
        if geom1_count == 0:
            for j in range(geom1_2d.GetPointCount()):
                x, y = geom1_2d.GetPoint_2D(j)
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint(x, y)
                if pt.Distance(geom2_2d) > max_distance:
                    return False
        else:
            for i in range(geom1_count):
                g = geom1_2d.GetGeometryRef(i)
                for j in range(g.GetPointCount()):
                    x, y = g.GetPoint_2D(j)
                    pt = ogr.Geometry(ogr.wkbPoint)
                    pt.AddPoint(x, y)
                    if pt.Distance(geom2_2d) > max_distance:
                        return False

        # check is geom2 has a point that is not close to geom1
        geom2_count = geom2_2d.GetGeometryCount()
        if geom2_count == 0:
            for j in range(geom2_2d.GetPointCount()):
                x, y = geom2_2d.GetPoint_2D(j)
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint(x, y)
                if pt.Distance(geom1_2d) > max_distance:
                    return False
        else:
            for i in range(geom2_count):
                g = geom2_2d.GetGeometryRef(i)
                for j in range(g.GetPointCount()):
                    x, y = g.GetPoint_2D(j)
                    pt = ogr.Geometry(ogr.wkbPoint)
                    pt.AddPoint(x, y)
                    if pt.Distance(geom1_2d) > max_distance:
                        return False
        return True
    elif geom1_2d_type in [ogr.wkbMultiPoint,
                           ogr.wkbMultiLineString,
                           ogr.wkbMultiPolygon,
                           ogr.wkbGeometryCollection
                           ]:
        return False  # TODO: compare this geometry types
    else:
        raise NotImplementedError


def get_feature_verbose_name(layer_name, feature):
    """Returns a unique feature verbose name

    Args:
        layer_name (str): layer name
        feature (ogr.Feature): OGR feature

    Returns:
        str: verbose name
    """
    return '%s:%s' % (layer_name, feature.GetFID())


def exterior_ring(polygon):
    exterior_ring = polygon.GetGeometryRef(0)
    return exterior_ring


def as_multipoint(point):
    geom_type = point.GetGeometryType()
    if geom_type == ogr.wkbPoint:
        multi_geom = ogr.Geometry(ogr.wkbMultiPoint)
    elif geom_type == ogr.wkbPoint25D:
        multi_geom = ogr.Geometry(ogr.wkbMultiPoint25D)
    multi_geom.AddGeometry(point)
    return multi_geom


def as_multilinestring(linestring):
    geom_type = linestring.GetGeometryType()
    if geom_type == ogr.wkbLineString:
        multi_geom = ogr.Geometry(ogr.wkbMultiLineString)
    elif geom_type == ogr.wkbLineString25D:
        multi_geom = ogr.Geometry(ogr.wkbMultiLineString25D)
    multi_geom.AddGeometry(linestring)
    return multi_geom


def as_polygon(linestring):
    geom_type = linestring.GetGeometryType()
    if geom_type == ogr.wkbLineString:
        polygon = ogr.Geometry(ogr.wkbPolygon)
    elif geom_type == ogr.wkbLineString25D:
        polygon = ogr.Geometry(ogr.wkbPolygon25D)
    ring = ogr.Geometry(ogr.wkbLinearRing)  # does not exist as 25D
    # add points
    points = linestring.GetPoints()
    for p in points:
        ring.AddPoint(*p)
    # add first point
    ring.AddPoint(*points[0])
    # add ring to polygon
    polygon.AddGeometry(ring)
    return polygon


def as_linestring(point, block_segment_length, xscale, rotation):
    # create base segment
    point_tuple = point.GetPoint()
    p1 = point_tuple
    is_3D = len(point_tuple) == 3
    if is_3D:
        x, y, z = point_tuple
        p2 = (x + block_segment_length * xscale, y, z)
    else:
        x, y = point_tuple
        p2 = (x + xscale, y)
    line = LineString([p1, p2])
    # rotate segment
    rotated_line = affinity.rotate(line, rotation, p1)
    # convert as OGR
    if is_3D:
        linestring = ogr.Geometry(ogr.wkbLineString25D)
    else:
        linestring = ogr.Geometry(ogr.wkbLineString)
    linestring.AddPoint(*rotated_line.coords[0])
    linestring.AddPoint(*rotated_line.coords[1])
    return linestring


class OGRWriter:
    """Class to handle batch writing of OGR features on a layer
    """

    def __init__(self, ogr_layer, transaction_max_size=1000):
        """Constructor

        Args:
            ogr_layer (ogr.Layer): OGR Layer
            transaction_max_size (int, optional): Maximum number of features in a write transaction. Defaults to 1000.
        """
        self.ogr_layer = ogr_layer
        self.features_to_write = []
        self.transaction_max_size = transaction_max_size

    def add_feature(self, feature):
        """add a feature for writing

        Args:
            feature (ogr.Feature): OGR feature

        Returns:
            boolean: True if feature was properly added
        """
        self.features_to_write.append(feature)
        if len(self.features_to_write) >= self.transaction_max_size:
            self.__write_features()
        return True

    def CreateFeature(self, feature):
        """Method for compatibility with OGR Layer to add features

        Args:
            feature (ogr.Feature): OGR feature

        Returns:
            boolean: True if feature was properly added
        """
        return self.add_feature(feature)

    def GetLayerDefn(self):
        """Method for compatibility with OGR Layer to get a layer definition

        Args:
            feature (ogr.Feature): OGR feature

        Returns:
            ogr.FeatureDefn: Layer definition
        """
        return self.ogr_layer.GetLayerDefn()

    def has_field_defn(self, field_name, case_sensitive=True):
        return layer_has_field(self.ogr_layer, field_name, case_sensitive)

    def CreateField(self, field_defn):
        self.ogr_layer.CreateField(field_defn)

    def __write_features(self):
        """Internal method to write features in layer and reset transaction
        """
        self.ogr_layer.StartTransaction()
        for feature in self.features_to_write:
            self.ogr_layer.CreateFeature(feature)
        self.ogr_layer.CommitTransaction()
        self.features_to_write = []

    def flush(self):
        self.__write_features()

    def close(self):
        """Close writer and flush all pending writes
        """
        self.flush()
