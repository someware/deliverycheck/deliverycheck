from osgeo import gdal
import numpy
import math


class Bounds:
    def __init__(self, xmin, ymin, xmax, ymax):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax

    def __repr__(self):
        return 'Bounds: %f, %f, %f, %f' % (self.xmin, self.ymin, self.xmax, self.ymax)

    
    def extend(self, distance):
        return Bounds(self.xmin - distance, self.ymin - distance,
                      self.xmax + distance, self.ymax + distance)

    def contains_point(self, world_x, world_y):
        return (world_x >= self.xmin and world_x <= self.xmax
                and world_y >= self.ymin and world_y <= self.ymax)

    def contains_bounds(self, bounds):
        return (bounds.xmin >= self.xmin and bounds.xmax <= self.xmax
                and bounds.ymin >= self.ymin and bounds.ymax <= self.ymax)

    def lower_left(self):
        return self.xmin, self.ymin

    def upper_right(self):
        return self.xmax, self.ymax


class RasterInfo:
    def __init__(self, gdal_dataset):
        self.gdal_dataset = gdal_dataset
        self.xsize = gdal_dataset.RasterXSize
        self.ysize = gdal_dataset.RasterYSize
        geotransform = gdal_dataset.GetGeoTransform()
        self.xmin = geotransform[0]
        self.dx = geotransform[1]
        self.xmax = self.xmin + self.dx * self.xsize
        self.ymax = geotransform[3]
        self.dy = math.fabs(geotransform[5])
        self.ymin = self.ymax - self.dy * self.ysize
        self._bounds = Bounds(self.xmin, self.ymin, self.xmax, self.ymax)        

    @property
    def bounds(self):
        return self._bounds

    def get_pixel_coordinates(self, world_x, world_y):
        if not self.bounds.contains_point(world_x, world_y):
            return False, None, None
        # TODO : int cast can lead to side effects, improve min/max handling
        pixel_x = int((world_x - self.xmin) / self.dx)
        pixel_y = int((self.ymax - world_y) / self.dy)
        if pixel_x < 0.0 or pixel_x > self.xsize:
            return False, None, None
        if pixel_y < 0.0 or pixel_y > self.ysize:
            return False, None, None
        return True, pixel_x, pixel_y

    def get_float_pixel_value(self, world_x, world_y, raster_band):
        within, px, py = self.get_pixel_coordinates(world_x, world_y)
        if not within:
            return None
        value = raster_band.ReadAsArray(px, py, 1, 1).astype(numpy.float)[0][0]
        return value

class DTMInfo(RasterInfo):
    def __init__(self, gdal_dataset):
        super().__init__(gdal_dataset)
        # Check num bands (a DTM always has only 1 band)
        num_bands = gdal_dataset.RasterCount
        self._is_valid = (num_bands == 1)
        # Get first (and only) band
        if self._is_valid:
            self.band = gdal_dataset.GetRasterBand(1)
            self.no_data_value = self.band.GetNoDataValue()
    
    @property
    def is_valid(self):
        return self._is_valid

    def get_terrain_height(self, world_x, world_y):
        """Return terrain height at world position (same spatial reference as input dataset)

        Args:
            world_x (float): world x coordinate
            world_y (float): world y coordinate

        Returns:
            float: height value or None if position is out of bounds
        """
        if not self.is_valid:
            return None
        height = self.get_float_pixel_value(world_x, world_y, self.band)
        if height == self.no_data_value:
            return None
        return height
