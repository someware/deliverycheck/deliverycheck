import unicodedata
import os.path
import os

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

def get_basename_without_extension(path):
    return os.path.splitext(os.path.basename(path))[0]

def remove_file_if_exists(path):
    if os.path.exists(path) and os.path.isfile(path):
        os.remove(path)