# DeliveryCheck

DeveliryCheck is an Open Source Python module aimed at running checks and creating reports on data deliverables (mostly GIS or CAD deliverables).

This first version of the module is meant to check topographic data according to the GeoBretagne specifications.

Its code is therefore mostly architectured for this purpose, but it could be easily improved to introduce other check profiles in some pluggable way so that each check profile is a standalone python module that relies on the base mechanisms provided by DeliveryCheck.

## License

The software is distributed under the terms of the CeCILL-C license, which is similar to the LGPL license.

The full license for this software is available in file `LICENCE.txt`.

## Getting started

Install Python 3.9+

Install requirements
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements-dev.txt
```

Beware requirements.txt point to a specific GDAL version. The GDAL module can also be installed this way:
```
pip install gdal
pip install -r requirements-dev.txt
```

Install package
```
pip install -e .
```

Run
```
deliverycheck --help
```

Example
```
deliverycheck -t referentiel_topographie_geobretagne.socle_commun.dxf -p "BUBRY_022Rue Poulna_CentreSecours_GeofitExpert_3D(20220406).dxf" -s EPSG:3948 --report-dir /tmp/ --bounds-path contour_bubry.geojson --bounds-srs EPSG:4326
```

To run all examples (.sh files), you first need to get all referenced DXF/SHP files. Then you can create a ```paths.env``` file with the proper variables to set your input/output directories. See example ```paths_to_adapt.env``` 


## Testing

To test if the checks and various functions are running corectly
```
pytest
```

## Credits

Initial development of DeliveryCheck was supported by the following organizations:

* Région Bretagne
* Région Pays de Loire
* Lorient Agglomération
* Rennes Métropole
* Brest Métropole
* Lannion-Trégor Communauté
* SDEF
* SDE 22
* SDE 35
* Morbihan Énergies
* Redon Agglomération
* Quimperlé Communauté
* Ploërmel Communauté
* Communauté de Communes du Pays Fouesnantais

Software development was carried out by [Someware](https://www.someware.fr).

