###################################################
# Check Socle Commun DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# SDEF

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/CCHC/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/crozon/SHP" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/ -o bounds_file_path ${DATAPATH}/sdef/crozon/crozon.geojson -o bounds_srs EPSG:4326

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/lanmeur/FORMAT SHAPES-GML/SHP" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/bourg-blanc/VECTO/SHP" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/autre-territoire/SHP/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/territoire-clean/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/territoire-clean2/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/saint-martin/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/landivisiau/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/bigpcrs/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230526" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/montsdarree/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/combrit/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

# deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230626/VOIEF_L.shp" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

# deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230626/JARDI_P.shp" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

# deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230626/PCRS_AFFLEURANT_E.shp" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

# deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230626/CLOTU_L.shp" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/

# deliverycheck check-dataset -t referentiel_topographie_geobretagne.sdef.shapefile -p "${DATAPATH}/sdef/export_20230626/" -o srs EPSG:3948 -o data_type RTS --report-dir ${OUTPUTPATH}/
