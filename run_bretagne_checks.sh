###################################################
# Check Bretagne DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

deliverycheck check-dataset -t referentiel_topographie_geobretagne.socle_commun.dxf -p "${DATAPATH}/bretagne/20230308683-24_DETECT_1_CC48.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} 