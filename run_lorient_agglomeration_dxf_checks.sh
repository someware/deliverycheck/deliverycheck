###################################################
# Check Lorient DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# DXF checks
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/Lorient_Estacade port-Rue gahinet_Cabinet Nicolas_20230609.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} 

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/Guidel_Rue duPort_Cabinet Nicolas_20230117.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} 

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LORIENT_RUE_DE_BELGIQUE_GEOSAT_20230223_ag.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} 

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/BUBRY_022Rue Poulna_CentreSecours_GeofitExpert_3D(20220406).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o bounds_file_path ${DATAPATH}/lorient/contour_bubry.geojson -o bounds_srs EPSG:4326 -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LOCMIQUELIC_018 Rue Roger Tremaré_Geofit Expert_3D(20220408).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/QUEVEN_021CheminDeDoued_GeofitExpert_3D(20220405)_.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/RIANTEC 017_Rue du stade_Geofit Expert_3D(20220209)_.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/RIANTEC_015_Rue Alain Le Blevec_GeofitExpert_3D(20220404).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/socle_commun+LA_testeur.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/socle_commun+LA.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/INZINZACH LOCHRIST_Ty Mat_QUARTA 2D_20230804.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LANESTER_Bd_Noermandie_Av_Gabriel_Péri_SARL Laurent MARTIN_20230307_2D.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LORIENT_Bois du Château_QUARTA 3D_20240327.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} 

# DXF + Archive checks + Control points checks

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LOCMIQUELIC_018 Rue Roger Tremaré_Geofit Expert_3D(20220408).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o archive_path ${DATAPATH}/lorient/archive_plugin_la.zip -o control_points_path "${DATAPATH}/lorient/canevas.gpkg" -o control_points_ogr_driver_name GPKG -o control_points_srs EPSG:3948 -o control_points_accuracy_config_path accuracy_check_sample.csv

# DXF + DTM checks

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LOCMIQUELIC_018 Rue Roger Tremaré_Geofit Expert_3D(20220408).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o dtm_path "/media/data/workspaces/veille_data/mnt_rgealti_56_epsg_2154.tif" -o dtm_srs EPSG:2154 -o dtm_accuracy_config_path dtm_accuracy_check_sample.csv

deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/BUBRY_022Rue Poulna_CentreSecours_GeofitExpert_3D(20220406).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o dtm_path "/media/data/workspaces/veille_data/mnt_rgealti_56_epsg_2154.tif" -o dtm_srs EPSG:2154 -o dtm_accuracy_config_path dtm_accuracy_check_sample.csv

# GML export
deliverycheck check-dataset -t referentiel_topographie_geobretagne.lorient_agglomeration.dxf -p "${DATAPATH}/lorient/LOCMIQUELIC_018 Rue Roger Tremaré_Geofit Expert_3D(20220408).dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"Lorient Agglomération\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {\"ELEC\": \"Gestionnaire ELEC\",\"INCE\": \"Gestionnaire Incendie\"},\"default_qualite\" : \"01\",\"qualite_map\" : {\"02\": \"02\"},\"default_precisionxy\" : 5,\"precisionxy_map\" : {\"02\": 5},\"default_precisionz\" : 5,\"precisionz_map\" : {\"02\": 10}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"