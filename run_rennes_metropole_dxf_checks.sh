###################################################
# Check Socle Commun DXF datasets
###################################################

# Exit when any command fails
set -e

export $(grep -v '^#' paths.env | xargs)

# DXF checks
deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/MD_codif_topstation.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/export_base.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/topo_all.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH}

# DXF + DTM checks

deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/topo_all.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o dtm_path "/media/data/workspaces/aap_bretagne/Donnees/MNT/0.50/optimize/MNT_SUP_50_RM_18_CC48.tif" -o dtm_srs EPSG:3948 -o dtm_accuracy_config_path dtm_accuracy_check_sample.csv

# GML export
deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/export_base.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"Rennes Métropole\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {\"ELEC\": \"Gestionnaire ELEC\",\"INCE\": \"Gestionnaire Incendie\"},\"default_qualite\" : \"01\",\"qualite_map\" : {\"02\": \"02\"},\"default_precisionxy\" : 5,\"precisionxy_map\" : {\"02\": 5},\"default_precisionz\" : 5,\"precisionz_map\" : {\"02\": 10}, \"prefixe_id\" : \"ABC\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"

# GML export with no prefixe_id set
deliverycheck check-dataset -t referentiel_topographie_geobretagne.rennes_metropole.dxf -p "${DATAPATH}/rennes/export_base.dxf" -o srs EPSG:3948 --report-dir ${OUTPUTPATH} -o gml_export_options "{\"producteur\" : \"Rennes Métropole\", \"default_gestionnaire\" : \"Gestionnaire par défaut\", \"gestionnaire_map\" : {\"ELEC\": \"Gestionnaire ELEC\",\"INCE\": \"Gestionnaire Incendie\"},\"default_qualite\" : \"01\",\"qualite_map\" : {\"02\": \"02\"},\"default_precisionxy\" : 5,\"precisionxy_map\" : {\"02\": 5},\"default_precisionz\" : 5,\"precisionz_map\" : {\"02\": 10}, \"prefixe_id\" : \"\", \"date_leve\" : \"2023-01-18\", \"horodatage\" : \"2023-01-01\"}"