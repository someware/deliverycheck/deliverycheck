from setuptools import setup, find_packages

setup(
    name='deliverycheck',
    version='1.0.7',
    description=(
        'Python lib to check topographic data and '
        'export it in the French PCRS format (GML based).',
    ),
    author='Someware',
    url='https://gitlab.com/someware/deliverycheck/deliverycheck',
    python_requires='>=3',
    install_requires=[
        'click',
        'ezdxf',
        'GDAL',
        'rtree',
        'shapely',
        'xmlschema',
        'Cerberus'
    ],
    packages=find_packages(
        where='.',
        include=['deliverycheck.*'],
    ),

    include_package_data=True,
    entry_points={
        'console_scripts': [
            'deliverycheck = deliverycheck.cli.cli:cli',
        ],
    }
)
